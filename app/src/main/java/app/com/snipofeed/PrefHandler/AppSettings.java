package app.com.snipofeed.PrefHandler;

import android.content.Context;

import org.json.JSONArray;

/**
 * Created by yuvaraj on 25/04/18.
 */

public class AppSettings {
    Context context;
    String accessToken;
    String userType;

    public String getUserType() {
        userType = SharedPreference.getKey(context, "userType");

        return userType;
    }

    public void setUserType(String userType) {
        SharedPreference.putKey(context, "userType", userType);

        this.userType = userType;
    }

    String isLoggedIn;
    String email;
    String fullName;
    String userName;
    String imagePath;
    String fireBaseToken;
    JSONArray myPostsArray;
    String website;
    String canScroll;

    public String getCanScroll() {
        canScroll = SharedPreference.getKey(context, "canScroll");

        return canScroll;
    }

    public void setCanScroll(String canScroll) {
        SharedPreference.putKey(context, "canScroll", canScroll);

        this.canScroll = canScroll;
    }

    String bioDescription;
    String userEmail;
    String userGender;
    String userMobile;

    public String getIsPrivateAccout() {
        isPrivateAccout = SharedPreference.getKey(context, "isPrivateAccout");

        return isPrivateAccout;
    }

    public void setIsPrivateAccout(String isPrivateAccout) {
        SharedPreference.putKey(context, "isPrivateAccout", isPrivateAccout);

        this.isPrivateAccout = isPrivateAccout;
    }

    String isPrivateAccout;

    public String getUserMobile() {
        userMobile = SharedPreference.getKey(context, "userMobile");

        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        SharedPreference.putKey(context, "userMobile", userMobile);

        this.userMobile = userMobile;
    }

    public String getUserGender() {
        userGender = SharedPreference.getKey(context, "userGender");

        return userGender;
    }

    public void setUserGender(String userGender) {
        SharedPreference.putKey(context, "userGender", userGender);

        this.userGender = userGender;
    }

    public String getUserEmail() {
        userEmail = SharedPreference.getKey(context, "userEmail");

        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        SharedPreference.putKey(context, "userEmail", userEmail);

        this.userEmail = userEmail;
    }

    public String getBioDescription() {
        bioDescription = SharedPreference.getKey(context, "bioDescription");

        return bioDescription;
    }

    public void setBioDescription(String bioDescription) {
        SharedPreference.putKey(context, "bioDescription", bioDescription);

        this.bioDescription = bioDescription;
    }

    public String getWebsite() {
        website = SharedPreference.getKey(context, "website");

        return website;
    }

    public void setWebsite(String website) {
        SharedPreference.putKey(context, "website", website);

        this.website = website;
    }

    public String getUserImage() {
        userImage = SharedPreference.getKey(context, "userImage");

        return userImage;
    }

    public void setUserImage(String userImage) {
        SharedPreference.putKey(context, "userImage", userImage);

        this.userImage = userImage;
    }

    String userImage;


    public AppSettings(Context context) {
        this.context = context;
    }

    public String getFullName() {
        fullName = SharedPreference.getKey(context, "fullName");

        return fullName;
    }
    public String getFireBaseToken() {
        fireBaseToken=SharedPreference.getKey(context,"fireBaseToken");
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        SharedPreference.putKey(context, "fireBaseToken", fireBaseToken);
        this.fireBaseToken = fireBaseToken;
    }

    public String getImagePath() {
        imagePath = SharedPreference.getKey(context, "imagePath");

        return imagePath;
    }

    public void setImagePath(String imagePath) {
        SharedPreference.putKey(context, "imagePath", imagePath);

        this.imagePath = imagePath;
    }

    public void setFullName(String fullName) {
        SharedPreference.putKey(context, "fullName", fullName);

        this.fullName = fullName;
    }

    public String getUserName() {
        userName = SharedPreference.getKey(context, "userName");

        return userName;
    }

    public void setUserName(String userName) {
        SharedPreference.putKey(context, "userName", userName);

        this.userName = userName;
    }

    public String getEmail() {
        email = SharedPreference.getKey(context, "email");

        return email;
    }

    public void setEmail(String email) {
        SharedPreference.putKey(context, "email", email);

        this.email = email;
    }

    public String getIsLoggedIn() {
        isLoggedIn = SharedPreference.getKey(context, "isLoggedIn");

        return isLoggedIn;
    }

    public void setIsLoggedIn(String isLoggedIn) {
        SharedPreference.putKey(context, "isLoggedIn", isLoggedIn);

        this.isLoggedIn = isLoggedIn;
    }

    public String getAccessToken() {
        accessToken = SharedPreference.getKey(context, "accessToken");
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        SharedPreference.putKey(context, "accessToken", accessToken);
        this.accessToken = accessToken;
    }


}
