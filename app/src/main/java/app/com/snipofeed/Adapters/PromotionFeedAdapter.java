package app.com.snipofeed.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.varunest.sparkbutton.SparkButton;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.RecyclerViewIndicator;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Videhandler.UniversalMediaController;
import app.com.snipofeed.Videhandler.UniversalVideoView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 02/05/18.
 */

public class PromotionFeedAdapter extends RecyclerView.Adapter<PromotionFeedAdapter.ViewHolder> {


    JSONObject jsonObject;
    AppSettings appSettings;
    Dialog dialog;
    FragmentManager fragmentManager;
    int value;
    private Activity context;
    private String TAG = PromotionFeedAdapter.class.getSimpleName();

    public PromotionFeedAdapter(Activity context, JSONObject jsonObject, FragmentManager supportFragmentManager, int i) {
        this.context = context;
        appSettings = new AppSettings(context);
        this.jsonObject = jsonObject;
        this.fragmentManager = supportFragmentManager;
        this.value = i;
    }

    @NonNull
    @Override
    public PromotionFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_feed_item, viewGroup, false);
        return new PromotionFeedAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final PromotionFeedAdapter.ViewHolder holder, final int position) {



        if (value==1) {
            if (jsonObject.optString("isPromoted").equalsIgnoreCase("1")) {
                holder.userLocation.setText(context.getResources().getString(R.string.sponsored));
                if (jsonObject.optString("actionButton").equalsIgnoreCase("0")) {
                    holder.sponsoredLayout.setVisibility(View.GONE);

                } else {
                    holder.sponsoredText.setText(jsonObject.optString("actionButton"));
                    holder.sponsoredLayout.setVisibility(View.VISIBLE);

                }

            } else {
                holder.userLocation.setText(jsonObject.optString("area"));
                holder.sponsoredLayout.setVisibility(View.GONE);

            }
        } else {
            holder.sponsoredLayout.setVisibility(View.GONE);

        }

        String username = jsonObject.optString("name");
        String caption = jsonObject.optString("caption");
        if (caption.length() > 0) {
            setTags(holder.captionText, caption, username, position);
        }

        Glide.with(context).load(jsonObject.optString("profilePic")).apply(Utils.getRequestOptions()).into(holder.userImage);
        holder.userName.setText(username);
        holder.userLocation.setText(jsonObject.optString("area"));
        holder.postTime.setText(Utils.getDateFormatted(jsonObject.optString("createdAt")));
        holder.likesCount.setText(jsonObject.optString(ConstantKeys.LIKES_COUNT_KEY));
        holder.commentsCount.setText(jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY));
        JSONArray linkDataArray = new JSONArray();
        linkDataArray = jsonObject.optJSONArray("linkData");
        //            setUpViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);
        setUpTempViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);

        if (jsonObject.optString(ConstantKeys.BOOKMARK_KEY).equalsIgnoreCase("true")) {
            holder.bookmarkButton.setChecked(true);
        } else {
            holder.bookmarkButton.setChecked(false);
        }


        if (jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY).equalsIgnoreCase("0")) {
            holder.fullCommentCount.setVisibility(View.GONE);
        } else {
            holder.fullCommentCount.setVisibility(View.VISIBLE);
            holder.fullCommentCount.setText(context.getResources().getString(R.string.view_all) + " " + jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY) + " " + context.getResources().getString(R.string.comments));
//
        }


        if (jsonObject.optString(ConstantKeys.USER_LIKED_KEY).equalsIgnoreCase("true")) {
            holder.likeButton.setChecked(true);
        } else {
            holder.likeButton.setChecked(false);
        }

        holder.bookmarkButton.setEnabled(false);
        holder.likeButton.setEnabled(false);



        holder.sponsoredLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=jsonObject.optString("websiteUrl");
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);

            }
        });


    }

    private void setUpTempViewPager(JSONArray linkDataArray, RecyclerView recyclerView, RecyclerViewIndicator circleIndicator) {

        NewsFeedMediaAdapterTemp newsFeedMediaAdapter = new NewsFeedMediaAdapterTemp(context, linkDataArray);
        SnapHelper snapHelper = new PagerSnapHelper();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        try {
            snapHelper.attachToRecyclerView(recyclerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
//
        recyclerView.setAdapter(newsFeedMediaAdapter);
        circleIndicator.setRecyclerView(recyclerView);
        circleIndicator.forceUpdateItemCount();

        if (linkDataArray.length() < 2) {
            circleIndicator.setVisibility(View.GONE);
        } else {
            circleIndicator.setVisibility(View.VISIBLE);
        }

        newsFeedMediaAdapter.setClickListener(new onRecyclerViewItemClicked() {
            @Override
            public void onClicked(String username) {

                showPlayDialog(username);

            }

            @Override
            public void onClicked(String username, JSONObject jsonObject) {

            }
        });
    }


    private void showPlayDialog(final String username) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.video_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

        final UniversalVideoView mVideoView;
        UniversalMediaController mMediaController;
        mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        dialog.findViewById(R.id.closeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                int cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoView.setLayoutParams(videoLayoutParams);
                mVideoView.start();
                mVideoView.setVideoPath(username);
                mVideoView.requestFocus();

            }
        });


    }


    @Override
    public int getItemCount() {
        return 1;
    }

    private void setTags(TextView pTextView, String pTagString, String username, final int caption_tags) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#' || pTagString.charAt(i) == '@') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    Log.d(TAG, "setTags: " + tag);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            Log.d(TAG, "onClick: " + tag);
                            if (tag.startsWith("#")) {


                                JSONArray capArray = jsonObject.optJSONArray("caption_tags");

                                for (int i = 0; i < capArray.length(); i++) {
                                    Log.d(TAG, "goingInside" + capArray.optJSONObject(i).optString("tagName") + "/" + tag);
                                    String checkvalue = capArray.optJSONObject(i).optString("tagName");
                                    if (checkvalue.trim().equalsIgnoreCase(tag.trim())) {
                                        Log.d(TAG, "onClick: Happened");

                                    }
                                }


                            } else if (tag.startsWith("@")) {
//                                moveToMentionPage(tag, caption_tags);

                            }
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#33b5e5"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        SpannableString ss_caption = new SpannableString(username);

        ss_caption.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.text_black)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        pTextView.setText("");
        pTextView.append(ss_caption);
        pTextView.append(" ");
        pTextView.append(string);
    }


    public String removeFirstChar(String s) {
        return s.substring(1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView captionText, userLocation, userName, postTime, likesCount, commentsCount, fullCommentCount,sponsoredText;
        ImageView postOption;
        ImageView postImage;
        SparkButton bookmarkButton, likeButton;
        CircleImageView userImage;
        RecyclerViewIndicator circleIndicator;
        //        AAH_CustomRecyclerView mediaPager;
        RecyclerView mediaPager;
        LinearLayout commentLayout;
        RelativeLayout sponsoredLayout;

        public ViewHolder(View view) {
            super(view);
            captionText = view.findViewById(R.id.captionText);
            postImage = view.findViewById(R.id.postImage);
            userImage = view.findViewById(R.id.userImage);
            userLocation = view.findViewById(R.id.userLocation);
            postTime = view.findViewById(R.id.postTime);
            userName = view.findViewById(R.id.userName);
            bookmarkButton = view.findViewById(R.id.bookmarkButton);
            sponsoredText = view.findViewById(R.id.sponsoredText);
            sponsoredLayout = view.findViewById(R.id.sponsoredLayout);
            likeButton = view.findViewById(R.id.likeButton);
            postOption = view.findViewById(R.id.postOption);
            likesCount = view.findViewById(R.id.likesCount);
            commentsCount = view.findViewById(R.id.commentsCount);
            fullCommentCount = view.findViewById(R.id.fullCommentCount);
            mediaPager = view.findViewById(R.id.mediaPager);
            circleIndicator = view.findViewById(R.id.circleIndicator);
            commentLayout = view.findViewById(R.id.commentLayout);

            captionText.setMovementMethod(LinkMovementMethod.getInstance());

        }
    }


}

