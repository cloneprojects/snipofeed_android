package app.com.snipofeed.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allattentionhere.autoplayvideos.AAH_CustomRecyclerView;
import com.bumptech.glide.Glide;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.FullCommentActivity;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.RecyclerViewIndicator;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Videhandler.UniversalMediaController;
import app.com.snipofeed.Videhandler.UniversalVideoView;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 02/05/18.
 */

public class PostFeedAdapter extends RecyclerView.Adapter<PostFeedAdapter.ViewHolder> {


    JSONArray jsonArray;
    AppSettings appSettings;
    Dialog dialog;
    FragmentManager fragmentManager;
    private Activity context;
    private String TAG = PostFeedAdapter.class.getSimpleName();
    private onBottomReachedListner onBottomReachedListner;
    private onProfileImageClicked onProfileImageClicked;
    private onHashTagClicked onHashTagClicked;
    private onMentionClicked onMentionClicked;

    public PostFeedAdapter(Activity context, JSONArray newsPosts, FragmentManager supportFragmentManager) {
        this.context = context;
        appSettings = new AppSettings(context);
        this.jsonArray = newsPosts;
        this.fragmentManager = supportFragmentManager;
    }

    @NonNull
    @Override
    public PostFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_feed_item, viewGroup, false);
        return new PostFeedAdapter.ViewHolder(view);
    }

    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }

    public void removeObject(int position) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            jsonArray.remove(position);
        }
    }

    public void changeBookmarkStatus(String status, int position) {
//        jsonArray.optJSONObject(position).remove(ConstantKeys.BOOKMARK_KEY);
//        try {
//            jsonArray.optJSONObject(position).put(ConstantKeys.BOOKMARK_KEY, status);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        notifyDataSetChanged();

    }

    public void changeLikeStatus(String status, int position) {
        Log.d(TAG, "changeLikeStatus: " + status);
        jsonArray.optJSONObject(position).remove(ConstantKeys.USER_LIKED_KEY);
        try {
            jsonArray.optJSONObject(position).put(ConstantKeys.USER_LIKED_KEY, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int countValue;
        countValue = Integer.parseInt(jsonArray.optJSONObject(position).optString(ConstantKeys.LIKES_COUNT_KEY));
        if (status.equalsIgnoreCase("true")) {
            countValue = countValue + 1;
        } else {
            countValue = countValue - 1;
        }
        jsonArray.optJSONObject(position).remove(ConstantKeys.LIKES_COUNT_KEY);
        try {
            jsonArray.optJSONObject(position).put(ConstantKeys.LIKES_COUNT_KEY, "" + countValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();

    }


    public void deleteConfirmation(final String postId, final int position) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.confirm_deleteion);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        dialog.show();
        final TextView dontDelete, delete;
        dontDelete = dialog.findViewById(R.id.dontDelete);
        delete = dialog.findViewById(R.id.delete);
        dontDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    deletePostAll(postId, position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void showPostOptionsDialog(final String postId, final String userId, final int position, boolean isDeleteenabled) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        if (isDeleteenabled) {
            dialog.setContentView(R.layout.post_options_layout_delete);
        } else {
            dialog.setContentView(R.layout.post_options_layout);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        dialog.show();
        TextView reportPost, unFollowUser, deletePost;
        unFollowUser = dialog.findViewById(R.id.unFollowUser);
        reportPost = dialog.findViewById(R.id.reportPost);
        if (isDeleteenabled) {
            deletePost = dialog.findViewById(R.id.deletePost);
            deletePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    deleteConfirmation(postId, position);

                }
            });

        }

        reportPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showReportOptionsDialog(postId, position);

            }
        });

        unFollowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    unFollowUserApi(userId, position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void deletePostAll(String postId, final int position) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", postId);
        ApiCall.PostMethod(context, UrlHelper.DELETE_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    jsonArray.remove(position);
                }
                notifyDataSetChanged();
                dialog.dismiss();


            }
        });
    }

    private void unFollowUserApi(String userId, final int position) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(context, UrlHelper.UN_FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                EventBus.getDefault().post(new unFollow(position));

            }
        });
    }

    private void reportPost(String postId, String status) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", postId);
        jsonObject.put("reason", status);
        ApiCall.PostMethod(context, UrlHelper.REPORT_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                showReportSucessDialog();
            }
        });

    }

    public void showReportSucessDialog() {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.report_sucess_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


    }


    public void showReportOptionsDialog(final String postId, int position) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.report_options_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView spamView, inAppropriate;
        spamView = dialog.findViewById(R.id.spamView);
        inAppropriate = dialog.findViewById(R.id.inAppropriate);

        spamView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                try {
                    reportPost(postId, context.getResources().getString(R.string.its_spam));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        inAppropriate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                try {
                    reportPost(postId, context.getResources().getString(R.string.its_inappropriate));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }


    @Override
    public void onBindViewHolder(final PostFeedAdapter.ViewHolder holder, final int position) {

        if (jsonArray.length() > 8) {
            if (position == jsonArray.length() - 1) {
                onBottomReachedListner.onBottomReached(position);
            }
        }

        final boolean isDeleteenabled;

        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        if (jsonObject.optString("isPromoted").equalsIgnoreCase("1"))
        {
            holder.userLocation.setText(context.getResources().getString(R.string.sponsored));
            if (jsonObject.optString("goalType").equalsIgnoreCase("1"))
            {
                holder.sponsoredLayout.setVisibility(View.GONE);

            } else {
                holder.sponsoredText.setText(jsonObject.optString("actionButton"));
                holder.sponsoredLayout.setVisibility(View.VISIBLE);

            }

        } else {
            holder.userLocation.setText(jsonObject.optString("area"));
            holder.sponsoredLayout.setVisibility(View.GONE);

        }
        Log.d(TAG, "onBindViewHolder: " + jsonObject);

        if (jsonObject.optString("username").equalsIgnoreCase(appSettings.getUserName())) {
            isDeleteenabled = true;
        } else {
            isDeleteenabled = false;

        }

        String username = jsonObject.optString("name");
        String caption = jsonObject.optString("caption");
        if (caption.length() > 0) {
            setTags(holder.captionText, caption, username, position);
        } else {
            holder.captionText.setText("");
        }

        Glide.with(context).load(jsonObject.optString("profilePic")).apply(Utils.getRequestOptions()).into(holder.userImage);
        holder.userName.setText(username);
        holder.postTime.setText(Utils.getDateFormatted(jsonObject.optString("createdAt")));
        holder.likesCount.setText(jsonObject.optString(ConstantKeys.LIKES_COUNT_KEY));
        holder.commentsCount.setText(jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY));
        JSONArray linkDataArray = new JSONArray();
        linkDataArray = jsonObject.optJSONArray("linkData");
        //            setUpViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);
        setUpTempViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);

        if (jsonObject.optString(ConstantKeys.BOOKMARK_KEY).equalsIgnoreCase("true")) {
            holder.bookmarkButton.setChecked(true);
        } else {
            holder.bookmarkButton.setChecked(false);
        }

        holder.sponsoredLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=jsonObject.optString("websiteUrl");
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(browserIntent);

            }
        });



        if (jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY).equalsIgnoreCase("0")) {
            holder.fullCommentCount.setVisibility(View.GONE);
        } else {
            holder.fullCommentCount.setVisibility(View.VISIBLE);
            holder.fullCommentCount.setText(context.getResources().getString(R.string.view_all) + " " + jsonObject.optString(ConstantKeys.COMMENTS_COUNT_KEY) + " " + context.getResources().getString(R.string.comments));

        }

        holder.commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jsonObject.optString("commentingStatus").equalsIgnoreCase("1")) {
                    moveFullCommentActivity(jsonObject.optString("id"));
                } else {
                    Utils.toast(context,context.getResources().getString(R.string.comments_are_disabled_for_the_post));
                }

            }
        });


        if (jsonObject.optString(ConstantKeys.USER_LIKED_KEY).equalsIgnoreCase("true")) {
            holder.likeButton.setChecked(true);
        } else {
            holder.likeButton.setChecked(false);
        }


        holder.postOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPostOptionsDialog(jsonObject.optString("id"), jsonObject.optString("postedBy"), position, isDeleteenabled);
            }
        });


        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileImageClicked.onClicked(jsonObject.optString("username"));
            }
        });
        holder.bookmarkButton.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean isliked) {
                if (isliked) {
                    try {
                        bookMarkPost(jsonObject.optString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        removeBookMarkPost(jsonObject.optString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean isLiked) {
                holder.bookmarkButton.setEnabled(true);
                if (isLiked) {
                    EventBus.getDefault().post(new onBookmarked(position, "true"));

                } else {
                    EventBus.getDefault().post(new onBookmarked(position, "false"));

                }

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {
                holder.bookmarkButton.setEnabled(false);


            }
        });

        holder.likeButton.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean isliked) {
                if (isliked) {
                    try {
                        likePost(jsonObject.optString("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        unLikePost(jsonObject.optString("id"));
                        EventBus.getDefault().post(new PostLiked(position, "false"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean isliked) {
                holder.likeButton.setEnabled(true);

                if (isliked) {
                    EventBus.getDefault().post(new PostLiked(position, "true"));

                } else {

                }

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

                holder.likeButton.setEnabled(false);
            }
        });


    }

    private void setUpTempViewPager(JSONArray linkDataArray, RecyclerView recyclerView, RecyclerViewIndicator circleIndicator) {

        NewsFeedMediaAdapterTemp newsFeedMediaAdapter = new NewsFeedMediaAdapterTemp(context, linkDataArray);
        SnapHelper snapHelper = new PagerSnapHelper();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        try {
            snapHelper.attachToRecyclerView(recyclerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
//
        recyclerView.setAdapter(newsFeedMediaAdapter);
        circleIndicator.setRecyclerView(recyclerView);
        circleIndicator.forceUpdateItemCount();

        if (linkDataArray.length() < 2) {
            circleIndicator.setVisibility(View.GONE);
        } else {
            circleIndicator.setVisibility(View.VISIBLE);
        }

        newsFeedMediaAdapter.setClickListener(new onRecyclerViewItemClicked() {
            @Override
            public void onClicked(String username) {

                showPlayDialog(username);

            }

            @Override
            public void onClicked(String username, JSONObject jsonObject) {

            }
        });
    }


    private void showPlayDialog(final String username) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.video_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

        final UniversalVideoView mVideoView;
        UniversalMediaController mMediaController;
        mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        dialog.findViewById(R.id.closeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                int cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoView.setLayoutParams(videoLayoutParams);
                mVideoView.start();
                mVideoView.setVideoPath(username);
                mVideoView.requestFocus();

            }
        });


    }

    private void setUpViewPager(JSONArray linkDataArray, AAH_CustomRecyclerView recyclerView, RecyclerViewIndicator circleIndicator) throws JSONException {
        NewsFeedMediaAdapter newsFeedMediaAdapter = new NewsFeedMediaAdapter(linkDataArray, context);
        SnapHelper snapHelper = new PagerSnapHelper();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        try {
            snapHelper.attachToRecyclerView(recyclerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.setPlayOnlyFirstVideo(true); // false by default
        recyclerView.setVisiblePercent(10);
        recyclerView.setActivity(context);
        recyclerView.setAdapter(newsFeedMediaAdapter);
        circleIndicator.setRecyclerView(recyclerView);
// If you need to change the adapter size, you should call this function
        circleIndicator.forceUpdateItemCount();

        if (linkDataArray.length() < 2) {
            circleIndicator.setVisibility(View.GONE);
        } else {
            circleIndicator.setVisibility(View.VISIBLE);

        }


    }

    private void moveFullCommentActivity(String id) {
        Intent intent = new Intent(context, FullCommentActivity.class);
        intent.putExtra("postId", id);
        context.startActivity(intent);
    }

    private void unLikePost(String id) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("postId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.UN_LIKE_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }

    private void likePost(String id) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("postId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.LIKE_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }

    private void removeBookMarkPost(String id) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("postId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.BOOKMARK_POST_REMOVE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }

    private void bookMarkPost(String id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("postId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.BOOKMARK_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    private void setTags(TextView pTextView, String pTagString, String username, final int caption_tags) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#' || pTagString.charAt(i) == '@') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    Log.d(TAG, "setTags: " + tag);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            Log.d(TAG, "onClick: " + tag);
                            if (tag.startsWith("#")) {


                                JSONArray capArray = jsonArray.optJSONObject(caption_tags).optJSONArray("caption_tags");

                                for (int i = 0; i < capArray.length(); i++) {
                                    Log.d(TAG, "goingInside" + capArray.optJSONObject(i).optString("tagName") + "/" + tag);
                                    String checkvalue = capArray.optJSONObject(i).optString("tagName");
                                    if (checkvalue.trim().equalsIgnoreCase(tag.trim())) {
                                        Log.d(TAG, "onClick: Happened");
                                        moveToHashPage(tag, capArray.optJSONObject(i).optString("tagId"));
                                    }
                                }


                            } else if (tag.startsWith("@")) {
                                moveToMentionPage(tag, caption_tags);

                            }
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#33b5e5"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        SpannableString ss_caption = new SpannableString(username);

        ss_caption.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.text_black)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        pTextView.setText("");
        pTextView.append(ss_caption);
        pTextView.append(" ");
        pTextView.append(string);
    }

    private void moveToMentionPage(String username, int caption_tags) {
//        Toast.makeText(context, ""+username, Toast.LENGTH_SHORT).show();
        onMentionClicked.onClicked(removeFirstChar(username), caption_tags);

    }

    public String removeFirstChar(String s) {
        return s.substring(1);
    }

    private void moveToHashPage(String hashTag, String caption_tags) {
//        Toast.makeText(context, ""+hashTag, Toast.LENGTH_SHORT).show();
        onHashTagClicked.onClicked(removeFirstChar(hashTag), caption_tags);

    }

    public void setOnBottomReachedListener(onBottomReachedListner onBottomReachedListner) {

        this.onBottomReachedListner = onBottomReachedListner;
    }

    public void setOnProfileImageClicked(onProfileImageClicked onProfileImageClicked) {

        this.onProfileImageClicked = onProfileImageClicked;
    }

    public void setOnHashTagClickListener(onHashTagClicked onHashTagClicked) {

        this.onHashTagClicked = onHashTagClicked;
    }

    public void setOnMentionClicked(onMentionClicked onMentionClicked) {

        this.onMentionClicked = onMentionClicked;
    }

    class MyClickableSpan extends ClickableSpan {
        String text;

        MyClickableSpan(String text) {
            this.text = text;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(false); // get rid of underlining
            ds.setColor(Color.RED);     // make links red
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView captionText, userLocation, userName, postTime, likesCount, commentsCount, fullCommentCount,sponsoredText;
        ImageView postOption;
        ImageView postImage;
        RelativeLayout sponsoredLayout;
        SparkButton bookmarkButton, likeButton;
        CircleImageView userImage;
        RecyclerViewIndicator circleIndicator;
        //        AAH_CustomRecyclerView mediaPager;
        RecyclerView mediaPager;
        LinearLayout commentLayout;

        public ViewHolder(View view) {
            super(view);
            captionText = view.findViewById(R.id.captionText);
            postImage = view.findViewById(R.id.postImage);
            userImage = view.findViewById(R.id.userImage);
            userLocation = view.findViewById(R.id.userLocation);
            postTime = view.findViewById(R.id.postTime);
            userName = view.findViewById(R.id.userName);
            bookmarkButton = view.findViewById(R.id.bookmarkButton);
            likeButton = view.findViewById(R.id.likeButton);
            postOption = view.findViewById(R.id.postOption);
            likesCount = view.findViewById(R.id.likesCount);
            commentsCount = view.findViewById(R.id.commentsCount);
            fullCommentCount = view.findViewById(R.id.fullCommentCount);
            mediaPager = view.findViewById(R.id.mediaPager);
            circleIndicator = view.findViewById(R.id.circleIndicator);
            commentLayout = view.findViewById(R.id.commentLayout);
            sponsoredText = view.findViewById(R.id.sponsoredText);
            sponsoredLayout = view.findViewById(R.id.sponsoredLayout);

            captionText.setMovementMethod(LinkMovementMethod.getInstance());

        }
    }


}

