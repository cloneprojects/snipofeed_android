package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {


    JSONArray jsonArray;
    int type;
    private Context context;
    private String TAG = SearchListAdapter.class.getSimpleName();
    onRecyclerViewItemClicked onRecyclerViewItemClicked;


    public SearchListAdapter(Context context, JSONArray jsonArray, int type) {
        Log.d(TAG, "SearchListAdapter: "+jsonArray);
        this.context = context;
        this.jsonArray = jsonArray;
        this.type = type;


    }

    @NonNull
    @Override
    public SearchListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_recycler_view_item, viewGroup, false);
        return new ViewHolder(view);
    }

    public void setClickListeners(onRecyclerViewItemClicked onRecyclerViewItemClicked)
    {
        this.onRecyclerViewItemClicked=onRecyclerViewItemClicked;
    }
    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(SearchListAdapter.ViewHolder holder, final int position) {
        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d(TAG, "onBindViewHolder: "+jsonObject);
        String title = "", subTitle = "";
        if (type == ConstantKeys.isPeople) {
            title = jsonObject.optString("name");
            subTitle = jsonObject.optString("username");
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_dummy_profile);
            requestOptions.error(R.drawable.ic_dummy_profile);
            Glide.with(context).load(jsonObject.optString("profilePic")).apply(requestOptions).into(holder.imageIcon);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclerViewItemClicked.onClicked(jsonObject.optString("username"));
                }
            });

        } else if (type == ConstantKeys.isPlaces) {
            title = jsonObject.optString("area");
            subTitle = jsonObject.optString("city");
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.location_item)).into(holder.imageIcon);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclerViewItemClicked.onClicked(jsonObject.optString("area")+"/"+jsonObject.optString("city"));
                }
            });

        } else if (type == ConstantKeys.isHashtags) {
            title = jsonObject.optString("tagName");
            if (Integer.parseInt(jsonObject.optString("post_count")) > 1) {
                subTitle = jsonObject.optString("post_count") + " " + context.getResources().getString(R.string.posts);

            } else {
                subTitle = jsonObject.optString("post_count") + " " + context.getResources().getString(R.string.post);


            }
            Glide.with(context).load(context.getResources().getDrawable(R.drawable.hash_tag_item)).into(holder.imageIcon);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclerViewItemClicked.onClicked(jsonObject.optString("id"));
                }
            });
        }

        holder.titleText.setText(title);
        holder.subTitleText.setText(subTitle);


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imageIcon;
        TextView titleText, subTitleText;


        public ViewHolder(View view) {
            super(view);
            imageIcon = view.findViewById(R.id.imageIcon);
            titleText = view.findViewById(R.id.titleText);
            subTitleText = view.findViewById(R.id.subTitleText);

        }
    }

}
