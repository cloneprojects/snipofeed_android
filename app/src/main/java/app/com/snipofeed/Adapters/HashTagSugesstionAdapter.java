package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Events.HashTagSelected;
import app.com.snipofeed.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class HashTagSugesstionAdapter extends RecyclerView.Adapter<HashTagSugesstionAdapter.ViewHolder> {

    JSONArray jsonArray;
    int start;
    int count;
    String type;
    private Context context;

    public HashTagSugesstionAdapter(Context context, JSONArray jsonArray, int start, int count, String type) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.start = start;
        this.count = count;
        this.type = type;

    }

    @NonNull
    @Override
    public HashTagSugesstionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        if (type.equalsIgnoreCase("hash")) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hash_tag_suggestion_item, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mention_suggestion_item, viewGroup, false);

        }
        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(HashTagSugesstionAdapter.ViewHolder holder, final int position) {

        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        if (type.equalsIgnoreCase("hash")) {
            holder.tagName.setText(jsonObject.optString("tagName"));
            if (Integer.parseInt(jsonObject.optString("post_count")) > 1) {
                holder.postCount.setText(jsonObject.optString("post_count") + " " + context.getResources().getString(R.string.posts));

            } else {
                holder.postCount.setText(jsonObject.optString("post_count") + " " + context.getResources().getString(R.string.post));

            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new HashTagSelected(start, count, jsonObject.optString("id"), jsonObject.optString("tagName")));
                }
            });
        } else {
            holder.fullName.setText(jsonObject.optString("name"));
            holder.userName.setText(jsonObject.optString("username"));

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_dummy_profile);
            requestOptions.error(R.drawable.ic_dummy_profile);
            Glide.with(context).load(jsonObject.optString("profilePic")).into(holder.userPic);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new HashTagSelected(start, count, jsonObject.optString("id"), "@"+jsonObject.optString("username")));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tagName, postCount;
        TextView fullName, userName;
        CircleImageView userPic;

        public ViewHolder(View view) {
            super(view);
            tagName = view.findViewById(R.id.tagName);
            postCount = view.findViewById(R.id.postCount);
            fullName = view.findViewById(R.id.fullName);
            userName = view.findViewById(R.id.userName);
            userPic = view.findViewById(R.id.userPic);

        }
    }

}
