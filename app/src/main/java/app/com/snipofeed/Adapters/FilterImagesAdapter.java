package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class FilterImagesAdapter extends RecyclerView.Adapter<FilterImagesAdapter.ViewHolder> {


    ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();

    private Activity context;
    private String TAG = FilterImagesAdapter.class.getSimpleName();
    onRecyclerViewItemClicked onRecyclerViewItemClicked;


    public FilterImagesAdapter(Activity context, ArrayList<Bitmap> bitmapArray) {
        this.context = context;
        this.bitmapArray = bitmapArray;



    }

    @NonNull
    @Override
    public FilterImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.filter_top_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final FilterImagesAdapter.ViewHolder holder, final int position) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Drawable verticalImage = new BitmapDrawable( bitmapArray.get(position));

                holder.postImage.setImageBitmap(bitmapArray.get(position));
//                holder.postImage.setImageDrawable(verticalImage);

            }
        });


    }

    public ArrayList<Bitmap> getBitmapArray() {
        return bitmapArray;
    }

    @Override
    public int getItemCount() {
        return bitmapArray.size();
    }

    public void changeBitmap(Bitmap bitmap, int pageSelected) {
        bitmapArray.remove(pageSelected);
        bitmapArray.add(pageSelected,bitmap);
        notifyItemChanged(pageSelected);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView postImage;


        public ViewHolder(View view) {
            super(view);
            postImage = view.findViewById(R.id.postImage);


        }
    }

}
