package app.com.snipofeed.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.RecyclerViewIndicator;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Videhandler.UniversalMediaController;
import app.com.snipofeed.Videhandler.UniversalVideoView;

/**
 * Created by yuvaraj on 02/05/18.
 */

public class PromotionFeedTopAdapter extends RecyclerView.Adapter<PromotionFeedTopAdapter.ViewHolder> {


    JSONArray jsonArray;
    AppSettings appSettings;
    Dialog dialog;
    FragmentManager fragmentManager;
    private Activity context;
    private onRecyclerViewItemClicked onRecyclerViewItemClicked;
    private String TAG = PromotionFeedTopAdapter.class.getSimpleName();

    public PromotionFeedTopAdapter(Activity context, JSONArray jsonArray, FragmentManager supportFragmentManager) {
        this.context = context;
        appSettings = new AppSettings(context);
        this.jsonArray = jsonArray;
        this.fragmentManager = supportFragmentManager;
    }

    public void setListeners(onRecyclerViewItemClicked onRecyclerViewItemClicked) {
        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;
    }

    @NonNull
    @Override
    public PromotionFeedTopAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.promotion_item_lists, viewGroup, false);
        return new PromotionFeedTopAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final PromotionFeedTopAdapter.ViewHolder holder, final int position) {


        JSONArray linkDataArray = new JSONArray();
        linkDataArray = jsonArray.optJSONObject(position).optJSONArray("linkData");
        //            setUpViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);
//        setUpTempViewPager(linkDataArray, holder.mediaPager, holder.circleIndicator);
        Glide.with(context).load(linkDataArray.optJSONObject(0).optString("linkData")).into(holder.thumbNailImage);
        if (linkDataArray.optJSONObject(0).optString("type").equalsIgnoreCase("video")) {
            holder.videoIcon.setVisibility(View.VISIBLE);
            holder.isMultiple.setVisibility(View.GONE);
        } else {
            holder.videoIcon.setVisibility(View.GONE);
            if (linkDataArray.length() > 1) {
                holder.isMultiple.setVisibility(View.VISIBLE);
            } else {
                holder.isMultiple.setVisibility(View.GONE);

            }

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewItemClicked.onClicked("", jsonArray.optJSONObject(position));
            }
        });

    }

    private void setUpTempViewPager(JSONArray linkDataArray, RecyclerView recyclerView, RecyclerViewIndicator circleIndicator) {

        NewsFeedMediaAdapterTemp newsFeedMediaAdapter = new NewsFeedMediaAdapterTemp(context, linkDataArray);
        SnapHelper snapHelper = new PagerSnapHelper();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        try {
            snapHelper.attachToRecyclerView(recyclerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
//
        recyclerView.setAdapter(newsFeedMediaAdapter);
        circleIndicator.setRecyclerView(recyclerView);
        circleIndicator.forceUpdateItemCount();

        if (linkDataArray.length() < 2) {
            circleIndicator.setVisibility(View.GONE);
        } else {
            circleIndicator.setVisibility(View.VISIBLE);
        }

        newsFeedMediaAdapter.setClickListener(new onRecyclerViewItemClicked() {
            @Override
            public void onClicked(String username) {

                showPlayDialog(username);

            }

            @Override
            public void onClicked(String username, JSONObject jsonObject) {

            }
        });
    }


    private void showPlayDialog(final String username) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.video_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

        final UniversalVideoView mVideoView;
        UniversalMediaController mMediaController;
        mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        dialog.findViewById(R.id.closeIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                int cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoView.setLayoutParams(videoLayoutParams);
                mVideoView.start();
                mVideoView.setVideoPath(username);
                mVideoView.requestFocus();

            }
        });


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public String removeFirstChar(String s) {
        return s.substring(1);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewIndicator circleIndicator;
        ImageView thumbNailImage;
        RecyclerView mediaPager;
        ImageView videoIcon, isMultiple;
        LinearLayout commentLayout;

        public ViewHolder(View view) {
            super(view);

            mediaPager = view.findViewById(R.id.mediaPager);
            circleIndicator = view.findViewById(R.id.circleIndicator);
            commentLayout = view.findViewById(R.id.commentLayout);
            thumbNailImage = view.findViewById(R.id.thumbNailImage);
            isMultiple = view.findViewById(R.id.isMultiple);
            videoIcon = view.findViewById(R.id.videoIcon);


        }
    }


}

