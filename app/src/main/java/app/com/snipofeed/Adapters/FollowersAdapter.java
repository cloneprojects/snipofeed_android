package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.ViewHolder> {


    JSONArray jsonArray;
    AppSettings appSettings;
    private Activity context;
    private String TAG = FollowersAdapter.class.getSimpleName();
    private onProfileImageClicked onProfileImageClicked;


    public FollowersAdapter(Activity context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        appSettings = new AppSettings(context);
    }


    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }

    public void setOnProfileImageClicked(onProfileImageClicked onProfileImageClicked) {

        this.onProfileImageClicked = onProfileImageClicked;
    }


    @NonNull
    @Override
    public FollowersAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.followers_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final FollowersAdapter.ViewHolder holder, final int position) {


        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d(TAG, "onBindViewHolder: " + jsonObject);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);


        Glide.with(context).load(jsonObject.optString("profilePic")).apply(requestOptions).into(holder.userIcon);
        holder.nameText.setText(jsonObject.optString("name"));
        holder.userNameText.setText(jsonObject.optString("username"));
        holder.userIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileImageClicked.onClicked(jsonObject.optString("username"));
            }
        });


        if (jsonObject.optString("username").equalsIgnoreCase(appSettings.getUserName())) {
            holder.followButton.setVisibility(View.GONE);
            holder.followingButton.setVisibility(View.GONE);
        } else {
            if (jsonObject.optString("user_follow_status").equalsIgnoreCase("true")) {
                holder.followingButton.setVisibility(View.VISIBLE);
                holder.followButton.setVisibility(View.GONE);
            } else {
                holder.followingButton.setVisibility(View.GONE);
                holder.followButton.setVisibility(View.VISIBLE);
            }
        }


        holder.followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    followUser(jsonObject.optString("id"), holder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.followingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUnFollowDialog(jsonObject.optString("username"), jsonObject.optString("profilePic"), jsonObject.optString("id"), holder);
            }
        });


    }

    private void showUnFollowDialog(String userNameee, String userImageee, final String userId, final ViewHolder holder) {

        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.unfollow_confirmation_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView cancelButton, unfollowButton, userName;
        CircleImageView userImage;
        cancelButton = dialog.findViewById(R.id.cancelButton);
        userImage = dialog.findViewById(R.id.userImage);
        unfollowButton = dialog.findViewById(R.id.unfollowButton);
        userName = dialog.findViewById(R.id.userName);
        userName.setText(context.getString(R.string.unfollow) + " @" + userNameee);
        Glide.with(context).load(userImageee).apply(Utils.getRequestOptions()).into(userImage);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        unfollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    unFollowUser(dialog, userId, holder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void followUser(String userId, final ViewHolder holder) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(context, UrlHelper.FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                changeTofollow(holder);

            }
        });

    }

    private void changeTofollow(ViewHolder holder) {
        holder.followingButton.setVisibility(View.VISIBLE);
        holder.followButton.setVisibility(View.GONE);
    }

    private void unFollowUser(final Dialog dialog, String userId, final ViewHolder holder) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(context, UrlHelper.UN_FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                changeToUnfollow(holder);

            }
        });
    }

    private void changeToUnfollow(ViewHolder holder) {
        holder.followingButton.setVisibility(View.GONE);
        holder.followButton.setVisibility(View.VISIBLE);

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userIcon;
        TextView userNameText;
        TextView nameText;
        ImageView followButton, followingButton;


        public ViewHolder(View view) {
            super(view);
            userNameText = view.findViewById(R.id.userNameText);
            nameText = view.findViewById(R.id.nameText);
            userIcon = view.findViewById(R.id.userIcon);
            followingButton = view.findViewById(R.id.followingButton);
            followButton = view.findViewById(R.id.followButton);

        }
    }

}
