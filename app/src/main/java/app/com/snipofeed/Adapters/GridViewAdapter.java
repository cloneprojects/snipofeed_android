package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Videhandler.UniversalMediaController;
import app.com.snipofeed.Videhandler.UniversalVideoView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.ViewHolder> {


    JSONArray jsonArray;
    private Context context;
    private onBottomReachedListner onBottomReachedListner;
    private onRecyclerViewItemClicked onRecyclerViewItemClicked;
    private String TAG = GridViewAdapter.class.getSimpleName();
    private Dialog dialog;
    RecyclerRefreshLayout recyclerRefreshLayout;

    public GridViewAdapter(Context context, JSONArray jsonArray, RecyclerRefreshLayout recyclerRefreshLayout) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.recyclerRefreshLayout = recyclerRefreshLayout;

    }

    public void setVisibility(boolean isVisible, View itemView) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        if (isVisible) {
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        } else {
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }

    public void setOnBottomReachedListener(onBottomReachedListner onBottomReachedListner) {

        this.onBottomReachedListner = onBottomReachedListner;
    }


    public void setOnRecyclerViewItemClicked(onRecyclerViewItemClicked onRecyclerViewItemClicked) {

        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;
    }


    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public GridViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_feed_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final GridViewAdapter.ViewHolder holder, final int position) {

        if (jsonArray.length() > 8) {
            if (position == jsonArray.length() - 1) {
                onBottomReachedListner.onBottomReached(position);
            }
        }


        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d(TAG, "onBindViewHolder: " + jsonObject);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);

        if (jsonObject.optJSONArray("linkData").length() > 1) {
            Glide.with(context).load(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData")).into(holder.postImage);

            holder.isMultiple.setVisibility(View.VISIBLE);
//            setVisibility(true,holder.itemView);

        } else if (jsonObject.optJSONArray("linkData").length() == 1) {
            Glide.with(context).load(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData")).apply(requestOptions).into(holder.postImage);

//            setVisibility(true,holder.itemView);

            holder.isMultiple.setVisibility(View.GONE);
        } else {
//            setVisibility(false,holder.itemView);
            holder.isMultiple.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewItemClicked.onClicked(jsonObject.optString("username"), jsonObject);

            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AppSettings appSettings=new AppSettings(context);
                appSettings.setCanScroll("false");
                recyclerRefreshLayout.setEnabled(false);

                Log.d(TAG, "onLongClick: " + jsonObject);

                if (jsonObject.optJSONArray("linkData").optJSONObject(0).optString("type").equalsIgnoreCase("video")) {
                    showPlayDialog(jsonObject);
                } else {

                    showFullDialog(jsonObject);
                }

                return false;
            }
        });

        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                holder.itemView.onTouchEvent(event);
                Log.d(TAG, "onTouch: "+event.getAction());
                // We're only interested in when the button is released.
                AppSettings appSettings=new AppSettings(context);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    appSettings.setCanScroll("true");
                    recyclerRefreshLayout.setEnabled(true);

                    try {
                        dialog.dismiss();
                    } catch (Exception e) {

                    }
                }
                if (appSettings.getCanScroll().equalsIgnoreCase("false")) {
                    if (event.getAction() == MotionEvent.ACTION_MOVE
                            ) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);

                    } else {
                        v.getParent().requestDisallowInterceptTouchEvent(false);

                    }
                } else {
                    v.getParent().requestDisallowInterceptTouchEvent(false);

                }

                return true;
            }
        });
    }

    private void showFullDialog(JSONObject jsonObject) {

        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.profile_pop_up);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        CardView parentView = dialog.findViewById(R.id.parentView);
        parentView.requestFocus();
        TextView userName, userLocation;
        CircleImageView userImage;
        ImageView postImage;
        userName = dialog.findViewById(R.id.userName);
        userLocation = dialog.findViewById(R.id.userLocation);
        userImage = dialog.findViewById(R.id.userImage);
        postImage = dialog.findViewById(R.id.postImage);
        userName.setText(jsonObject.optString("name"));
        Glide.with(context).load(jsonObject.optString("profilePic")).into(userImage);
        if (userName.getText().toString().length() == 0) {
            userName.setVisibility(View.GONE);
        } else {
            userName.setVisibility(View.VISIBLE);

        }
        Log.d(TAG, "showFullDialog: " + jsonObject);
        Glide.with(context).load(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData")).into(postImage);
        userLocation.setText(jsonObject.optString("area"));
        dialog.findViewById(R.id.editText).requestFocus();


    }


    private void showPlayDialog(final JSONObject jsonObject) {
        dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.profile_video_pop_up);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

        CardView parentView = dialog.findViewById(R.id.parentView);
        parentView.requestFocus();
        TextView userName, userLocation;
        CircleImageView userImage;
        ImageView postImage;
        userName = dialog.findViewById(R.id.userName);
        userLocation = dialog.findViewById(R.id.userLocation);
        userImage = dialog.findViewById(R.id.userImage);
        postImage = dialog.findViewById(R.id.postImage);
        userName.setText(jsonObject.optString("name"));
        Glide.with(context).load(jsonObject.optString("profilePic")).into(userImage);
        if (userName.getText().toString().length() == 0) {
            userName.setVisibility(View.GONE);
        } else {
            userName.setVisibility(View.VISIBLE);

        }
        userLocation.setText(jsonObject.optString("area"));


        final UniversalVideoView mVideoView;
        UniversalMediaController mMediaController;
        mVideoView = (UniversalVideoView) dialog.findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) dialog.findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        mMediaController.setVisibility(View.GONE);

        mVideoView.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoView.getWidth();
                int cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoView.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoView.setLayoutParams(videoLayoutParams);
                mVideoView.start();
                mVideoView.setVideoPath(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData"));
                mVideoView.requestFocus();

            }
        });


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView postImage;
        ImageView isMultiple;


        public ViewHolder(View view) {
            super(view);
            postImage = view.findViewById(R.id.postImage);
            isMultiple = view.findViewById(R.id.isMultiple);

        }
    }

}
