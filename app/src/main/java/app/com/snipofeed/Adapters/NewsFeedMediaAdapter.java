package app.com.snipofeed.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.allattentionhere.autoplayvideos.AAH_CustomViewHolder;
import com.allattentionhere.autoplayvideos.AAH_VideosAdapter;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.R;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class NewsFeedMediaAdapter extends AAH_VideosAdapter {

    JSONArray jsonArray;
    Context context;


    public class MyViewHolder extends AAH_CustomViewHolder {

        public MyViewHolder(View x) {
            super(x);

        }
    }

    public NewsFeedMediaAdapter(JSONArray jsonArray, Context context) {
        this.jsonArray=jsonArray;
        this.context=context;
    }

    @Override
    public AAH_CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_feed_video, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AAH_CustomViewHolder holder, int position) {
        JSONObject jsonObject=jsonArray.optJSONObject(position);


        if (jsonObject.optString("type").equalsIgnoreCase("video"))
        {
            holder.setVideoUrl(jsonObject.optString("linkData"));
//            holder.setImageUrl(jsonObject.optString("thumbnail"));
//            Glide.with(context).load(jsonObject.optString("linkData")).into(holder.getAAH_ImageView());
        } else {
            holder.setImageUrl(jsonObject.optString("linkData"));
            Glide.with(context).load(holder.getImageUrl()).into(holder.getAAH_ImageView());
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


}