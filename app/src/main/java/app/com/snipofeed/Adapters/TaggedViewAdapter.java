package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.R;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class TaggedViewAdapter extends RecyclerView.Adapter<TaggedViewAdapter.ViewHolder> {


    JSONArray jsonArray;
    private Context context;
    private onBottomReachedListner onBottomReachedListner;
    private String TAG = TaggedViewAdapter.class.getSimpleName();

    public TaggedViewAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    public void setVisibility(boolean isVisible,View itemView){
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams)itemView.getLayoutParams();
        if (isVisible){
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        }else{
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }

    public void setOnBottomReachedListener(onBottomReachedListner onBottomReachedListner) {

        this.onBottomReachedListner = onBottomReachedListner;
    }


    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }



    @NonNull
    @Override
    public TaggedViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_feed_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(TaggedViewAdapter.ViewHolder holder, final int position) {

        if (jsonArray.length() > 8) {
            if (position == jsonArray.length() - 1) {
                onBottomReachedListner.onBottomReached(position);
            }
        }



        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);


        if (jsonObject.optJSONArray("linkData").length() > 1) {
            Glide.with(context).load(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData")).into(holder.postImage);

            holder.isMultiple.setVisibility(View.VISIBLE);
//            setVisibility(true,holder.itemView);

        } else if (jsonObject.optJSONArray("linkData").length() == 1) {
            Glide.with(context).load(jsonObject.optJSONArray("linkData").optJSONObject(0).optString("linkData")).apply(requestOptions).into(holder.postImage);

//            setVisibility(true,holder.itemView);

            holder.isMultiple.setVisibility(View.GONE);
        } else {
//            setVisibility(false,holder.itemView);
            holder.isMultiple.setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView postImage;
        ImageView isMultiple;


        public ViewHolder(View view) {
            super(view);
            postImage = view.findViewById(R.id.postImage);
            isMultiple = view.findViewById(R.id.isMultiple);

        }
    }

}
