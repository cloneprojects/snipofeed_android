package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.onHashTagSuggestionItemClicked;
import app.com.snipofeed.R;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class HashTagSugesstionAdapterHorizontal extends RecyclerView.Adapter<HashTagSugesstionAdapterHorizontal.ViewHolder> {

    JSONArray jsonArray;
    private Context context;
    onHashTagSuggestionItemClicked onHashTagSuggestionItemClicked;

    public HashTagSugesstionAdapterHorizontal(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    @NonNull
    @Override
    public HashTagSugesstionAdapterHorizontal.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.hash_tag_suggestion_item_horizontal, viewGroup, false);

        return new ViewHolder(view);
    }

    public void setClickListener(onHashTagSuggestionItemClicked onHashTagSuggestionItemClicked)
    {
        this.onHashTagSuggestionItemClicked=onHashTagSuggestionItemClicked;

    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(HashTagSugesstionAdapterHorizontal.ViewHolder holder, final int position) {

        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        if (position == 0) {
            holder.releatedText.setVisibility(View.VISIBLE);
        } else {
            holder.releatedText.setVisibility(View.GONE);

        }

        holder.tagName.setText(jsonObject.optString("tagName"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHashTagSuggestionItemClicked.Clicked(jsonObject.optString("id"));
            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tagName, releatedText;

        public ViewHolder(View view) {
            super(view);
            tagName = view.findViewById(R.id.tagName);
            releatedText = view.findViewById(R.id.releatedText);


        }
    }

}
