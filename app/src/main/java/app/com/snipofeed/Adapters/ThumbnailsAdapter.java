package app.com.snipofeed.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mmin18.widget.RealtimeBlurView;
import com.zomato.photofilters.utils.ThumbnailCallback;
import com.zomato.photofilters.utils.ThumbnailItem;

import java.util.List;

import app.com.snipofeed.R;

/**
 * @author Varun on 01/07/15.
 */
public class ThumbnailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "THUMBNAILS_ADAPTER";
    private static int lastPosition = -1;
    private ThumbnailCallback thumbnailCallback;
    private List<ThumbnailItem> dataSet;
    int size;

    public ThumbnailsAdapter(List<ThumbnailItem> dataSet, ThumbnailCallback thumbnailCallback, int size) {
        Log.v(TAG, "Thumbnails Adapter has " + dataSet.size() + " items");
        this.dataSet = dataSet;
        this.size = size;
        this.thumbnailCallback = thumbnailCallback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.v(TAG, "On Create View Holder Called");
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.filter_items, viewGroup, false);
        return new ThumbnailsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ThumbnailsViewHolder thumbnailsViewHolder = (ThumbnailsViewHolder) holder;

        if (position == 0) {


            final ThumbnailItem thumbnailItem = dataSet.get(position);
            thumbnailsViewHolder.thumbnail.setImageBitmap(thumbnailItem.image);
            thumbnailsViewHolder.thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
            thumbnailsViewHolder.filterName.setText(thumbnailItem.filterName);
            thumbnailsViewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    thumbnailCallback.onThumbnailClick(thumbnailItem.filter);

                }

            });

            if (size > 1) {
                thumbnailsViewHolder.blurView.setVisibility(View.VISIBLE);
                thumbnailsViewHolder.filterletterName.setVisibility(View.VISIBLE);
                thumbnailsViewHolder.filterletterName.setText("" + thumbnailItem.filterName.charAt(0));
            } else {
                thumbnailsViewHolder.blurView.setVisibility(View.GONE);
                thumbnailsViewHolder.filterletterName.setVisibility(View.GONE);
            }


        } else {

            final ThumbnailItem thumbnailItem = dataSet.get(position);
            Log.v(TAG, "On Bind View Called");
            thumbnailsViewHolder.thumbnail.setImageBitmap(thumbnailItem.image);
            thumbnailsViewHolder.thumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
            thumbnailsViewHolder.filterName.setText(thumbnailItem.filterName);
            thumbnailsViewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastPosition != position) {
                        thumbnailCallback.onThumbnailClick(thumbnailItem.filter);
                        lastPosition = position;
                    }
                }

            });

            if (size > 1) {
                thumbnailsViewHolder.blurView.setVisibility(View.VISIBLE);
                thumbnailsViewHolder.filterletterName.setVisibility(View.VISIBLE);
                thumbnailsViewHolder.filterletterName.setText("" + thumbnailItem.filterName.charAt(0));
            } else {
                thumbnailsViewHolder.blurView.setVisibility(View.GONE);
                thumbnailsViewHolder.filterletterName.setVisibility(View.GONE);
            }
        }


    }

//    private void setAnimation(View viewToAnimate, int position) {
//        {
//            ViewHelper.setAlpha(viewToAnimate, .0f);
//            com.nineoldandroids.view.ViewPropertyAnimator.animate(viewToAnimate).alpha(1).setDuration(250).start();
//            lastPosition = position;
//        }
//    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ThumbnailsViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView filterName;
        public TextView filterletterName;
        RealtimeBlurView blurView;

        public ThumbnailsViewHolder(View v) {
            super(v);
            this.thumbnail = (ImageView) v.findViewById(R.id.thumbnail);
            this.filterName = v.findViewById(R.id.filterName);
            this.blurView = v.findViewById(R.id.blurView);
            this.filterletterName = v.findViewById(R.id.filterletterName);
        }
    }
}
