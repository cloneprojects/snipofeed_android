package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.varunest.sparkbutton.SparkButton;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Events.ReplyToEvent;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class CommentsReplyAdapter extends RecyclerView.Adapter<CommentsReplyAdapter.ViewHolder> {


    JSONArray jsonArray;
    private Context context;
    private String TAG = CommentsReplyAdapter.class.getSimpleName();

    public CommentsReplyAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    @NonNull
    @Override
    public CommentsReplyAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comments_item_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(CommentsReplyAdapter.ViewHolder holder, final int position) {
        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);
        Glide.with(context).load(jsonObject.optString("profilePic")).into(holder.userImage);
        try {
            holder.commentTime.setText(Utils.getDateFormatted(jsonObject.optString("repliedAt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTextValue(jsonObject.optString("name"), jsonObject.optString("reply"), holder.commentText);
        int commentLikes = 0;
        try {
            commentLikes = Integer.parseInt(jsonObject.optString("comment_likes"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (commentLikes > 0) {
            holder.likesCount.setText(commentLikes + " Likes");
        } else {
            holder.likesCount.setVisibility(View.GONE);

        }


        holder.replyOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new ReplyToEvent(jsonObject.optString("id"), jsonObject.optString("name"), Integer.parseInt(jsonObject.optString("parentPos"))));

            }
        });


    }

    public void setTextValue(String username, String text, TextView textView) {
        Log.d("test", "Custom set text");

        SpannableString ss = new SpannableString(text);
        SpannableString ss_caption = new SpannableString(username);

        ss_caption.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // Splitting the words by spaces
        String[] words = text.split(" ");
        Log.d(TAG, "setTextValue: " + words.length);
        for (String word : words) {
            Log.d("test", word);
            word = word.replace("\n", "");
            if (word.startsWith("@") && word.length() >= 2) {
                // word is a mention
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                final String finalWord3 = word;
                Log.e("test", "" + finalWord3);
                ClickableSpan mentionClickSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Log.d(TAG, "onClick: " + finalWord3);
                    }
                };
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(mentionClickSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Mention matched" + startIndex + " : " + endIndex);
            } else if (word.startsWith("#") && word.length() >= 2) {
                // word is a hash tag
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                final String finalWord4 = word;

                ClickableSpan mentionClickSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Log.d(TAG, "onClick: " + finalWord4);
                    }
                };
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(mentionClickSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Hash tag matched" + startIndex + " : " + endIndex);
            }
            Log.d(TAG, "setTextValue: Finsied");
        }

        textView.setText("");
        textView.append(ss_caption);
        textView.append(" ");
        textView.append(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView commentText, commentTime, likesCount, replyOption;
        SparkButton likeButton;


        public ViewHolder(View view) {
            super(view);
            userImage = view.findViewById(R.id.userImage);
            replyOption = view.findViewById(R.id.replyOption);
            likeButton = view.findViewById(R.id.likeButton);
            commentText = view.findViewById(R.id.commentText);
            commentTime = view.findViewById(R.id.commentTime);
            likesCount = view.findViewById(R.id.likesCount);
        }
    }

}
