package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.ReplyToEvent;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {


    JSONArray jsonArray;
    JSONObject jsonObject = new JSONObject();
    private Activity context;
    AppSettings appSettings;
    private String TAG = CommentsAdapter.class.getSimpleName();

    public CommentsAdapter(Activity context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        appSettings=new AppSettings(context);

    }



    @NonNull
    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comments_item_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final CommentsAdapter.ViewHolder holder, final int position) {
        final JSONObject jsonObject = jsonArray.optJSONObject(position);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);
        Glide.with(context).load(jsonObject.optString("profilePic")).into(holder.userImage);
        holder.commentTime.setText(Utils.getDateFormatted(jsonObject.optString("commentedAt")));
        setTextValue(jsonObject.optString("name"), jsonObject.optString("comment"), holder.commentText);
        final int commentLikes = Integer.parseInt(jsonObject.optString("comment_likes"));
        if (commentLikes > 0) {
            holder.likesCount.setVisibility(View.VISIBLE);
            holder.likesCount.setText(commentLikes + " Likes");
        } else {
            holder.likesCount.setVisibility(View.GONE);
        }

        JSONArray commentReply = jsonObject.optJSONArray("comment_replyes");

        for(int i=0;i<commentReply.length();i++)
        {
            try {
                commentReply.optJSONObject(i).put("parentPos",""+position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (commentReply.length() > 0) {
            holder.bottomLayout.setVisibility(View.VISIBLE);
            CommentsReplyAdapter commentsReplyAdapter = new CommentsReplyAdapter(context, commentReply);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.commentReplyRecyclerView.setLayoutManager(linearLayoutManager);
            holder.commentReplyRecyclerView.setAdapter(commentsReplyAdapter);


        } else {
            holder.bottomLayout.setVisibility(View.GONE);
        }

        if (jsonObject.optString("user_liked_status").equalsIgnoreCase("true"))
        {
            holder.likeButton.setChecked(true);
        } else {

            holder.likeButton.setChecked(false);
        }

        holder.replyOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new ReplyToEvent(jsonObject.optString("commentId"),jsonObject.optString("name"),position));

            }
        });



        holder.likeButton.setEventListener(new SparkEventListener() {
            @Override
            public void onEvent(ImageView button, boolean isliked) {
                if (isliked) {
                    try {
                        likeComments(jsonObject.optString("commentId"),position);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        unLikeComments(jsonObject.optString("commentId"),position);
                        changeLikeStatus("false",position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onEventAnimationEnd(ImageView button, boolean isliked) {
                holder.likeButton.setEnabled(true);

                if (isliked) {

                    changeLikeStatus("true",position);

                } else {

                }

            }

            @Override
            public void onEventAnimationStart(ImageView button, boolean buttonState) {

                holder.likeButton.setEnabled(false);
            }
        });







//        holder.likeButton.setEventListener(new SparkEventListener() {
//            @Override
//            public void onEvent(ImageView button, boolean isliked) {
//                if (isliked) {
//                    try {
//                        int likeCount= Integer.parseInt(jsonArray.optJSONObject(position).optString("comment_likes"));
//                        likeCount=likeCount+1;
//                        jsonArray.optJSONObject(position).remove("comment_likes");
//                        try {
//                            jsonArray.optJSONObject(position).put("comment_likes",""+likeCount);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    EventBus.getDefault().post(new PostLiked(position, "true"));
//                } else {
//                    int likeCount= Integer.parseInt(jsonArray.optJSONObject(position).optString("comment_likes"));
//                    likeCount=likeCount-1;
//                    jsonArray.optJSONObject(position).remove("comment_likes");
//                    try {
//                        jsonArray.optJSONObject(position).put("comment_likes",""+likeCount);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    notifyDataSetChanged();
//
//
//                    EventBus.getDefault().post(new PostLiked(position, "false"));
//
//                }
//            }
//
//            @Override
//            public void onEventAnimationEnd(ImageView button, boolean buttonState) {
//
//            }
//
//            @Override
//            public void onEventAnimationStart(ImageView button, boolean buttonState) {
//
//
//            }
//        });


    }


    public void changeLikeStatus(String status, int position) {
        Log.d(TAG, "changeLikeStatus: " + status);
        jsonArray.optJSONObject(position).remove(ConstantKeys.USER_LIKED_STATUS_KEY);
        try {
            jsonArray.optJSONObject(position).put(ConstantKeys.USER_LIKED_STATUS_KEY, status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int countValue;
        countValue = Integer.parseInt(jsonArray.optJSONObject(position).optString(ConstantKeys.COMMENTS_LIKES_COUNT_KEY));
        if (status.equalsIgnoreCase("true")) {
            countValue = countValue + 1;
        } else {
            countValue = countValue - 1;
        }
        jsonArray.optJSONObject(position).remove(ConstantKeys.COMMENTS_LIKES_COUNT_KEY);
        try {
            jsonArray.optJSONObject(position).put(ConstantKeys.COMMENTS_LIKES_COUNT_KEY, "" + countValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();

    }


    private void likeComments(String id, final int position) throws Exception {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("commentId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.LIKE_COMMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


            }
        });
    }

    private void unLikeComments(String id, final int position) throws JSONException {


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("commentId", id);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethodNoProgress(context, UrlHelper.UN_LIKE_COMMENT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


            }
        });
    }



    public void setTextValue(String username, String text, TextView textView) {
        Log.d("test", "Custom set text");

        SpannableString ss = new SpannableString(text);
        SpannableString ss_caption = new SpannableString(username);

        ss_caption.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // Splitting the words by spaces
        String[] words = text.split(" ");
        Log.d(TAG, "setTextValue: " + words.length);
        for (String word : words) {
            Log.d("test", word);
            word = word.replace("\n", "");
            if (word.startsWith("@") && word.length() >= 2) {
                // word is a mention
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                final String finalWord3 = word;
                Log.e("test", "" + finalWord3);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Log.d(TAG, "onClick: Clicked");
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Mention matched" + startIndex + " : " + endIndex);
            } else if (word.startsWith("#") && word.length() >= 2) {
                // word is a hash tag
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                final String finalWord4 = word;

                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Log.d(TAG, "onClick: Clicked");
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Hash tag matched" + startIndex + " : " + endIndex);
            }
            Log.d(TAG, "setTextValue: Finsied");
        }

        textView.setText("");
        textView.append(ss_caption);
        textView.append(" ");
        textView.append(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public void addNewObject(JSONObject newlyAddedObj) {
        jsonArray.put(newlyAddedObj);
        notifyDataSetChanged();
    }

    public void addReplyJsonObject(JSONObject newlyAddedObj, int position) {
        jsonArray.optJSONObject(position).optJSONArray("comment_replyes").put(newlyAddedObj);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView commentText, commentTime, likesCount,replyOption;
        SparkButton likeButton;
        RecyclerView commentReplyRecyclerView;
        LinearLayout bottomLayout;


        public ViewHolder(View view) {
            super(view);
            userImage = view.findViewById(R.id.userImage);
            replyOption = view.findViewById(R.id.replyOption);
            likeButton = view.findViewById(R.id.likeButton);
            commentText = view.findViewById(R.id.commentText);
            commentTime = view.findViewById(R.id.commentTime);
            likesCount = view.findViewById(R.id.likesCount);
            commentReplyRecyclerView = view.findViewById(R.id.commentReplyRecyclerView);
            bottomLayout = view.findViewById(R.id.bottomLayout);
        }
    }

}
