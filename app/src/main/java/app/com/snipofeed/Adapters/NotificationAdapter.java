package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onPostImageClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.R;
import app.com.snipofeed.model.Image;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    JSONArray jsonArray;
    private Context context;
    private onBottomReachedListner onBottomReachedListner;
    private onProfileImageClicked onProfileImageClicked;
    private onPostImageClicked onPostImageClicked;
    private String TAG=NotificationAdapter.class.getSimpleName();


    public NotificationAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;


    }

    public void setOnBottomReachedListener(onBottomReachedListner onBottomReachedListner) {

        this.onBottomReachedListner = onBottomReachedListner;
    }

    public void setOnProfileImageClicked(onProfileImageClicked onProfileImageClicked) {

        this.onProfileImageClicked = onProfileImageClicked;
    }


    public void setOnPostImageClicked(onPostImageClicked onPostImageClicked) {

        this.onPostImageClicked = onPostImageClicked;
    }


    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        Log.d(TAG, "onCreateViewHolder: "+jsonArray.optJSONObject(i));
        if (i==0) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_item_follow, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_item_like, viewGroup, false);

        }

        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "NewBindViewChek: "+jsonArray.optJSONObject(position));
        if (jsonArray.length() > 8) {
            if (position == jsonArray.length() - 1) {
                onBottomReachedListner.onBottomReached(position);
            }
        }


        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        String name = jsonObject.optString("name");
        String userImage = jsonObject.optString("userImage");
        String notification= jsonObject.optString("notification");
        setTextValue(name,notification,holder.captionText,Utils.getDateFormatted(jsonObject.optString("DT"),""));
        Glide.with(context).load(userImage).apply(Utils.getRequestOptions()).into(holder.userImage);
        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileImageClicked.onClicked(jsonObject.optString("name"));
            }
        });

        if (jsonArray.optJSONObject(position).optString("notification_type").equalsIgnoreCase("follow")) {
            if (jsonObject.optString("follow_status").equalsIgnoreCase("true"))
            {
                holder.followingButton.setVisibility(View.VISIBLE);
                holder.followButton.setVisibility(View.GONE);
            } else {
                holder.followingButton.setVisibility(View.GONE);
                holder.followButton.setVisibility(View.VISIBLE);
            }
        } else {
            Glide.with(context).load(jsonObject.optString("linkData")).apply(Utils.getRequestOptions()).into(holder.postImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPostImageClicked.onClicked(jsonObject);
            }
        });
    }

    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }



    @SuppressLint("SetTextI18n")
    private void setTextValue(String name, String notification, TextView captionText, String time) {

        captionText.setText(notification+" "+time, TextView.BufferType.SPANNABLE);

        int start = 0;
        Spannable s = (Spannable) captionText.getText();
        int end = start + name.length();
        s.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.text_black)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @Override
    public int getItemViewType(int i) {
        int returnValue;
        if (jsonArray.optJSONObject(i).optString("notification_type").equalsIgnoreCase("follow")) {
            returnValue=0;
        } else {
            returnValue=1;

        }

        return returnValue;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView userImage;
        TextView captionText;
        ImageView postImage;
        ImageView followingButton,followButton;

        public ViewHolder(View view) {
            super(view);
            userImage = view.findViewById(R.id.userImage);
            captionText = view.findViewById(R.id.captionText);
            postImage = view.findViewById(R.id.postImage);
            followButton = view.findViewById(R.id.followButton);
            followingButton = view.findViewById(R.id.followingButton);


        }
    }

}
