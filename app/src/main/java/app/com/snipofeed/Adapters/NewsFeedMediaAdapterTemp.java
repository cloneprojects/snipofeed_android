package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.R;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class NewsFeedMediaAdapterTemp extends RecyclerView.Adapter<NewsFeedMediaAdapterTemp.ViewHolder> {

    JSONArray jsonArray;
    onRecyclerViewItemClicked onRecyclerViewItemClicked;
    private Context context;

    public NewsFeedMediaAdapterTemp(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    @NonNull
    @Override
    public NewsFeedMediaAdapterTemp.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;

        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_feed_media_item_temp, viewGroup, false);

        return new ViewHolder(view);
    }


    public void setClickListener(onRecyclerViewItemClicked onRecyclerViewItemClicked) {
        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;

    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    @Override
    public void onBindViewHolder(NewsFeedMediaAdapterTemp.ViewHolder holder, final int position) {

        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d("NEWSFEEDARRAY", "onBindViewHolder: "+jsonObject.optString("linkData"));

        if (jsonObject.optString("type").equalsIgnoreCase("video")) {
            holder.playLayout.setVisibility(View.VISIBLE);
            Glide.with(context).load(jsonObject.optString("thumbnail")).into(holder.postImageView);
        } else {
            Glide.with(context).load(jsonObject.optString("linkData")).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    Log.d("asda", "onResourceReady: loaded");
                    return false;
                }
            }).into(holder.postImageView);
            holder.playLayout.setVisibility(View.GONE);

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jsonObject.optString("type").equalsIgnoreCase("video")) {
                    onRecyclerViewItemClicked.onClicked(jsonObject.optString("linkData"));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView postImageView;
        RelativeLayout playLayout;

        public ViewHolder(View view) {
            super(view);
            postImageView = view.findViewById(R.id.postImageView);
            playLayout = view.findViewById(R.id.playLayout);


        }
    }

}
