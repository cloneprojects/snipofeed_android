package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.onPostImageClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class NotificationFollowingAdapter extends RecyclerView.Adapter<NotificationFollowingAdapter.ViewHolder> {


    JSONArray jsonArray;
    AppSettings appSettings;
    private Activity context;
    private String TAG = NotificationFollowingAdapter.class.getSimpleName();
    private onProfileImageClicked onProfileImageClicked;
    private onPostImageClicked onPostImageClicked;


    public NotificationFollowingAdapter(Activity context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        appSettings = new AppSettings(context);
    }


    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }

    public void setOnProfileImageClicked(onProfileImageClicked onProfileImageClicked) {

        this.onProfileImageClicked = onProfileImageClicked;
    }


    public void setOnPostImageClicked(onPostImageClicked onPostImageClicked) {

        this.onPostImageClicked = onPostImageClicked;
    }



    @NonNull
    @Override
    public NotificationFollowingAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.following_notification_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final NotificationFollowingAdapter.ViewHolder holder, final int position) {


        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d(TAG, "onBindViewHolder: " + jsonObject);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);


        String user_name_1 = jsonObject.optString("notification_1");
        String caption_content = jsonObject.optString("notification_2");
        String user_name_2 = jsonObject.optString("notification_3");


        SpannableString ss_caption = new SpannableString(user_name_1);

        ss_caption.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.text_black)), 0, user_name_1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString ss_captionnew = new SpannableString(user_name_2);

        ss_captionnew.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.text_black)), 0, user_name_2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        if (jsonObject.optString("notification_type").equalsIgnoreCase("followers_followers")) {
            holder.captionText.setText("");
            holder.captionText.append(ss_caption);
            holder.captionText.append(caption_content);
            holder.captionText.append(ss_captionnew);
        } else {

            holder.captionText.setText("");
            holder.captionText.append(ss_caption);
            holder.captionText.append(caption_content);
            holder.captionText.append(ss_captionnew);
            holder.captionText.append(" posts");


        }

        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileImageClicked.onClicked(jsonObject.optString("name"));
            }
        });





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPostImageClicked.onClicked(jsonObject);
            }
        });

        JSONArray jsonArray = jsonObject.optJSONArray("data");
        Log.d(TAG, "NotificationFollowi: " + jsonArray.length());
        if (jsonArray.length() > 1) {
            holder.postImages.setVisibility(View.VISIBLE);
            holder.postImage.setVisibility(View.GONE);
            holder.shadowImage.setVisibility(View.GONE);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 5, LinearLayoutManager.VERTICAL, false);
            GridViewNotificationAdapter gridViewNotificationAdapter = new GridViewNotificationAdapter(context, jsonArray);
            holder.postImages.setLayoutManager(gridLayoutManager);
            holder.postImages.setAdapter(gridViewNotificationAdapter);
        } else {

            if (!jsonObject.optString("notification_type").equalsIgnoreCase("followers_followers")) {
                holder.postImages.setVisibility(View.GONE);
                holder.postImage.setVisibility(View.VISIBLE);
                holder.shadowImage.setVisibility(View.VISIBLE);
            } else {
                holder.postImages.setVisibility(View.GONE);
                holder.postImage.setVisibility(View.GONE);
                holder.shadowImage.setVisibility(View.GONE);
            }
            Glide.with(context).load(jsonArray.optJSONObject(0).optString("linkData")).apply(requestOptions).into(holder.postImage);
        }
        Glide.with(context).load(jsonArray.optJSONObject(0).optString("userImage")).apply(requestOptions).into(holder.userImage);


    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView userImage;
        TextView captionText;
        ImageView postImage, shadowImage;
        RecyclerView postImages;


        public ViewHolder(View view) {
            super(view);
            userImage = view.findViewById(R.id.userImage);
            captionText = view.findViewById(R.id.captionText);
            postImage = view.findViewById(R.id.postImage);
            postImages = view.findViewById(R.id.postImages);
            shadowImage = view.findViewById(R.id.shadowImage);


        }
    }

}
