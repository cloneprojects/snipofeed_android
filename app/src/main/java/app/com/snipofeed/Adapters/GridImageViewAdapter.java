package app.com.snipofeed.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import app.com.snipofeed.Events.ImageSelected;
import app.com.snipofeed.Fragment.GalleryFragment;
import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.R;
import app.com.snipofeed.model.Image;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class GridImageViewAdapter extends RecyclerView.Adapter<GridImageViewAdapter.ViewHolder> {

    ArrayList<Image> images = new ArrayList<>();
    private Context context;
    public static int count=0;


    public GridImageViewAdapter(Context context, ArrayList<Image> images) {
        this.context = context;
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int i) {

        try {
            Glide.with(context).load(images.get(i).getPath()).into(holder.imageValue);
            if (GalleryFragment.isMultipleSelected) {
                if (images.get(i).isSelected()) {
                    holder.selectedSymbol.setVisibility(View.VISIBLE);
                } else {
                    holder.selectedSymbol.setVisibility(View.GONE);

                }
            } else {
                holder.multipleSelection.setVisibility(View.GONE);

            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (GalleryFragment.isMultipleSelected) {
                        if (images.get(i).isSelected()) {
                            int indexOf = GalleryFragment.multipleImages.indexOf(images.get(i).getPath());
                            GalleryFragment.multipleImages.remove(indexOf);
                            GalleryFragment.multipleImagesContentType.remove(indexOf);
                            images.get(i).setSelected(false);
                            notifyDataSetChanged();

                        } else {
                            GalleryFragment.multipleImages.add(images.get(i).getPath());
                            GalleryFragment.multipleImagesContentType.add(images.get(i).getContentType());
                            images.get(i).setSelected(true);
                            notifyDataSetChanged();
                        }
                        EventBus.getDefault().post(new ImageSelected(images.get(i).getPath(), images.get(i).getContentType()));

                    } else {
                        GalleryFragment.multipleImages=new ArrayList<>();
                        GalleryFragment.multipleImages.add(images.get(i).getPath());
                        GalleryFragment.multipleImagesContentType.add(images.get(i).getContentType());
                        EventBus.getDefault().post(new ImageSelected(images.get(i).getPath(), images.get(i).getContentType()));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
//        return images.size();
        return count;
//        return 300;
    }

    public void changeValues(ArrayList<Image> imagess) {
        images.clear();
        images.addAll(imagess);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout multipleSelection;
        ImageView selectedSymbol;
        private ImageView  imageValue;

        public ViewHolder(View view) {
            super(view);
            imageValue = view.findViewById(R.id.imageValue);
            selectedSymbol = (ImageView) view.findViewById(R.id.selectedSymbol);
            multipleSelection = view.findViewById(R.id.multipleSelection);
        }
    }

}
