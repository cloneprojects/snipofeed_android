package app.com.snipofeed.Adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.R;
import app.com.snipofeed.Videhandler.UniversalMediaController;
import app.com.snipofeed.Videhandler.UniversalVideoView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by yuvaraj on 30/04/18.
 */

public class GridViewNotificationAdapter extends RecyclerView.Adapter<GridViewNotificationAdapter.ViewHolder> {


    JSONArray jsonArray;
    private Context context;

    private onRecyclerViewItemClicked onRecyclerViewItemClicked;
    private String TAG = GridViewNotificationAdapter.class.getSimpleName();
    private Dialog dialog;

    public GridViewNotificationAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    public void setVisibility(boolean isVisible, View itemView) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        if (isVisible) {
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        } else {
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }



    public void setOnRecyclerViewItemClicked(onRecyclerViewItemClicked onRecyclerViewItemClicked) {

        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;
    }


    public void addValues(JSONArray jsonArrays) {
        for (int i = 0; i < jsonArrays.length(); i++) {
            JSONObject jsonObject1 = jsonArrays.optJSONObject(i);
            jsonArray.put(jsonObject1);
        }
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public GridViewNotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_image_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final GridViewNotificationAdapter.ViewHolder holder, final int position) {



        final JSONObject jsonObject = jsonArray.optJSONObject(position);
        Log.d(TAG, "onBindViewHolder: " + jsonObject);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);
        Glide.with(context).load(jsonObject.optString("linkData")).apply(requestOptions).into(holder.postImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewItemClicked.onClicked(jsonObject.optString("username"), jsonObject);

            }
        });



    }





    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView postImage;



        public ViewHolder(View view) {
            super(view);
            postImage = view.findViewById(R.id.postImage);


        }
    }

}
