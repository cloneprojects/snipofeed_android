package app.com.snipofeed.model;

import java.util.ArrayList;

/**
 * Created by boss1088 on 8/22/16.
 */
public class Folder {

    private String folderName;
    private ArrayList<Image> images;
    private String folderType;

    public Folder(String bucket,String folderType) {
        folderName = bucket;
        images = new ArrayList<>();
        this.folderType=folderType;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }
}
