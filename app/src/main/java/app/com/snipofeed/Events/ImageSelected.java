package app.com.snipofeed.Events;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.model.Folder;
import app.com.snipofeed.model.Image;

/**
 * Created by yuvaraj on 27/04/18.
 */

public class ImageSelected {
String imagePath;
String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ImageSelected(String imagePath, String type) {

        this.imagePath = imagePath;
        this.type = type;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public ImageSelected(String imagePath) {

        this.imagePath = imagePath;

    }
}
