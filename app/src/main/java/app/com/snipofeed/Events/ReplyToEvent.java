package app.com.snipofeed.Events;

/**
 * Created by yuvaraj on 07/05/18.
 */

public class ReplyToEvent {

    String commentId;
    String userName;
    int position;


    public ReplyToEvent(String commentId, String userName, int position) {
        this.commentId = commentId;
        this.userName = userName;
        this.position = position;
    }

    public int getPosition() {

        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }




}
