package app.com.snipofeed.Events;

/**
 * Created by yuvaraj on 08/05/18.
 */

public class HashTagSelected {
    int start;
    int count;
    String id;
    String tagName;

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public HashTagSelected(int start, int count, String id, String tagName) {

        this.start = start;
        this.count = count;
        this.id = id;
        this.tagName = tagName;
    }
}
