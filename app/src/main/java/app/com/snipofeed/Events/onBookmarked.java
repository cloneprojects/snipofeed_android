package app.com.snipofeed.Events;

/**
 * Created by yuvaraj on 04/05/18.
 */

public class onBookmarked {
    int position;
    String bookMarkStatus;


    public String getBookMarkStatus() {
        return bookMarkStatus;
    }

    public void setBookMarkStatus(String bookMarkStatus) {
        this.bookMarkStatus = bookMarkStatus;
    }




    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }



    public onBookmarked(int position, String bookMarkStatus) {

        this.position = position;
        this.bookMarkStatus = bookMarkStatus;

    }
}
