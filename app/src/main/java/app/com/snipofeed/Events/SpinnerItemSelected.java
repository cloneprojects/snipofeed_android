package app.com.snipofeed.Events;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.model.Folder;
import app.com.snipofeed.model.Image;

/**
 * Created by yuvaraj on 27/04/18.
 */

public class SpinnerItemSelected {
    int position;
    Folder folders;
    String folderType;

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public SpinnerItemSelected(int position, Folder folders) {
        this.position = position;
        this.folders = folders;
        this.folderType = folders.getFolderType();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Folder getFolders() {
        return folders;
    }

    public void setFolders(Folder folders) {
        this.folders = folders;
    }
}
