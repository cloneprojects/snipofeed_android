package app.com.snipofeed.Events;

/**
 * Created by yuvaraj on 04/05/18.
 */

public class PostLiked {
    int position;
    String likeStatus;

    public PostLiked(int position, String likeStatus) {
        this.position = position;
        this.likeStatus = likeStatus;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getLikeStatus() {
        return likeStatus;
    }

    public void setLikeStatus(String likeStatus) {
        this.likeStatus = likeStatus;
    }


}
