package app.com.snipofeed.Events;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.model.Folder;
import app.com.snipofeed.model.Image;

/**
 * Created by yuvaraj on 27/04/18.
 */

public class ImageFetched {
    private List<Folder> folders;
    private ArrayList<Image> images;

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public ImageFetched(List<Folder> folders, ArrayList<Image> images) {

        this.folders = folders;
        this.images = images;
    }
}
