package app.com.snipofeed.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import app.com.snipofeed.BuildConfig;
import app.com.snipofeed.Fragment.HomeFragment;
import app.com.snipofeed.Fragment.NewPostFragment;
import app.com.snipofeed.Fragment.NotificationFragment;
import app.com.snipofeed.Fragment.ProfileFragment;
import app.com.snipofeed.Fragment.SearchFragment;
import app.com.snipofeed.Helper.BottomNavigationViewHelper;
import app.com.snipofeed.Helper.CustomViewPager;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    public static BottomNavigationView bottomNavigationView;
    public static CustomViewPager viewPager;
    ImageView cameraIcon;
    private Uri uri;
    private String filePath;
    private CircleImageView profileImage;
    private AppSettings appSettings;
    private File displayPicture;
    private String path;
    private Button submitPhoto;
    private Dialog dialog;
    private String TAG=MainActivity.class.getSimpleName();

    public static void moveToOurProfile() {
//        bottomNavigationView.setSelectedItemId(R.id.profile_item);
        viewPager.setCurrentItem(4);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);

        setContentView(R.layout.activity_main);
        appSettings = new AppSettings(MainActivity.this);
        handleIntents();
        initTab();
        try {
            updateDeviceToken();

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    private void updateDeviceToken() throws JSONException {
        int versionCode = BuildConfig.VERSION_CODE;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("deviceToken", appSettings.getFireBaseToken());
        jsonObject.put("appVersion", "" + versionCode);
        jsonObject.put("os", "android");
        ApiCall.PostMethodNoProgress(MainActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initTab() {
        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);


        viewPager = (CustomViewPager) findViewById(R.id.customViewPager);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(4);
        ViewPagerAdapter adapter = new ViewPagerAdapter(MainActivity.this.getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "title");
        adapter.addFragment(new SearchFragment(), "title");
        adapter.addFragment(new NewPostFragment(), "title");
        adapter.addFragment(new NotificationFragment(), "title");
        adapter.addFragment(new ProfileFragment(), "title");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    bottomNavigationView.setSelectedItemId(R.id.profile_item);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.home_item:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.search_item:
                                viewPager.setCurrentItem(1);

                                break;
                            case R.id.new_post_item:
                                viewPager.setCurrentItem(2);

                                break;
                            case R.id.notification_item:
                                viewPager.setCurrentItem(3);

                                break;

                            case R.id.profile_item:
//                                selectedFragment = ProfileFragment.newInstance();
                                viewPager.setCurrentItem(4);

                                break;


                        }
//                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.frame_layout, selectedFragment);
//                        transaction.commit();
                        return true;
                    }
                });

        // Manually displaying the first fragment - one time only
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
//        transaction.commit();

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    private void handleIntents() {
        Intent intent = getIntent();
        if (intent.getStringExtra("showImageUpload").equalsIgnoreCase("true")) {
            showProfilePicUpload();
        }

        try {
            if (intent.getStringExtra("moveProfile").equalsIgnoreCase("true")) {
                moveToOurProfile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProfilePicUpload() {

        dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.profile_picture_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        submitPhoto = dialog.findViewById(R.id.submitPhoto);
        TextView skipTextView = dialog.findViewById(R.id.skipTextView);
        ImageView cameraIcon = dialog.findViewById(R.id.cameraIcon);

        submitPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
        profileImage = dialog.findViewById(R.id.profileImage);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });
        cameraIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cameraIcon.setVisibility(View.VISIBLE);
        profileImage.setVisibility(View.GONE);
    }

    private void uploadImage() {
        Utils.uploadFile(MainActivity.this, displayPicture, new Utils.UploadCallBack() {
            @Override
            public void result(boolean status, String message) {

                try {
                    updateApi(message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void updateApi(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("profilePic", message);
        ApiCall.PostMethod(MainActivity.this, UrlHelper.PROFILE_PIC_UPLOAD, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();

            }
        });
    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(MainActivity.this, getPackageName() + ".provider", file);
            appSettings.setImagePath(uri.getPath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        } else {
            uri = Uri.fromFile(file);
            path = file.getPath();
            appSettings.setImagePath(path);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity_Res", "" + requestCode);
        switch (requestCode) {
            case 1:
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        beginCrop(uri);
                    } else {
                        Utils.toast(MainActivity.this, getResources().getString(R.string.unable_to_select_image));
                    }
                }
                break;
            case 104:
                if (uri != null) {
                    beginCrop(uri);
                } else {
                    Utils.toast(MainActivity.this, getResources().getString(R.string.unable_to_select_image));

                }
                break;
            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(MainActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(MainActivity.this, getResources().getString(R.string.storage_permission_error));

                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void beginCrop(Uri source) {
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg"));
        Crop.of(source, outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == RESULT_OK) {
            cameraIcon.setVisibility(View.GONE);
            profileImage.setVisibility(View.VISIBLE);
            filePath = Utils.getRealPathFromUriNew(this, Crop.getOutput(result));
            displayPicture = new File(filePath);
            Glide.with(this).load(Crop.getOutput(result)).into(profileImage);
            submitPhoto.setVisibility(View.VISIBLE);


        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.toast(MainActivity.this, getResources().getString(R.string.unable_to_select_image));
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final SparseArray<WeakReference<Fragment>> instantiatedFragments = new SparseArray<>();
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final Fragment fragment = (Fragment) super.instantiateItem(container, position);
            instantiatedFragments.put(position, new WeakReference<>(fragment));
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            instantiatedFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Nullable
        Fragment getFragment(final int position) {
            final WeakReference<Fragment> wr = instantiatedFragments.get(position);
            if (wr != null) {
                return wr.get();
            } else {
                return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
