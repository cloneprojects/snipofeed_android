package app.com.snipofeed.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {
    TextView moveSignInText, moveSignUpText, changeUserNameText, moveSignUp;
    RelativeLayout emailSignIn, emailSignUp, successSignUp, changeUserName, facebookLogin;
    LinearLayout moveFacebookLogin, signInTextLayout, signUpTextLayout, facebookLoginButton;
    String lastPlacedLayout = "emailSignIn";
    Button loginButton, signUpButton, changeUserNameButton, changePasswordButton;
    LoginButton loginFbButton;
    TextView moveForgotPassword;
    LoginManager loginManager;
    CallbackManager callbackManager;
    EditText signInUserName, signInPassword, signUpEmail, signUpFullName, signUpPassword, changeUserNameEditText, forgotPasswordEmail, newPassword, confirmPassword, signUpUserName;
    RelativeLayout changePasswordLayout, forgotPasswordLayout;
    Button forgotPasswordButton;
    boolean isOtpScreen = false;
    TextView userName;
    AppSettings appSettings;
    String OTP;
    Button moveMainActivity;
    private int RC_SIGN_IN = 0;
    private String TAG = SignInActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
        FacebookSdk.sdkInitialize(getApplicationContext());
        appSettings = new AppSettings(SignInActivity.this);
        initViews();
        initFacebookLogin();
        initListeners();
        generateKeyHash();


    }

    private void generateKeyHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    private void initFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginFbButton = (LoginButton) findViewById(R.id.loginFbButton);
        loginManager = LoginManager.getInstance();
        loginFbButton.setReadPermissions(Arrays.asList("email"));
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e(TAG, "Access token: " + accessToken);
                get_data_for_facebook(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
                Log.e(TAG, "onCancel: " + "canceling the facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e(TAG, "onError: " + exception);
            }
        });

    }

    private void get_data_for_facebook(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.e(TAG, "Facebook_Response :" + object);
                JSONObject fbJsonObject = new JSONObject();
                try {
                    final String email_json;
                    final String first_name = object.optString("first_name");
                    final String last_name = object.optString("last_name");
                    final String id = object.optString("id");
                    String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";
                    String sbirthday = "";


                    if (object.has("email")) {
                        email_json = object.optString("email");
                    } else {
                        showAlerDialog();
                        return;
                    }


                    fbJsonObject.put("email", email_json);
                    fbJsonObject.put("profilePic", imageUrl);
                    fbJsonObject.put("name", first_name + " " + last_name);
                    fbJsonObject.put("socialToken", id);
                    proceedSocialLogin(fbJsonObject);


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email,gender,age_range, locale");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void proceedSocialLogin(JSONObject fbJsonObject) {
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.SOCIAL_LOGIN, fbJsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                appSettings.setIsLoggedIn("true");
                appSettings.setFullName(response.optString("name"));
                appSettings.setUserName(response.optString("username"));
                appSettings.setEmail(response.optString("email"));
                appSettings.setAccessToken(response.optString("accessToken"));
//                userName.setText(response.optString("username"));

                if (response.optString("isNewUser").equalsIgnoreCase("true")) {
                    placeSocialSuccessLayout();
                } else {

                    moveMainActivity(0);
                }

            }
        });


    }

    private void showAlerDialog() {
        Utils.toast(SignInActivity.this, getResources().getString(R.string.email_is_not_attached));
    }


    private void initListeners() {
        moveSignInText.setOnClickListener(this);
        moveSignUpText.setOnClickListener(this);
        changeUserNameText.setOnClickListener(this);
        moveFacebookLogin.setOnClickListener(this);
        moveSignUp.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        changeUserNameButton.setOnClickListener(this);
        facebookLoginButton.setOnClickListener(this);
        moveForgotPassword.setOnClickListener(this);
        forgotPasswordButton.setOnClickListener(this);
        changePasswordButton.setOnClickListener(this);
        newPassword.setOnClickListener(this);
        confirmPassword.setOnClickListener(this);
        moveMainActivity.setOnClickListener(this);
    }

    private void initViews() {
        moveSignInText = findViewById(R.id.moveSignInText);
        moveForgotPassword = findViewById(R.id.moveForgotPassword);
        moveSignUpText = findViewById(R.id.moveSignUpText);
        moveSignUp = findViewById(R.id.moveSignUp);
        changeUserNameText = findViewById(R.id.changeUserNameText);
        emailSignIn = findViewById(R.id.emailSignIn);
        emailSignUp = findViewById(R.id.emailSignUp);
        successSignUp = findViewById(R.id.successSignUp);
        changeUserName = findViewById(R.id.changeUserName);
        facebookLogin = findViewById(R.id.facebookLogin);
        moveFacebookLogin = findViewById(R.id.moveFacebookLogin);
        signInTextLayout = findViewById(R.id.signInTextLayout);
        signUpTextLayout = findViewById(R.id.signUpTextLayout);
        loginButton = findViewById(R.id.loginButton);
        signInUserName = findViewById(R.id.signInUserName);
        signInPassword = findViewById(R.id.signInPassword);
        signUpEmail = findViewById(R.id.signUpEmail);
        signUpFullName = findViewById(R.id.signUpFullName);
        signUpPassword = findViewById(R.id.signUpPassword);
        signUpButton = findViewById(R.id.signUpButton);
        changeUserNameButton = findViewById(R.id.changeUserNameButton);
        changeUserNameEditText = findViewById(R.id.changeUserNameEditText);
        facebookLoginButton = findViewById(R.id.facebookLoginButton);
        changePasswordLayout = findViewById(R.id.changePasswordLayout);
        forgotPasswordLayout = findViewById(R.id.forgotPasswordLayout);
        forgotPasswordButton = findViewById(R.id.forgotPasswordButton);
        forgotPasswordEmail = findViewById(R.id.forgotPasswordEmail);
        changePasswordButton = findViewById(R.id.changePasswordButton);
        confirmPassword = findViewById(R.id.confirmPassword);
        newPassword = findViewById(R.id.newPassword);
//        userName = findViewById(R.id.userName);
        moveMainActivity = findViewById(R.id.moveMainActivity);
        signUpUserName = findViewById(R.id.signUpUserName);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void slideLeft(final View in, final View out) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_left);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void slideRight(final View in, final View out) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_left);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_right);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        if (lastPlacedLayout.equalsIgnoreCase("Facebook")) {
            lastPlacedLayout = "emailSignIn";
            setSignUpBottom();
            slideRight(emailSignIn, facebookLogin);
        } else if (lastPlacedLayout.equalsIgnoreCase("emailSignUp")) {
            lastPlacedLayout = "emailSignIn";
            slideRight(emailSignIn, emailSignUp);
            setSignUpBottom();

        } else if (lastPlacedLayout.equalsIgnoreCase("emailSignUpFromFB")) {
            lastPlacedLayout = "Facebook";
            slideRight(facebookLogin, emailSignUp);
            setSignUpBottom();

        } else if (lastPlacedLayout.equalsIgnoreCase("emailSignUpFromLogin")) {
            lastPlacedLayout = "emailSignIn";
            slideRight(emailSignIn, emailSignUp);
            setSignUpBottom();

        } else if (lastPlacedLayout.equalsIgnoreCase("SuccessSignUp")) {
            lastPlacedLayout = "emailSignUp";
            slideRight(emailSignUp, successSignUp);
            setSignInBottom();
        } else if (lastPlacedLayout.equalsIgnoreCase("ChangeUserName")) {
            lastPlacedLayout = "SuccessSignUp";
            slideRight(successSignUp, changeUserName);
            hideBottomLayout();

        } else if (lastPlacedLayout.equalsIgnoreCase("forgotPassword")) {
            lastPlacedLayout = "emailSignIn";
            slideRight(emailSignIn, forgotPasswordLayout);
            setSignUpBottom();
        } else if (lastPlacedLayout.equalsIgnoreCase("changePassword")) {
            lastPlacedLayout = "emailSignIn";
            isOtpScreen = false;
            slideRight(emailSignIn, changePasswordLayout);
            setSignUpBottom();
        } else if (lastPlacedLayout.equalsIgnoreCase("emailSignIn") || lastPlacedLayout.equalsIgnoreCase("SocialSuccessSignUp")) {
            super.onBackPressed();
        }

    }

    private void hideBottomLayout() {
        signUpTextLayout.setVisibility(View.GONE);
        signInTextLayout.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        if (v == moveFacebookLogin) {
            setSignInBottom();
            placeFacebookLogin();
        } else if (v == moveSignUpText) {
            if (lastPlacedLayout.equalsIgnoreCase("emailSignIn")) {
                lastPlacedLayout = "emailSignUp";
                setSignInBottom();
                placeSignUpEmail(emailSignIn);
            }
        } else if (v == moveSignUp) {
            lastPlacedLayout = "emailSignUpFromFB";
            setSignInBottom();
            placeSignUpEmail(facebookLogin);
        } else if (v == moveSignInText) {
            if (lastPlacedLayout.equalsIgnoreCase("emailSignUp") || lastPlacedLayout.equalsIgnoreCase("emailSignUpFromFB")) {
                lastPlacedLayout = "emailSignIn";
                setSignUpBottom();
                placeSignInEmail(emailSignUp);

            } else if (lastPlacedLayout.equalsIgnoreCase("Facebook")) {
                lastPlacedLayout = "emailSignIn";
                setSignUpBottom();
                placeSignInEmail(facebookLogin);
            } else if (lastPlacedLayout.equalsIgnoreCase("forgotPassword")) {
                lastPlacedLayout = "emailSignIn";
                setSignUpBottom();
                placeSignInEmail(forgotPasswordLayout);
            }
        } else if (v == loginButton) {
            processInputs();
        } else if (v == signUpButton) {
            processSignUpInputs();

        } else if (v == changeUserNameText) {
            placeChangeUserName();
        } else if (v == changeUserNameButton) {
            try {
                processChangeUserNameInputs();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (v == facebookLoginButton) {
            loginFbButton.performClick();
        } else if (v == moveForgotPassword) {
            placeForgotPassword();
        } else if (v == forgotPasswordButton) {
            if (!isOtpScreen) {
                if (Utils.isValidEmail(forgotPasswordEmail.getText().toString())) {
                    try {
                        hitForgotPasswordApi();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_email));
                }
            } else {
                if (forgotPasswordEmail.getText().toString().equalsIgnoreCase(OTP)) {
                    placeChangePasswordLayout();
                } else {
                    Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_otp));
                }
            }
        } else if (v == changePasswordButton) {
            if (confirmPassword.getText().toString().trim().length() > 6) {
                if (confirmPassword.getText().toString().trim().equalsIgnoreCase(newPassword.getText().toString().trim())) {
                    try {
                        hitChangePassword();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.toast(SignInActivity.this, getResources().getString(R.string.password_not_match));

                }
            } else {
                Utils.toast(SignInActivity.this, getResources().getString(R.string.password_should_be_minimum));
            }
        } else if (v == moveMainActivity) {


            try {
                processChangeUserNameInputs();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void hitChangePassword() throws JSONException {

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", appSettings.getEmail());
        jsonObject.put("password", confirmPassword.getText().toString());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.FORGOT_PASSWORD, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(SignInActivity.this, getResources().getString(R.string.password_changed_successfully));
                placeSignInEmail(changePasswordLayout);


            }
        });

    }

    private void hitForgotPasswordApi() throws JSONException {

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", forgotPasswordEmail.getText().toString());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.FORGOT_PASSWORD, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                appSettings.setEmail(jsonObject.optString("email"));
                OTP = response.optString("otp");
                changeOtpLayout();


            }
        });
    }

    private void placeChangePasswordLayout() {

        lastPlacedLayout = "changePassword";
        hideBottomLayout();
        slideLeft(changePasswordLayout, forgotPasswordLayout);
    }

//    placeSignInEmail(changePasswordLayout);


    private void changeOtpLayout() {
        isOtpScreen = true;
        forgotPasswordEmail.setText("");
        forgotPasswordEmail.setHint(getResources().getString(R.string.enter_otp));
    }

    public void changeEmailLayout() {
        isOtpScreen = false;
        forgotPasswordEmail.setText("");
        forgotPasswordEmail.setHint(getResources().getString(R.string.email));
    }


    private void placeForgotPassword() {
        lastPlacedLayout = "forgotPassword";
        setSignInBottom();
        slideLeft(forgotPasswordLayout, emailSignIn);
        changeEmailLayout();
    }

    private void processChangeUserNameInputs() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", changeUserNameEditText.getText().toString());
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.CHANGE_USER_NAME, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                appSettings.setUserName(response.optString("username"));
                Utils.toast(SignInActivity.this, getResources().getString(R.string.username_changed_successfully));
                moveMainActivity(1);


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }


    }


    private void placeChangeUserName() {
        lastPlacedLayout = "ChangeUserName";
        hideBottomLayout();
        slideLeft(changeUserName, successSignUp);
    }

    private void placeSignUpSuccessLayout() {

        changeUserNameEditText.setText(appSettings.getUserName());
        changeUserNameEditText.setVisibility(View.GONE);
        lastPlacedLayout = "SuccessSignUp";
        hideBottomLayout();
        slideLeft(successSignUp, emailSignUp);
    }

    private void placeSocialSuccessLayout() {
        lastPlacedLayout = "SocialSuccessSignUp";
        hideBottomLayout();
        slideLeft(successSignUp, facebookLogin);
    }

    private void processSignUpInputs() {

        if (signUpEmail.getText().toString().trim().trim().length() == 0) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_email));
        } else if (!Utils.isValidEmail(signUpEmail.getText().toString().trim())) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_email));
        } else if (signUpPassword.getText().toString().trim().length() == 0) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_password));
        } else if (signUpFullName.getText().toString().trim().length() == 0) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_name));
        } else {
            try {
                checkForUserName();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void checkForUserName() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", signUpUserName.getText().toString());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.CHECK_USERNAME, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    signUp();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void signUp() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", signUpFullName.getText().toString());
        jsonObject.put("email", signUpEmail.getText().toString());
        jsonObject.put("password", signUpPassword.getText().toString());
        jsonObject.put("username", signUpUserName.getText().toString());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.CREATE_ACCOUNT, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                appSettings.setFullName(response.optString("name"));
                appSettings.setUserName(response.optString("username"));
                appSettings.setEmail(response.optString("email"));
                appSettings.setAccessToken(response.optString("accessToken"));
//                userName.setText(response.optString("username"));
                appSettings.setIsLoggedIn("true");

                placeSignUpSuccessLayout();


            }
        });
    }

    private void processInputs() {
        if (signInUserName.getText().toString().trim().trim().length() == 0) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_email));
        } else if (!Utils.isValidEmail(signInUserName.getText().toString().trim())) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_email));
        } else if (signInPassword.getText().toString().trim().length() == 0) {
            Utils.toast(SignInActivity.this, getResources().getString(R.string.please_enter_valid_password));
        } else {
            try {
                signIn();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void signIn() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("email", signInUserName.getText().toString());
        jsonObject.put("password", signInPassword.getText().toString());
        ApiCall.PostMethod(SignInActivity.this, UrlHelper.SIGN_IN, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                appSettings.setFullName(response.optString("name"));
                appSettings.setUserName(response.optString("username"));
                appSettings.setEmail(response.optString("email"));
                appSettings.setAccessToken(response.optString("accessToken"));
//                userName.setText(response.optString("username"));
                appSettings.setIsLoggedIn("true");


                moveMainActivity(0);

            }
        });
    }

    private void moveMainActivity(int value) {
        Intent intent = new Intent(SignInActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (value == 0) {
            intent.putExtra("showImageUpload", "false");
        } else {
            intent.putExtra("showImageUpload", "true");

        }
        startActivity(intent);
    }

    private void placeSignInEmail(View view) {
        lastPlacedLayout = "emailSignIn";
        slideLeft(emailSignIn, view);
        setSignUpBottom();
    }

    private void setSignInBottom() {
        signUpTextLayout.setVisibility(View.GONE);
        signInTextLayout.setVisibility(View.VISIBLE);
    }

    private void setSignUpBottom() {
        signUpTextLayout.setVisibility(View.VISIBLE);
        signInTextLayout.setVisibility(View.GONE);
    }

    private void placeSignUpEmail(View nextLayout) {
        slideLeft(emailSignUp, nextLayout);

    }

    private void placeFacebookLogin() {
        lastPlacedLayout = "Facebook";
        slideLeft(facebookLogin, emailSignIn);
    }
}
