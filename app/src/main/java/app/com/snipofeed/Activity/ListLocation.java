package app.com.snipofeed.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;

import java.util.StringTokenizer;

import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ListLocation extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    String selectedLat, selectedLong;
    TextView doneButton;
    boolean isCurrentLcoationSelected = true;
    ImageView closeButton;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private String TAG = ListLocation.class.getSimpleName();
    private RelativeLayout topLayout;
    private CardView topCard;
    private ImageView currentLocationRound;
    private RelativeLayout currentLocationLayout;
    private TextView customLocationName;
    private ImageView customLocationRound;
    private RelativeLayout customLocation;
    private RelativeLayout customLocationLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_list_location);

        initViews();
        initlocation();
        initListeners();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }
    }

    private void initListeners() {
        doneButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);
        customLocationLayout.setOnClickListener(this);
        currentLocationLayout.setOnClickListener(this);
        customLocation.setOnClickListener(this);
    }

    private void initViews() {
        customLocationLayout = (RelativeLayout) findViewById(R.id.customLocationLayout);
        customLocation = (RelativeLayout) findViewById(R.id.customLocation);
        customLocationRound = (ImageView) findViewById(R.id.customLocationRound);
        customLocationName = (TextView) findViewById(R.id.customLocationName);
        currentLocationLayout = (RelativeLayout) findViewById(R.id.currentLocationLayout);
        currentLocationRound = (ImageView) findViewById(R.id.currentLocationRound);
        topCard = (CardView) findViewById(R.id.topCard);
        topLayout = (RelativeLayout) findViewById(R.id.topLayout);
        doneButton = (TextView) findViewById(R.id.doneButton);
        closeButton = (ImageView) findViewById(R.id.closeButton);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {

                initlocationRequest();
            }
        }

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initlocationRequest();
                settingsrequest();

            }

        }
    }


    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ListLocation.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            Utils.log(TAG, "mylat:" + currentLatitude + ",mylong:" + currentLongitude);
            if (currentLatitude != 0.0 && currentLongitude != 0.0) {

//                    setlocation();

            }

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationChanged(Location location) {

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                selectedLat = data.getStringExtra("latitude");
                selectedLong = data.getStringExtra("longitude");


                StringTokenizer st = new StringTokenizer(data.getStringExtra("place_details"), ",");
                /************  Set Model values in Holder elements ***********/

                String areaname = st.nextToken();
                customLocationName.setText(areaname);
                String desc_detail = "";
                for (int i = 1; i < st.countTokens(); i++) {
                    if (i == st.countTokens() - 1) {
                        desc_detail = desc_detail + st.nextToken();
                    } else {
                        desc_detail = desc_detail + st.nextToken() + ",";
                    }
                }
                setCustomLayoutSelected();
            }
            if (resultCode == Activity.RESULT_CANCELED) {


                //Write your code if there's no result
            }
        }
    }

    private void setCustomLayoutSelected() {
        isCurrentLcoationSelected = false;
        customLocation.setVisibility(View.VISIBLE);
        customLocationRound.setImageResource(R.drawable.circle_round_blue);
        currentLocationRound.setImageResource(R.drawable.circle_round_grey);

    }


    @Override
    public void onClick(View v) {
        if (v == closeButton) {

            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();

        }
        if (v == doneButton) {

            Intent returnIntent = new Intent();
            if (isCurrentLcoationSelected) {
                returnIntent.putExtra("latitude", "" + currentLatitude);
                returnIntent.putExtra("longitude", "" + currentLongitude);
            } else {
                returnIntent.putExtra("latitude", "" + selectedLat);
                returnIntent.putExtra("longitude", "" + selectedLong);
            }
            setResult(Activity.RESULT_OK, returnIntent);
            finish();

        }
        if (v == customLocationLayout) {
            moveAddLocation();
        }
        if (v == currentLocationLayout) {
            setCurrentLocationSelected();
        }
        if (v == customLocation) {
            setCustomLayoutSelected();
        }
    }

    private void setCurrentLocationSelected() {
        isCurrentLcoationSelected = true;
        currentLocationRound.setImageResource(R.drawable.circle_round_blue);
        customLocationRound.setImageResource(R.drawable.circle_round_grey);
    }

    private void moveAddLocation() {
        Intent intent = new Intent(ListLocation.this, AddLocation.class);
        startActivityForResult(intent, 1);
    }
}
