package app.com.snipofeed.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wonderkiln.camerakit.CameraView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

import app.com.snipofeed.Adapters.HashTagSugesstionAdapter;
import app.com.snipofeed.Events.HashTagSelected;
import app.com.snipofeed.Events.MentionSelected;
import app.com.snipofeed.Helper.SquareImageView;
import app.com.snipofeed.Helper.SwitchButton;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.PrefHandler.SharedPreference;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PostFeedActivity extends AppCompatActivity {
    public static ArrayList<String> multipleImages = new ArrayList<>();
    public static ArrayList<String> multipleImagesContentType = new ArrayList<>();
    SquareImageView postImage;
    ImageView isMultiple;
    EditText postContent;
    TextView shareText;
    AppSettings appSettings;
    JSONArray finalImagesArray = new JSONArray();
    JSONArray finalContentArray = new JSONArray();

    JSONArray hashTagArray = new JSONArray();
    JSONArray usersArray = new JSONArray();
    int count = 0;
    String postType = "";
    TextView addLocation;
    RelativeLayout afterLocationSelected;
    TextView place_name, place_detail;
    RecyclerView suggestionLists;
    ImageView closeIcon;
    SwitchButton commentsDetect;
    CameraView cameraView;
    String latitude = "", longitude = "", areaname = "", cityname = "";
    CardView hashTagLists;
    ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
    private String TAG = PostFeedActivity.class.getSimpleName();
    private Spannable mspanable;
    private int hashTagIsComing = 0;
    private int mentionIsComing = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        int totalCoun = Integer.parseInt(getIntent().getStringExtra("totalImage"));
        for (int i = 0; i < totalCoun; i++) {
            SharedPreference.removeKey(PostFeedActivity.this, "bitmap_" + i);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        int totalCoun = Integer.parseInt(getIntent().getStringExtra("totalImage"));
        for (int i = 0; i < totalCoun; i++) {
            SharedPreference.removeKey(PostFeedActivity.this, "bitmap_" + i);
        }
        EventBus.getDefault().unregister(this);
    }


    @SuppressLint("SetTextI18n")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onHashClicked(HashTagSelected hashTagSelected) {
        String currentText = postContent.getText().toString();
        Log.d(TAG, "onHashClicked: " + currentText);
        Log.d(TAG, "onHashClickedNew: " + hashTagSelected.getStart() + "/" + hashTagSelected.getCount());

        String splittting;
        String splittting2;
        if (hashTagSelected.getStart() + 1 < currentText.length()) {
            splittting = currentText.substring(0, hashTagSelected.getStart() + 1);
            splittting2 = currentText.substring(hashTagSelected.getStart() + 1, postContent.getText().toString().length());

        } else {
            splittting = currentText;
            splittting2 = "";
        }
        String[] finalvalue = splittting.split(" ");
        if (splittting2.length() == 0) {
            if (finalvalue.length > 1) {
                finalvalue[finalvalue.length - 1] = hashTagSelected.getTagName();

                for (int i = 0; i < finalvalue.length; i++) {
                    finalvalue[i] = finalvalue[i] + " ";
                }

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < finalvalue.length; i++) {
                    strBuilder.append(finalvalue[i]);
                }
                String newString = strBuilder.toString();
                postContent.setText(newString + " " + splittting2);
                postContent.setSelection(newString.length());
                hashTagLists.setVisibility(View.GONE);
            } else {
                postContent.setText(hashTagSelected.getTagName() + " ");
                postContent.setSelection(hashTagSelected.getTagName().length() + 1);
                hashTagLists.setVisibility(View.GONE);
            }
        } else {

            finalvalue[finalvalue.length - 1] = hashTagSelected.getTagName();

            for (int i = 0; i < finalvalue.length; i++) {
                finalvalue[i] = finalvalue[i] + " ";
            }

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < finalvalue.length; i++) {
                strBuilder.append(finalvalue[i]);
            }
            String newString = strBuilder.toString();
            postContent.setText(newString + " " + splittting2);
            postContent.setSelection(newString.length());
            hashTagLists.setVisibility(View.GONE);

        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", hashTagSelected.getId());
            jsonObject.put("type", "hash");
            jsonObject.put("name", hashTagSelected.getTagName());
            hashTagArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMentionClicked(MentionSelected hashTagSelected) {
        String currentText = postContent.getText().toString();
        Log.d(TAG, "onHashClicked: " + currentText);
        Log.d(TAG, "onHashClickedNew: " + hashTagSelected.getStart() + "/" + hashTagSelected.getCount());

        String splittting;
        String splittting2;
        if (hashTagSelected.getStart() + 1 < currentText.length()) {
            splittting = currentText.substring(0, hashTagSelected.getStart() + 1);
            splittting2 = currentText.substring(hashTagSelected.getStart() + 1, postContent.getText().toString().length());

        } else {
            splittting = currentText;
            splittting2 = "";
        }
        String[] finalvalue = splittting.split(" ");
        if (splittting2.length() == 0) {
            if (finalvalue.length > 1) {
                finalvalue[finalvalue.length - 1] = hashTagSelected.getTagName();

                for (int i = 0; i < finalvalue.length; i++) {
                    finalvalue[i] = finalvalue[i] + " ";
                }

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 0; i < finalvalue.length; i++) {
                    strBuilder.append(finalvalue[i]);
                }
                String newString = strBuilder.toString();
                postContent.setText(newString + " " + splittting2);
                postContent.setSelection(newString.length());
                hashTagLists.setVisibility(View.GONE);
            } else {
                postContent.setText(hashTagSelected.getTagName() + " ");
                postContent.setSelection(hashTagSelected.getTagName().length() + 1);
                hashTagLists.setVisibility(View.GONE);
            }
        } else {

            finalvalue[finalvalue.length - 1] = hashTagSelected.getTagName();

            for (int i = 0; i < finalvalue.length; i++) {
                finalvalue[i] = finalvalue[i] + " ";
            }

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < finalvalue.length; i++) {
                strBuilder.append(finalvalue[i]);
            }
            String newString = strBuilder.toString();
            postContent.setText(newString + " " + splittting2);
            postContent.setSelection(newString.length());
            hashTagLists.setVisibility(View.GONE);

        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", hashTagSelected.getId());
            jsonObject.put("type", "hash");
            jsonObject.put("name", hashTagSelected.getTagName());
            hashTagArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_post_feed);
        appSettings = new AppSettings(PostFeedActivity.this);
        initViews();
        getIntentValues();
        initListners();
    }

    private void initListners() {
        shareText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                completeBitmap();
            }
        });

        addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveAddLocationActivity();
            }
        });

        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afterLocationSelected.setVisibility(View.GONE);
                addLocation.setVisibility(View.VISIBLE);
            }
        });

        mspanable = postContent.getText();

        postContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                Log.d(TAG, "beforeTextChanged: " + s + "/" + start + "/" + count + "/" + after);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d(TAG, "onTextChanged: " + postContent.getSelectionStart() + "/" + postContent.getSelectionEnd());

                try {

                    if (postContent.getSelectionStart() == postContent.getText().toString().length()) {
                        if (!String.valueOf(postContent.getText().toString().charAt(postContent.getText().toString().length() - 1)).equalsIgnoreCase(" ")) {

                            if (postContent.getText().toString().length() > 1)

                            {

                                if (before == 0) {
                                    CharSequence chars = s.subSequence(0, start);
                                    String[] total_value = String.valueOf(chars).split(" ");
                                    if (!String.valueOf(postContent.getText().toString().charAt(postContent.getText().toString().length() - 1)).equalsIgnoreCase(" "))
                                        if (total_value.length > 1) {
                                            String lastWord = total_value[total_value.length - 1];
                                            if (lastWord.length() != 0) {
                                                if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                                    Log.d(TAG, "afterTextChanged00: hash" + lastWord);
                                                    try {
                                                        getTagSuggestion(lastWord, start, count);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                                    Log.d(TAG, "afterTextChanged00" +
                                                            ": mentio" + lastWord);

                                                    try {
                                                        getUserSuggestion(lastWord, start, count);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }


                                                } else {
                                                    hashTagLists.setVisibility(View.GONE);
                                                }
                                            } else {
                                                hashTagLists.setVisibility(View.GONE);

                                            }
                                        } else {
                                            String lastWord = String.valueOf(s);
                                            if (lastWord.length() != 0) {
                                                if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                                    Log.d(TAG, "afterTextChanged0: hash" + lastWord.substring(0, start + 1));

                                                    try {
                                                        getTagSuggestion(lastWord.substring(0, start + 1), start, count);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                                    Log.d(TAG, "afterTextChanged0: mentio" + lastWord.substring(0, start + 1));

                                                    try {
                                                        getUserSuggestion(lastWord, start, count);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                } else {
                                                    hashTagLists.setVisibility(View.GONE);

                                                }
                                            } else {
                                                hashTagLists.setVisibility(View.GONE);

                                            }
                                        }


                                } else {

                                    String[] complete = s.toString().split(" ");
                                    String lastWord = complete[complete.length - 1];
                                    if (lastWord.length() != 0) {
                                        if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#")) {
                                            Log.d(TAG, "afterTextChanged1: hash");

                                            if (!String.valueOf(s.toString().charAt(s.length() - 1)).equalsIgnoreCase(" ")) {
                                                try {
                                                    getTagSuggestion(lastWord, start, count);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            } else {

                                            }


                                        } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@")) {
                                            Log.d(TAG, "afterTextChanged1: mentio");

                                            try {
                                                getUserSuggestion(lastWord, start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        } else {
                                            hashTagLists.setVisibility(View.GONE);
                                        }
                                    } else {
                                        hashTagLists.setVisibility(View.GONE);

                                    }
                                }
                            } else {
                                hashTagLists.setVisibility(View.GONE);

                            }
                        } else {
                            hashTagLists.setVisibility(View.GONE);

                        }
                    } else {
                        if (postContent.getText().toString().length() > 1) {
                            if (before == 0) {
                                CharSequence chars = s.subSequence(0, start);
                                String[] total_value = String.valueOf(chars).split(" ");

                                if (total_value.length > 1) {
                                    String lastWord = total_value[total_value.length - 1];
                                    if (lastWord.length() != 0) {
                                        if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                            Log.d(TAG, "afterTextChanged00: hash" + lastWord);
                                            try {
                                                getTagSuggestion(lastWord, start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@") && !String.valueOf(lastWord.charAt(lastWord.length() - 1)).equalsIgnoreCase(" ")) {
                                            Log.d(TAG, "afterTextChanged00" +
                                                    ": mentio" + lastWord);

                                            try {
                                                getUserSuggestion(lastWord, start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        } else {
                                            hashTagLists.setVisibility(View.GONE);
                                        }
                                    } else {
                                        hashTagLists.setVisibility(View.GONE);

                                    }
                                } else {
                                    String lastWord = String.valueOf(s);
                                    if (lastWord.length() != 0) {
                                        if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#")) {
                                            Log.d(TAG, "afterTextChanged0: hash" + lastWord.substring(0, start + 1));

                                            try {
                                                getTagSuggestion(lastWord.substring(0, start + 1), start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@")) {
                                            Log.d(TAG, "afterTextChanged0: mentio" + lastWord.substring(0, start + 1));

                                            try {
                                                getUserSuggestion(lastWord, start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        } else {
                                            hashTagLists.setVisibility(View.GONE);

                                        }
                                    } else {
                                        hashTagLists.setVisibility(View.GONE);

                                    }
                                }


                            } else {

                                String[] complete = s.toString().split(" ");
                                String lastWord = complete[complete.length - 1];
                                if (lastWord.length() != 0) {
                                    if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#")) {
                                        Log.d(TAG, "afterTextChanged1: hash");

                                        if (!String.valueOf(s.toString().charAt(s.length() - 1)).equalsIgnoreCase(" ")) {
                                            try {
                                                getTagSuggestion(lastWord, start, count);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        } else {

                                        }


                                    } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@")) {
                                        Log.d(TAG, "afterTextChanged1: mentio");

                                        try {
                                            getUserSuggestion(lastWord, start, count);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    } else {
                                        hashTagLists.setVisibility(View.GONE);

                                    }
                                } else {
                                    hashTagLists.setVisibility(View.GONE);

                                }
                            }
                        } else {
                            hashTagLists.setVisibility(View.GONE);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                validateForHashTag(s,start,before,count);
//                validateForMentions(s,start,before,count);


            }

            @Override
            public void afterTextChanged(Editable s) {
//                Log.d(TAG, "afterTextChanged: " + s);

//                Log.d(TAG, "afterTextChanged: " + s.toString());
//
//                String[] complete = s.toString().split(" ");
//
//                    String lastWord = complete[complete.length-1];
//                    if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("#")) {
//                        Log.d(TAG, "afterTextChanged: hash");
//                    } else if (String.valueOf(lastWord.charAt(0)).equalsIgnoreCase("@")) {
//                        Log.d(TAG, "afterTextChanged: mentio");
//                    }


            }
        });
    }

    private File getOutputMediaFile() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = Utils.getImageFileName(PostFeedActivity.this);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

        return file;
    }


    private void completeBitmap() {
        if (bitmapArray.size()>0) {
            for (int i = 0; i < bitmapArray.size(); i++) {

                Bitmap bitmap = bitmapArray.get(i);


                File pictureFile = getOutputMediaFile();
                if (pictureFile == null) {
                    Log.d(TAG,
                            "Error creating media file, check storage permissions: ");// e.getMessage());
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                } catch (FileNotFoundException e) {
                    Log.d(TAG, "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.d(TAG, "Error accessing file: " + e.getMessage());
                }


                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", "image");
                    jsonObject.put("data", pictureFile.getAbsolutePath());
                    jsonObject.put("thumbnail", "");
                    finalImagesArray.put(i, jsonObject);
                    if (i == (bitmapArray.size() - 1)) {
                        try {
                            uploadImages();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else {
            try {
                uploadImages();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void getUserSuggestion(String lastWord, final int start, final int count) throws JSONException {

        if (lastWord.length() > 1) {
            JSONObject jsonObject = new JSONObject();
            lastWord = lastWord.substring(1);
            jsonObject.put("userName", lastWord);

            jsonObject.put("accessToken", appSettings.getAccessToken());
            ApiCall.PostMethodNoProgress(PostFeedActivity.this, UrlHelper.MENTION_SEARCH, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    JSONArray jsonArray = response.optJSONArray("tagusers");
                    if (jsonArray.length() > 0) {
                        hashTagLists.setVisibility(View.VISIBLE);

                        HashTagSugesstionAdapter hashTagSugesstionAdapter = new HashTagSugesstionAdapter(PostFeedActivity.this, jsonArray, start, count, "mention");
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PostFeedActivity.this, LinearLayoutManager.VERTICAL, false);
                        suggestionLists.setLayoutManager(linearLayoutManager);
                        suggestionLists.setAdapter(hashTagSugesstionAdapter);
                        hashTagSugesstionAdapter.notifyDataSetChanged();

                    } else {

                        hashTagLists.setVisibility(View.GONE);
                    }
                }
            });
        } else {
            hashTagLists.setVisibility(View.GONE);
        }
    }

    private void getTagSuggestion(String lastWord, final int start, final int count) throws JSONException {
        if (lastWord.length() > 1) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tagName", lastWord);
            ApiCall.PostMethodNoProgress(PostFeedActivity.this, UrlHelper.HASH_TAG_SEARCH, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    JSONArray jsonArray = response.optJSONArray("hashTags");
                    if (jsonArray.length() > 0) {
                        hashTagLists.setVisibility(View.VISIBLE);

                        HashTagSugesstionAdapter hashTagSugesstionAdapter = new HashTagSugesstionAdapter(PostFeedActivity.this, jsonArray, start, count, "hash");
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PostFeedActivity.this, LinearLayoutManager.VERTICAL, false);
                        suggestionLists.setLayoutManager(linearLayoutManager);
                        suggestionLists.setAdapter(hashTagSugesstionAdapter);
                        hashTagSugesstionAdapter.notifyDataSetChanged();

                    } else {

                        hashTagLists.setVisibility(View.GONE);
                    }
                }
            });
        } else {
            hashTagLists.setVisibility(View.GONE);
        }
    }

    private void validateForMentions(CharSequence s, int start, int before, int count) {

        String startChar = null;

        try {
            startChar = Character.toString(s.charAt(start));
            Utils.log(getClass().getSimpleName(), "CHARACTER OF NEW WORD: " + startChar);
        } catch (Exception ex) {
            startChar = " ";
        }

        if (startChar.equals("@")) {
            tagCheck(s.toString().substring(start), start, start + count);
            mentionIsComing++;
        }

        if (startChar.equals(" ")) {
            mentionIsComing = 0;
        }

        if (mentionIsComing != 0) {
            tagCheck(s.toString().substring(start), start, start + count);
            mentionIsComing++;
        }
    }

    private void validateForHashTag(CharSequence s, int start, int before, int count) {
        String startChar = null;

        try {
            startChar = Character.toString(s.charAt(start));
            Utils.log(getClass().getSimpleName(), "CHARACTER OF NEW WORD: " + startChar);
        } catch (Exception ex) {
            startChar = " ";
        }

        if (startChar.equals("#")) {
            tagCheck(s.toString().substring(start), start, start + count);
            hashTagIsComing++;
        }

        if (startChar.equals(" ")) {
            hashTagIsComing = 0;
        }

        if (hashTagIsComing != 0) {
            tagCheck(s.toString().substring(start), start, start + count);
            hashTagIsComing++;
        }
    }

    private void tagCheck(String s, int start, int end) {
        mspanable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void moveAddLocationActivity() {

        Intent intent = new Intent(PostFeedActivity.this, AddLocation.class);
        startActivityForResult(intent, 1);

    }


    private void uploadImages() throws Exception {


        if (count < finalImagesArray.length()) {

            File uploadFile = new File(finalImagesArray.optJSONObject(count).optString("data"));


            if (finalImagesArray.optJSONObject(count).optString("thumbnail").equalsIgnoreCase("")) {
                if (finalImagesArray.optJSONObject(count).optString("type").equalsIgnoreCase("video")) {

                    Utils.uploadFile(PostFeedActivity.this, Utils.getVideoThumbnail(finalImagesArray.optJSONObject(count).optString("data"), PostFeedActivity.this), Utils.getImageFileName(PostFeedActivity.this), new Utils.UploadCallBack() {
                        @Override
                        public void result(boolean status, String message) {
                            try {
                                finalImagesArray.optJSONObject(count).remove("thumbnail");
                                finalImagesArray.optJSONObject(count).put("thumbnail", message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                uploadImages();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {

                    String filname;
                    if (finalImagesArray.optJSONObject(count).optString("type").equalsIgnoreCase("video")) {
                        filname = Utils.getVideoFileName(PostFeedActivity.this);
                    } else {
                        filname = Utils.getImageFileName(PostFeedActivity.this);
                    }

                    Utils.uploadFile(PostFeedActivity.this, uploadFile, filname, new Utils.UploadCallBack() {
                        @Override
                        public void result(boolean status, String message) {
                            try {
                                finalImagesArray.optJSONObject(count).remove("data");
                                finalImagesArray.optJSONObject(count).put("data", message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            count = count + 1;
                            try {
                                uploadImages();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


                }
            } else {
                String filname;
                if (finalImagesArray.optJSONObject(count).optString("type").equalsIgnoreCase("video")) {
                    filname = Utils.getVideoFileName(PostFeedActivity.this);
                } else {
                    filname = Utils.getImageFileName(PostFeedActivity.this);
                }

                Utils.uploadFile(PostFeedActivity.this, uploadFile, filname, new Utils.UploadCallBack() {
                    @Override
                    public void result(boolean status, String message) {
                        try {
                            finalImagesArray.optJSONObject(count).remove("data");
                            finalImagesArray.optJSONObject(count).put("data", message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        count = count + 1;
                        try {
                            uploadImages();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {
            if (postContent.getText().toString().trim().length() != 0) {
                try {
                    postFeed();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.toast(PostFeedActivity.this, getResources().getString(R.string.please_put_same_caption));
            }
        }

    }

    private void postFeed() throws JSONException {


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("contentType", postType);
        jsonObject.put("captionTags", hashTagArray.toString());
        jsonObject.put("linkData", finalImagesArray.toString());
        jsonObject.put("tagUsers", usersArray.toString());
        jsonObject.put("caption", postContent.getText().toString());
        jsonObject.put("hashTag", new JSONArray().toString());
        if (commentsDetect.isChecked()) {
            jsonObject.put("commentingStatus", 1);
        } else {
            jsonObject.put("commentingStatus", 0);

        }
        jsonObject.put("latitude", latitude);
        jsonObject.put("longitude", longitude);
        jsonObject.put("area", areaname);
        jsonObject.put("city", cityname);
        jsonObject.put("newHashTag", Utils.getHashtagLists(postContent.getText().toString(), hashTagArray).toString());
        ApiCall.PostMethod(PostFeedActivity.this, UrlHelper.NEW_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(PostFeedActivity.this, getResources().getString(R.string.image_posted));
                moveMainActivity();
            }
        });

    }

    private void moveMainActivity() {
        Intent intent = new Intent(PostFeedActivity.this, MainActivity.class);
        intent.putExtra("showImageUpload", "false");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        finalImagesArray = new JSONArray();
        multipleImages = intent.getStringArrayListExtra("images");
        multipleImagesContentType = intent.getStringArrayListExtra("contentType");
        postType = intent.getStringExtra("type");

        int totalCoun = Integer.parseInt(intent.getStringExtra("totalImage"));
        for (int i = 0; i < totalCoun; i++) {
            byte[] b = Base64.decode(SharedPreference.getKey(PostFeedActivity.this, "bitmap_" + i), Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            bitmapArray.add(bitmap);
        }


        for (int i = 0; i < multipleImages.size(); i++) {
            if (multipleImagesContentType.get(i).equalsIgnoreCase("video")) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", multipleImagesContentType.get(i));
                    jsonObject.put("data", multipleImages.get(i));
                    jsonObject.put("thumbnail", "");
                    finalImagesArray.put(i, jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        Log.d(TAG, "getIntentValues: " + finalImagesArray);

        if (multipleImages.size() > 1) {
            isMultiple.setVisibility(View.VISIBLE);
        } else {
            isMultiple.setVisibility(View.GONE);
        }
        Glide.with(PostFeedActivity.this).load(multipleImages.get(0)).into(postImage);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
                addLocation.setVisibility(View.GONE);
                afterLocationSelected.setVisibility(View.VISIBLE);


                StringTokenizer st = new StringTokenizer(data.getStringExtra("place_details"), ",");
                /************  Set Model values in Holder elements ***********/

                areaname = st.nextToken();
                place_name.setText(areaname);
                String desc_detail = "";
                for (int i = 1; i < st.countTokens(); i++) {
                    if (i == st.countTokens() - 1) {
                        desc_detail = desc_detail + st.nextToken();
                    } else {
                        desc_detail = desc_detail + st.nextToken() + ",";
                    }
                }
                cityname = desc_detail;
                place_detail.setText(desc_detail);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void initViews() {
        postImage = findViewById(R.id.postImage);
        isMultiple = findViewById(R.id.isMultiple);
        postContent = findViewById(R.id.postContent);
        shareText = findViewById(R.id.shareText);
        commentsDetect = findViewById(R.id.commentsDetect);
        addLocation = findViewById(R.id.addLocation);
        place_detail = findViewById(R.id.place_detail);
        place_name = findViewById(R.id.place_name);
        closeIcon = findViewById(R.id.closeIcon);
        afterLocationSelected = findViewById(R.id.afterLocationSelected);
        hashTagLists = findViewById(R.id.hashTagLists);
        suggestionLists = findViewById(R.id.suggestionLists);
        addLocation.setVisibility(View.VISIBLE);
        afterLocationSelected.setVisibility(View.GONE);
        hashTagLists.setVisibility(View.GONE);

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
