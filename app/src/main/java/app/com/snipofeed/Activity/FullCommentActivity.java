package app.com.snipofeed.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import app.com.snipofeed.Adapters.CommentsAdapter;
import app.com.snipofeed.Events.ReplyToEvent;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FullCommentActivity extends AppCompatActivity {
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";
    String postId;
    RecyclerView commentsRecyclerView;
    CommentsAdapter commentsAdapter;
    AppSettings appSettings;
    TextView postComment, userNameReply;
    EditText commentText;
    CardView replyToLayout;
    ImageView closeIcon;
    int position;
    boolean isReplyingComment = false;
    String commentId;
    CircleImageView userImage;
    RelativeLayout contentRoot;
    private int drawingStartLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);

        setContentView(R.layout.activity_full_comment);
        appSettings = new AppSettings(FullCommentActivity.this);


        try {
            initViews();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListeners();
        setClickListener();

    }


    private void setClickListener() {

        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isReplyingComment = false;
                commentId = postId;
                replyToLayout.setVisibility(View.GONE);

            }
        });

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void initListeners() {
        postComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isReplyingComment) {
                    try {
                        replyComments();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        postCommentValues();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void replyComments() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("replyData", commentText.getText().toString());
        jsonObject.put("commentId", commentId);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(FullCommentActivity.this, UrlHelper.REPLY_COMMENTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONObject newlyAddedObj = new JSONObject();
                try {
                    newlyAddedObj.put("id", response.optString("id"));
                    newlyAddedObj.put("userId", response.optString("userId"));
                    newlyAddedObj.put("profilePic", response.optString("profilePic"));
                    newlyAddedObj.put("name", response.optString("name"));
                    newlyAddedObj.put("repliedAt", response.optString("repliedAt"));
                    newlyAddedObj.put("reply", commentText.getText().toString());
                    newlyAddedObj.put("parentPos", position);
                    commentText.setText("");
                    commentsAdapter.addReplyJsonObject(newlyAddedObj, position);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    private void startIntroAnimation() {

        contentRoot.setScaleY(0.1f);
        contentRoot.setPivotY(drawingStartLocation);

        contentRoot.animate()
                .scaleY(1)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animateContent();
                    }
                })
                .start();
    }

    private void animateContent() {
//        commentsAdapter.updateItems();

    }


    private void postCommentValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("comments", commentText.getText().toString());
        jsonObject.put("Postsid", commentId);
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(FullCommentActivity.this, UrlHelper.POST_COMMENTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                JSONObject newlyAddedObj = new JSONObject();
                commentText.setText("");

                if (commentsAdapter != null) {
                    try {
                        newlyAddedObj.put("commentId", response.optString("commentId"));
                        newlyAddedObj.put("commentedAt", Utils.changeTimeFormats(response.optString("commentedAt")));
                        newlyAddedObj.put("postId", response.optString("postId"));
                        newlyAddedObj.put("userId", response.optString("userId"));
                        newlyAddedObj.put("comment", response.optString("comment"));
                        newlyAddedObj.put("profilePic", response.optString("profilePic"));
                        newlyAddedObj.put("name", response.optString("name"));
                        newlyAddedObj.put("comment_likes", "0");
                        newlyAddedObj.put("user_liked_status", "false");
                        newlyAddedObj.put("comment_replyes", new JSONArray());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    commentsAdapter.addNewObject(newlyAddedObj);
                    commentsRecyclerView.smoothScrollToPosition(commentsAdapter.getItemCount());
                } else {
                    try {
                        getData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private void getData() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", postId);
        ApiCall.PostMethod(FullCommentActivity.this, UrlHelper.SHOW_COMMENTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = response.optJSONArray("PostComments");
                if (jsonArray.length() > 0) {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FullCommentActivity.this, LinearLayoutManager.VERTICAL, false);
                    commentsRecyclerView.setLayoutManager(linearLayoutManager);
                    commentsAdapter = new CommentsAdapter(FullCommentActivity.this, jsonArray);
                    commentsRecyclerView.setAdapter(commentsAdapter);
                    commentsRecyclerView.smoothScrollToPosition(jsonArray.length() - 1);
                } else {
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FullCommentActivity.this, LinearLayoutManager.VERTICAL, false);
                    commentsRecyclerView.setLayoutManager(linearLayoutManager);
                    commentsAdapter = new CommentsAdapter(FullCommentActivity.this, new JSONArray());
                    commentsRecyclerView.setAdapter(commentsAdapter);
                    commentsRecyclerView.smoothScrollToPosition(jsonArray.length() - 1);
                }

            }
        });
    }


    private void initViews() throws JSONException {

        postId = getIntent().getStringExtra("postId");
        commentId = postId;
        commentsRecyclerView = findViewById(R.id.commentsRecyclerView);
        replyToLayout = findViewById(R.id.replyToLayout);
        closeIcon = findViewById(R.id.closeIcon);
        userNameReply = findViewById(R.id.userNameReply);
        postComment = findViewById(R.id.postComment);
        commentText = findViewById(R.id.commentText);
        contentRoot = findViewById(R.id.contentRoot);
        userImage = findViewById(R.id.userImage);
        getData();
        Glide.with(FullCommentActivity.this).load(appSettings.getUserImage()).apply(Utils.getRequestOptions()).into(userImage);


    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowReplyToView(ReplyToEvent event) {

        replyToLayout.setVisibility(View.VISIBLE);
        userNameReply.setText(getResources().getString(R.string.replying_to) + " " + event.getUserName());
        isReplyingComment = true;

        commentId = event.getCommentId();
        position = event.getPosition();
        commentText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(commentText, InputMethodManager.SHOW_IMPLICIT);

        /* Do something */
    }

    ;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
