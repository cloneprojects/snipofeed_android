package app.com.snipofeed.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.PromotionFeedAdapter;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PreviewActivity extends AppCompatActivity {

    JSONObject jsonObject = new JSONObject();
    RecyclerView promotionFeed;
    ImageView closeButton;
    JSONArray jsonArray = new JSONArray();
    String postId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        try {
            jsonObject = new JSONObject(getIntent().getStringExtra("post_details"));
            Utils.log(PreviewActivity.class.getSimpleName(),jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        initViews();
        initListners();

        initBottomRecyclerView(jsonObject);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    private void initListners() {

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
    }

    private void initViews() {
        promotionFeed = findViewById(R.id.promotionFeed);
        closeButton = findViewById(R.id.closeButton);
        initBottomRecyclerView(jsonObject);
        postId = jsonObject.optString("id");


    }

    private void initBottomRecyclerView(JSONObject jsonObject) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        PromotionFeedAdapter promotionFeedTopAdapter = new PromotionFeedAdapter(PreviewActivity.this, jsonObject, getSupportFragmentManager(), 1);
        promotionFeed.setLayoutManager(linearLayoutManager);
        promotionFeed.setAdapter(promotionFeedTopAdapter);
    }

}
