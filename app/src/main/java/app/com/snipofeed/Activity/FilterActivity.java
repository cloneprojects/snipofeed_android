package app.com.snipofeed.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;
import com.zomato.photofilters.FilterPack;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.utils.ThumbnailCallback;
import com.zomato.photofilters.utils.ThumbnailItem;
import com.zomato.photofilters.utils.ThumbnailsManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.Adapters.FilterImagesAdapter;
import app.com.snipofeed.Adapters.ThumbnailsAdapter;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.SharedPreference;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FilterActivity extends AppCompatActivity implements ThumbnailCallback {
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    RecyclerView filterList;
    Bitmap singleBitmap;
    RecyclerViewPager filterImages;
    ImageView placeHolderImageView;
    int drawable;
    int pageSelected = 0;
    ImageView backButton;
    TextView nextButton;
    JSONArray finalImagesArray = new JSONArray();
    List<Filter> filters;
    ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
    private ArrayList<String> multipleImages = new ArrayList<>();
    private ArrayList<String> multipleImagesContentType = new ArrayList<>();
    private FilterImagesAdapter topAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_filter);
        filters = FilterPack.getFilterPack(getApplicationContext());
        initViews();
        initListners();
        getIntentValues();


    }

    private void initListners() {

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getIntentValues() {
        Intent intent = getIntent();
        finalImagesArray = new JSONArray();
        multipleImages = intent.getStringArrayListExtra("images");
        multipleImagesContentType = intent.getStringArrayListExtra("contentType");

        bitmapArray = new ArrayList<>();
        for (int i = 0; i < multipleImages.size(); i++) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", multipleImagesContentType.get(i));
                jsonObject.put("data", multipleImages.get(i));
                jsonObject.put("thumbnail", "");
                finalImagesArray.put(i, jsonObject);
                if (multipleImagesContentType.get(i).equalsIgnoreCase("image")) {
                    bitmapArray.add(Utils.getBitmapFrompath(multipleImages.get(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (bitmapArray.size() > 1) {
            filterImages.setVisibility(View.VISIBLE);
            placeHolderImageView.setVisibility(View.GONE);

            LinearLayoutManager newlayoutManager = new LinearLayoutManager(this);
            newlayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            newlayoutManager.scrollToPosition(0);
            filterImages.setLayoutManager(newlayoutManager);
            filterImages.setHasFixedSize(true);

            topAdapter = new FilterImagesAdapter(FilterActivity.this, bitmapArray);
            filterImages.setAdapter(topAdapter);
            topAdapter.notifyDataSetChanged();


        } else {

            filterImages.setVisibility(View.GONE);
            placeHolderImageView.setVisibility(View.VISIBLE);
            placeHolderImageView.setImageBitmap(filters.get(0).processFilter(bitmapArray.get(0)));


        }


        singleBitmap = Utils.getBitmapFrompath(multipleImages.get(pageSelected));

    }


    private void initViews() {
        filterList = findViewById(R.id.filterList);
        nextButton = findViewById(R.id.nextButton);
        backButton = findViewById(R.id.backButton);
        filterImages = findViewById(R.id.filterImages);
        placeHolderImageView = findViewById(R.id.placeHolderImageView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        filterList.setLayoutManager(layoutManager);
        filterList.setHasFixedSize(true);
        drawable = R.drawable.rajkumar;


        filterImages.setItemViewCacheSize(20);
        filterImages.setDrawingCacheEnabled(true);
        filterImages.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        bindDataToAdapter();

        filterImages.addOnPageChangedListener(new RecyclerViewPager.OnPageChangedListener() {
            @Override
            public void OnPageChanged(int i, int i1) {
                Log.d("asdad", "OnPageChanged: " + filterImages.getCurrentPosition());
                pageSelected = filterImages.getCurrentPosition();

            }
        });

        filterImages.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (filterImages.getChildCount() < 3) {
                    if (filterImages.getChildAt(1) != null) {
                        if (filterImages.getCurrentPosition() == 0) {
                            View v1 = filterImages.getChildAt(1);
//                            v1.setScaleY(0.9f);
                            v1.setScaleX(0.95f);
                        } else {
                            View v1 = filterImages.getChildAt(0);
//                            v1.setScaleY(0.9f);
                            v1.setScaleX(0.95f);
                        }
                    }
                } else {
                    if (filterImages.getChildAt(0) != null) {
                        View v0 = filterImages.getChildAt(0);
//                        v0.setScaleY(0.9f);
                        v0.setScaleX(0.95f);
                    }
                    if (filterImages.getChildAt(2) != null) {
                        View v2 = filterImages.getChildAt(2);
//                        v2.setScaleY(0.9f);
                        v2.setScaleX(0.95f);
                    }
                }

            }
        });

        filterImages.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                Log.d("asdasd", "onScrollStateChanged: " + scrollState);

                if (scrollState == 0) {
                    pageSelected = filterImages.getCurrentPosition();

                }

            }

            @Override
            public void onScrolled(final RecyclerView recyclerView, int i, int i2) {

                /*int visibleItemCount = recyclerView.getChildCount();
                Log.e("visibleItemCount",""+visibleItemCount);*/

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int childCount = filterImages.getChildCount();
                        int width = filterImages.getChildAt(0).getWidth();
                        int padding = (filterImages.getWidth() - width) / 2;
                        Log.e("padding", "" + padding);

                        for (int j = 0; j < childCount; j++) {
                            View v = recyclerView.getChildAt(j);
                            float rate = 0;
                            Log.e("v.getLeft()", "" + v.getLeft());
                            if (v.getLeft() <= padding) {
                                if (v.getLeft() >= padding - v.getWidth()) {
                                    rate = (padding - v.getLeft()) * 1f / v.getWidth();
                                } else {
                                    rate = 1;
                                }

                                v.setScaleX(1 - rate * 0.05f);

                            } else {

                                if (v.getLeft() <= recyclerView.getWidth() - padding) {
                                    rate = (recyclerView.getWidth() - padding - v.getLeft()) * 1f / v.getWidth();
                                }
                                v.setScaleX(0.9f + rate * 0.05f);
                            }
                        }
                    }
                });


            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, PostFeedActivity.class);
                intent.putStringArrayListExtra("images", multipleImages);
                intent.putStringArrayListExtra("contentType", multipleImagesContentType);
                intent.putExtra("type", "image");
                if (bitmapArray.size() > 1) {
                    intent.putExtra("single", "false");
                    intent.putExtra("totalImage", "" + topAdapter.getBitmapArray().size());
                    for (int i = 0; i < topAdapter.getBitmapArray().size(); i++) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        topAdapter.getBitmapArray().get(i).compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                        SharedPreference.putKey(FilterActivity.this, "bitmap_" + i, encodedImage);
                    }

                } else {
                    intent.putExtra("single", "true");
                    intent.putExtra("totalImage", "1");
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    singleBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                    SharedPreference.putKey(FilterActivity.this, "bitmap_0", encodedImage);


                }
                startActivity(intent);
            }
        });


    }

    private void bindDataToAdapter() {
        final Context context = this.getApplication();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                Bitmap thumbImage;

                thumbImage = Utils.getBitmapFrompath(multipleImages.get(0));

                ThumbnailsManager.clearThumbs();


                for (Filter filter : filters) {
                    ThumbnailItem thumbnailItem = new ThumbnailItem();
                    thumbnailItem.image = thumbImage;
                    thumbnailItem.filter = filter;
                    thumbnailItem.filterName = filter.getName();
                    ThumbnailsManager.addThumb(thumbnailItem);
                }

                List<ThumbnailItem> thumbs = ThumbnailsManager.processThumbs(context);

                ThumbnailItem thumbnailItem = new ThumbnailItem();
                thumbnailItem.image = thumbImage;
                thumbnailItem.filter = null;
                thumbnailItem.filterName = getResources().getString(R.string.normal);
                thumbs.add(0, thumbnailItem);

                ThumbnailsAdapter adapter = new ThumbnailsAdapter(thumbs, FilterActivity.this, bitmapArray.size());
                filterList.setAdapter(adapter);

                adapter.notifyDataSetChanged();
            }
        };
        handler.post(r);

    }


    @Override
    public void onThumbnailClick(Filter filter) {
        if (filter != null) {
            if (bitmapArray.size() == 1) {
                singleBitmap = filter.processFilter(Utils.getBitmapFrompath(multipleImages.get(pageSelected)));
                placeHolderImageView.setImageBitmap(singleBitmap);
            } else {
                topAdapter.changeBitmap(filter.processFilter(Utils.getBitmapFrompath(multipleImages.get(pageSelected))), pageSelected);
            }
        } else {
            if (bitmapArray.size() == 1) {
                singleBitmap = filter.processFilter(Utils.getBitmapFrompath(multipleImages.get(pageSelected)));
                placeHolderImageView.setImageBitmap(Utils.getBitmapFrompath(multipleImages.get(pageSelected)));
            } else {
                topAdapter.changeBitmap(Utils.getBitmapFrompath(multipleImages.get(pageSelected)), pageSelected);
            }
        }
    }
}
