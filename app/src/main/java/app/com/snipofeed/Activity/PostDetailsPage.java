package app.com.snipofeed.Activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Fragment.HashTagFragment;
import app.com.snipofeed.Fragment.OthersProfileFragment;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

public class PostDetailsPage extends AppCompatActivity {

    View view;
    RecyclerRefreshLayout refresh_layout;
    RecyclerView postFeedRecyclerView;
    PostFeedAdapter postFeedAdapter;
    AppSettings appSettings;
    boolean isSinglePost = true;
    onBottomReachedListner onBottomReachedListner;

    JSONArray newsPosts = new JSONArray();
    int currentpageNumber = 1;
    int totalpageNumber = 1;

    public static float dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return dpValue * scale;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//             Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_post_details_page);
        initViews();


    }


    private void initViews() {

        appSettings = new AppSettings(PostDetailsPage.this);

        postFeedRecyclerView = findViewById(R.id.postFeedRecyclerView);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(PostDetailsPage.this, 120), (int) dip2px(PostDetailsPage.this, 120));


        if (getIntent().getStringExtra("singlePost").equalsIgnoreCase("true")) {
            isSinglePost = true;
        } else {
            isSinglePost = false;

        }

        if (!isSinglePost) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("postDetails"));
                newsPosts.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PostDetailsPage.this, LinearLayoutManager.VERTICAL, false);
            postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(PostDetailsPage.this));
            postFeedAdapter = new PostFeedAdapter(PostDetailsPage.this, newsPosts, PostDetailsPage.this.getSupportFragmentManager());
            postFeedRecyclerView.setAdapter(postFeedAdapter);
            postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                @Override
                public void onClicked(String username) {
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username);
                    OthersProfileFragment newFragment = new OthersProfileFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutPostDetails, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();
                }
            });
            postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                @Override
                public void onClicked(String hashtag, String caption_tags) {

                    Bundle bundle = new Bundle();
                    bundle.putString("hashTagId", caption_tags);
                    HashTagFragment newFragment = new HashTagFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutPostDetails, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();


                }
            });


            postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                @Override
                public void onClicked(String username, int caption_tags) {
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username);
                    OthersProfileFragment newFragment = new OthersProfileFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutPostDetails, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();
                }
            });


        } else {
            try {
                getInitialData();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //        refresh_layout = view.findViewById(R.id.refresh_layout);
//        refresh_layout.setRefreshInitialOffset(50);
//        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
//        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                try {
//                    getInitialData();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    private void getInitialData() throws JSONException {
        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", getIntent().getStringExtra("postId"));
        ApiCall.PostMethod(PostDetailsPage.this, UrlHelper.VIEW_POST_DETAILS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
//                try {
//                    refresh_layout.setRefreshing(false);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
                newsPosts = response.optJSONArray("Postdetails");

//                try {
//                    totalpageNumber = Integer.parseInt(response.optString("pageCount"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PostDetailsPage.this, LinearLayoutManager.VERTICAL, false);
                postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(PostDetailsPage.this));
                postFeedAdapter = new PostFeedAdapter(PostDetailsPage.this, newsPosts, PostDetailsPage.this.getSupportFragmentManager());
                postFeedRecyclerView.setAdapter(postFeedAdapter);
                postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                    @Override
                    public void onBottomReached(int position) {

//                        try {
//                            getNextData();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                });


                postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                    @Override
                    public void onClicked(String hashtag, String caption_tags) {


                        Bundle bundle = new Bundle();
                        bundle.putString("hashTagId", caption_tags);
                        HashTagFragment newFragment = new HashTagFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutPostDetails, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();


                    }
                });


                postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                    @Override
                    public void onClicked(String username, int caption_tags) {
                        Bundle bundle = new Bundle();
                        bundle.putString("username", username);
                        OthersProfileFragment newFragment = new OthersProfileFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutPostDetails, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    }
                });

            }
        });
    }

    @Subscribe
    public void onBookmarked(onBookmarked onBookmarked) {
        postFeedAdapter.changeBookmarkStatus(onBookmarked.getBookMarkStatus(), onBookmarked.getPosition());
    }


    @Subscribe
    public void PostLiked(PostLiked postLiked) {
        postFeedAdapter.changeLikeStatus(postLiked.getLikeStatus(), postLiked.getPosition());
    }

    @Subscribe
    public void unFollow(unFollow unFollow) {
        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getNextData() throws JSONException {
        currentpageNumber = currentpageNumber + 1;
        if (currentpageNumber <= totalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentpageNumber);
            ApiCall.PostMethod(PostDetailsPage.this, UrlHelper.VIEW_POST_DETAILS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(PostDetailsPage.this, response.optString("message"));
                    } else {
                        try {
                            totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("NewsPosts");
                        postFeedAdapter.addValues(duy);

                    }


                }
            });
        }

    }


}
