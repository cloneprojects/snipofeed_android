package app.com.snipofeed.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Helper.AnimationHelper;
import app.com.snipofeed.Helper.ConstantValues;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PromoteActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = PromoteActivity.class.getSimpleName();
    View viewOne, viewTwo, viewThree, viewFour;
    TextView editText, userNameText, webSiteName;
    RelativeLayout webSiteLayout, profileLayout;
    ImageView profileRound, webSiteRound;
    String action = ConstantValues.LEARN_MORE;
    AppSettings appSettings = new AppSettings(PromoteActivity.this);
    int defaultPosition = 0;
    String finalWebUrl = "";
    boolean isProfileSelected = true;
    String postId;
    String goal = "0";
    String radius = "0";
    String ageMin = "0";
    String ageMax = "0";
    String gender = "0";
    String latitude = "0";
    String longitude = "0";
    String audienceType = "0";
    String amountValue = "0";
    TextView est_profile_reach, est_profile_visits;
    TextView estReach, estProfileVisit;
    LinearLayout actionButtonLayout;
    JSONObject jsonObject = new JSONObject();
    LinearLayout previewLayout;
    private ImageView backButton;
    private ImageView maleRound;
    private ImageView femaleRound;
    private ImageView nextButton;
    private LinearLayout goalLayout;
    private TextView automaticText;
    private ImageView automaticRound;
    private RelativeLayout automaticLayout;
    private TextView localtext;
    private ImageView localRound;
    private RelativeLayout localLayout;
    private ImageView audienceImage;
    private TextView audienceText;
    private LinearLayout audienceLayout;
    private android.support.v7.widget.CardView topCard;
    private TextView text1;
    private TextView text2;
    private TextView amountLayout;
    private TextView reachTextOne;
    private TextView amountTextOne;
    private RelativeLayout cardOne;
    private TextView reachTextTwo;
    private TextView amountTextTwo;
    private RelativeLayout cardTwo;
    private TextView reachTextThree;
    private TextView amountTextThree;
    private RelativeLayout cardThree;
    private TextView reachTextFour;
    private TextView amountTextFour;
    private RelativeLayout cardFour;
    private LinearLayout chooseAmountLayout, reviewLayout;
    private ImageView cardOneImage;
    private ImageView cardTwoImage;
    private ImageView cardThreeImage;
    private ImageView cardFourImage;
    private TextView outcomeText;
    private TextView destinationText;
    private TextView actionButtonText;
    private TextView audienceTextPreview;
    private TextView budgetText;
    private Button createPromotion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_promote);

        initViews();
        initListeners();
        setValues();
        setProfileSelected();
        setViewOne();
    }

    private void setValues() {
        postId = getIntent().getStringExtra("postId");
        userNameText.setText(appSettings.getUserName());
        webSiteName.setText(appSettings.getWebsite());
        finalWebUrl = appSettings.getWebsite();

    }

    private void initListeners() {
        webSiteLayout.setOnClickListener(this);
        profileLayout.setOnClickListener(this);
        editText.setOnClickListener(this);
        localLayout.setOnClickListener(this);
        automaticLayout.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        cardOne.setOnClickListener(this);
        cardTwo.setOnClickListener(this);
        cardThree.setOnClickListener(this);
        cardFour.setOnClickListener(this);
        previewLayout.setOnClickListener(this);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initViews() {

        audienceLayout = (LinearLayout) findViewById(R.id.audienceLayout);
        actionButtonLayout = (LinearLayout) findViewById(R.id.actionButtonLayout);
        audienceText = (TextView) findViewById(R.id.audienceText);
        audienceImage = (ImageView) findViewById(R.id.audienceImage);
        localLayout = (RelativeLayout) findViewById(R.id.localLayout);
        localRound = (ImageView) findViewById(R.id.localRound);
        localtext = (TextView) findViewById(R.id.local_text);
        automaticLayout = (RelativeLayout) findViewById(R.id.automaticLayout);
        automaticRound = (ImageView) findViewById(R.id.automaticRound);
        automaticText = (TextView) findViewById(R.id.automaticText);
        goalLayout = (LinearLayout) findViewById(R.id.goalLayout);
        nextButton = (ImageView) findViewById(R.id.nextButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        viewOne = findViewById(R.id.viewOne);
        viewTwo = findViewById(R.id.viewTwo);
        viewThree = findViewById(R.id.viewThree);
        viewFour = findViewById(R.id.viewFour);
        editText = findViewById(R.id.editText);
        userNameText = findViewById(R.id.userNameText);
        webSiteName = findViewById(R.id.webSiteName);
        profileLayout = findViewById(R.id.profileLayout);
        webSiteLayout = findViewById(R.id.webSiteLayout);
        webSiteRound = findViewById(R.id.webSiteRound);
        profileRound = findViewById(R.id.profileRound);
        maleRound = findViewById(R.id.maleRound);
        femaleRound = findViewById(R.id.femaleRound);
        previewLayout = findViewById(R.id.previewLayout);

        createPromotion = (Button) findViewById(R.id.createPromotion);
        budgetText = (TextView) findViewById(R.id.budgetText);
        audienceTextPreview = (TextView) findViewById(R.id.audienceTextPreview);
        actionButtonText = (TextView) findViewById(R.id.actionButtonText);
        destinationText = (TextView) findViewById(R.id.destinationText);
        outcomeText = (TextView) findViewById(R.id.outcomeText);
        chooseAmountLayout = (LinearLayout) findViewById(R.id.chooseAmountLayout);
        cardFour = (RelativeLayout) findViewById(R.id.cardFour);
        amountTextFour = (TextView) findViewById(R.id.amountTextFour);
        reachTextFour = (TextView) findViewById(R.id.reachTextFour);
        cardFourImage = (ImageView) findViewById(R.id.cardFourImage);
        cardThree = (RelativeLayout) findViewById(R.id.cardThree);
        amountTextThree = (TextView) findViewById(R.id.amountTextThree);
        reachTextThree = (TextView) findViewById(R.id.reachTextThree);
        est_profile_visits = (TextView) findViewById(R.id.est_profile_visits);
        est_profile_reach = (TextView) findViewById(R.id.est_profile_reach);
        cardThreeImage = (ImageView) findViewById(R.id.cardThreeImage);
        cardTwo = (RelativeLayout) findViewById(R.id.cardTwo);
        amountTextTwo = (TextView) findViewById(R.id.amountTextTwo);
        reachTextTwo = (TextView) findViewById(R.id.reachTextTwo);
        cardTwoImage = (ImageView) findViewById(R.id.cardTwoImage);
        cardOne = (RelativeLayout) findViewById(R.id.cardOne);
        amountTextOne = (TextView) findViewById(R.id.amountTextOne);
        reachTextOne = (TextView) findViewById(R.id.reachTextOne);
        cardOneImage = (ImageView) findViewById(R.id.cardOneImage);
        amountLayout = (TextView) findViewById(R.id.amountLayout);
        audienceLayout = (LinearLayout) findViewById(R.id.audienceLayout);
        audienceText = (TextView) findViewById(R.id.audienceText);
        audienceImage = (ImageView) findViewById(R.id.audienceImage);
        localLayout = (RelativeLayout) findViewById(R.id.localLayout);
        localRound = (ImageView) findViewById(R.id.localRound);
        localtext = (TextView) findViewById(R.id.local_text);
        automaticLayout = (RelativeLayout) findViewById(R.id.automaticLayout);
        automaticRound = (ImageView) findViewById(R.id.automaticRound);
        automaticText = (TextView) findViewById(R.id.automaticText);
        goalLayout = (LinearLayout) findViewById(R.id.goalLayout);
        webSiteLayout = (RelativeLayout) findViewById(R.id.webSiteLayout);
        editText = (TextView) findViewById(R.id.editText);
        webSiteName = (TextView) findViewById(R.id.webSiteName);
        webSiteRound = (ImageView) findViewById(R.id.webSiteRound);
        text2 = (TextView) findViewById(R.id.text_2);
        profileLayout = (RelativeLayout) findViewById(R.id.profileLayout);
        userNameText = (TextView) findViewById(R.id.userNameText);
        profileRound = (ImageView) findViewById(R.id.profileRound);
        text1 = (TextView) findViewById(R.id.text_1);
        estProfileVisit = (TextView) findViewById(R.id.estProfileVisit);
        estReach = (TextView) findViewById(R.id.estReach);
        viewFour = (View) findViewById(R.id.viewFour);
        viewThree = (View) findViewById(R.id.viewThree);
        viewTwo = (View) findViewById(R.id.viewTwo);
        viewOne = (View) findViewById(R.id.viewOne);
        topCard = (CardView) findViewById(R.id.topCard);
        reviewLayout = findViewById(R.id.reviewLayout);
        nextButton = (ImageView) findViewById(R.id.nextButton);
        backButton = (ImageView) findViewById(R.id.backButton);

    }

    public void setViewOne() {
        viewOne.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewTwo.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));
        viewThree.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));
        viewFour.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));

    }

    public void setProfileSelected() {
        goal = "1";
        outcomeText.setText(getResources().getString(R.string.profile_visits));
        destinationText.setText("@" + appSettings.getUserName());
        isProfileSelected = true;
        userNameText.setVisibility(View.VISIBLE);
        webSiteName.setVisibility(View.INVISIBLE);
        editText.setVisibility(View.INVISIBLE);
        profileRound.setImageResource(R.drawable.circle_round_blue);
        webSiteRound.setImageResource(R.drawable.circle_round_grey);
    }


    public void setwebSiteSelected() {
        isProfileSelected = false;
        goal = "2";
        destinationText.setText(appSettings.getWebsite());
        outcomeText.setText(getResources().getString(R.string.website_reach));
        userNameText.setVisibility(View.INVISIBLE);
        webSiteName.setVisibility(View.VISIBLE);
        editText.setVisibility(View.VISIBLE);
        webSiteRound.setImageResource(R.drawable.circle_round_blue);
        profileRound.setImageResource(R.drawable.circle_round_grey);
    }


    public void setAutomatic() {
        audienceType = "1";
        audienceTextPreview.setText(getResources().getString(R.string.automatic));
        automaticRound.setImageResource(R.drawable.circle_round_blue);
        localRound.setImageResource(R.drawable.circle_round_grey);
        audienceImage.setImageResource(R.drawable.automatic_audience);

    }

    public void setLocal() {
        audienceType = "2";
        audienceTextPreview.setText(getResources().getString(R.string.local));
        automaticRound.setImageResource(R.drawable.circle_round_grey);
        localRound.setImageResource(R.drawable.circle_round_blue);
        audienceImage.setImageResource(R.drawable.local_audience);
        moveLocalActivity();
    }

    private void moveLocalActivity() {
        Intent intent = new Intent(PromoteActivity.this, AudienceChooseActivity.class);
        startActivityForResult(intent, 2);
    }

    public void setViewTwo() {
        viewOne.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewTwo.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewThree.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));
        viewFour.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));
    }


    public void setViewThree() {
        viewOne.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewTwo.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewThree.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewFour.setBackground(getResources().getDrawable(R.drawable.grey_round_progress));
    }

    public void setViewFour() {
        viewOne.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewTwo.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewThree.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
        viewFour.setBackground(getResources().getDrawable(R.drawable.blue_round_progress));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                webSiteName.setText(data.getStringExtra("website"));
                action = data.getStringExtra("action");
                finalWebUrl = data.getStringExtra("website");
                destinationText.setText(finalWebUrl);
                actionButtonText.setText(action);
            }
        }
        if (requestCode == 2) {


            if (resultCode == RESULT_OK) {
                radius = data.getStringExtra("radius");
                ageMin = data.getStringExtra("min");
                ageMax = data.getStringExtra("max");
                gender = data.getStringExtra("gender");
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
//                Log.e(TAG, "onActivityResult:" + data.getStringExtra("latitude"));
//                Log.e(TAG, "onActivityResult:" + data.getStringExtra("longitude"));
//                Log.e(TAG, "onActivityResult:" + data.getStringExtra("gender"));
//                Log.e(TAG, "onActivityResult:" + data.getStringExtra("min"));
//                Log.e(TAG, "onActivityResult:" + data.getStringExtra("max"));
//                Log.e(TAG, "onActivityResult:" +);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == profileLayout) {
            setProfileSelected();
        } else if (v == webSiteLayout) {
            setwebSiteSelected();
        } else if (v == editText) {
            moveEditWebSite();
        } else if (v == localLayout) {
            setLocal();
        } else if (v == automaticLayout) {
            setAutomatic();
        } else if (v == nextButton) {
            if (defaultPosition == 0) {
                if (isProfileSelected) {
                    defaultPosition = defaultPosition + 1;
                    placeAudienceLayout();
                } else {
                    if (validateWebSiteDetails().equalsIgnoreCase("true")) {
                        defaultPosition = defaultPosition + 1;
                        placeAudienceLayout();
                    } else {
                        Utils.toast(PromoteActivity.this, validateWebSiteDetails());
                    }
                }
            } else if (defaultPosition == 1) {
                defaultPosition = defaultPosition + 1;
                placeChooseAmountLayout();
            } else if (defaultPosition == 2) {
                defaultPosition = defaultPosition + 1;
                placeReviewLayout();

            } else if (defaultPosition == 3) {

                try {
                    hitAPI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (v == backButton) {
            if (defaultPosition == 1) {
                defaultPosition = defaultPosition - 1;
                placeGoalLayout();
            } else if (defaultPosition == 2) {
                defaultPosition = defaultPosition - 1;
                placeAudienceLayoutBack();
            } else if (defaultPosition == 3) {
                defaultPosition = defaultPosition - 1;
                placeReviewLayoutBack();
            }
        } else if (v == cardOne) {
            makeAllCardDefault();
            selectCardOneLayout();
        } else if (v == cardTwo) {
            makeAllCardDefault();

            selectCardTwoLayout();

        } else if (v == cardThree) {
            makeAllCardDefault();

            selectCardThreeLayout();
        } else if (v == cardFour) {
            makeAllCardDefault();

            selectCardFourLayout();

        } else if (v == previewLayout) {
            movePreviewActivity();
        }
    }

    private void movePreviewActivity() {
        Intent intent = new Intent(PromoteActivity.this, PreviewActivity.class);

        try {
            JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("postDetails"));
            jsonObject.put("isPromoted", "1");
            jsonObject.put("websiteUrl", finalWebUrl);
            jsonObject.put("actionButton", action);
            intent.putExtra("post_details", jsonObject.toString());
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void hitAPI() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", postId);
        jsonObject.put("goalType", goal);
        jsonObject.put("audienceType", audienceType);
        jsonObject.put("radius", radius);
        jsonObject.put("ageMin", ageMin);
        jsonObject.put("ageMax", ageMax);
        jsonObject.put("gender", gender);
        jsonObject.put("targetName", "0");
        jsonObject.put("targetLat", latitude);
        jsonObject.put("targetLon", longitude);
        jsonObject.put("intrest", "0");
        jsonObject.put("amount", amountValue);
        jsonObject.put("websiteUrl", finalWebUrl);
        jsonObject.put("actionButton", action);
        ApiCall.PostMethod(PromoteActivity.this, UrlHelper.PROMOTE_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(PromoteActivity.this, getResources().getString(R.string.post_promoted));
                moveMainActivity();
            }
        });
    }

    public String validateWebSiteDetails() {
        if (finalWebUrl.trim().length() == 0) {
            return getResources().getString(R.string.please_enter_valid_website_to_promote);
        } else if (!Utils.isValidUrl(finalWebUrl.trim())) {
            return getResources().getString(R.string.please_enter_valid_website_to_promote);

        } else {
            return "true";
        }
    }

    public void makeAllCardDefault() {
        amountTextOne.setTextColor(getResources().getColor(R.color.black));
        reachTextOne.setTextColor(getResources().getColor(R.color.black));
        cardOneImage.setImageResource(R.drawable.white_card);
        amountTextTwo.setTextColor(getResources().getColor(R.color.black));
        reachTextTwo.setTextColor(getResources().getColor(R.color.black));
        cardTwoImage.setImageResource(R.drawable.white_card);
        amountTextThree.setTextColor(getResources().getColor(R.color.black));
        reachTextThree.setTextColor(getResources().getColor(R.color.black));
        cardThreeImage.setImageResource(R.drawable.white_card);
        amountTextFour.setTextColor(getResources().getColor(R.color.black));
        reachTextFour.setTextColor(getResources().getColor(R.color.black));
        cardFourImage.setImageResource(R.drawable.white_card);

    }


    private void selectCardFourLayout() {
        budgetText.setText(getResources().getString(R.string.hundred_dollar) + "/ " + getResources().getString(R.string.two_days));
        amountValue = "100";

        est_profile_visits.setText(ConstantValues.REACH_FOUR);
        est_profile_reach.setText(ConstantValues.EST_FOUR);
        estProfileVisit.setText(ConstantValues.REACH_FOUR);
        estReach.setText(ConstantValues.EST_FOUR);
        budgetText.setText(getResources().getString(R.string.hundred_dollar));

        amountTextFour.setTextColor(getResources().getColor(R.color.white));
        reachTextFour.setTextColor(getResources().getColor(R.color.white));
        cardFourImage.setImageResource(R.drawable.violet_card);
    }

    private void selectCardThreeLayout() {
        amountValue = "75";

        budgetText.setText(getResources().getString(R.string.seventy_dollar) + "/ " + getResources().getString(R.string.two_days));

        est_profile_visits.setText(ConstantValues.REACH_THREE);
        est_profile_reach.setText(ConstantValues.EST_THREE);
        estProfileVisit.setText(ConstantValues.REACH_THREE);
        estReach.setText(ConstantValues.EST_THREE);

        amountTextThree.setTextColor(getResources().getColor(R.color.white));
        reachTextThree.setTextColor(getResources().getColor(R.color.white));
        cardThreeImage.setImageResource(R.drawable.violet_card);
    }

    private void selectCardTwoLayout() {
        budgetText.setText(getResources().getString(R.string.fifty_dollar) + " / " + getResources().getString(R.string.two_days));
        amountValue = "50";

        est_profile_visits.setText(ConstantValues.REACH_TWO);
        est_profile_reach.setText(ConstantValues.EST_TWO);
        estProfileVisit.setText(ConstantValues.REACH_TWO);
        estReach.setText(ConstantValues.EST_TWO);

        amountTextTwo.setTextColor(getResources().getColor(R.color.white));
        reachTextTwo.setTextColor(getResources().getColor(R.color.white));
        cardTwoImage.setImageResource(R.drawable.violet_card);
    }

    private void selectCardOneLayout() {
        amountValue = "20";
        budgetText.setText(getResources().getString(R.string.twenty_dollar) + " / " + getResources().getString(R.string.two_days));
        est_profile_visits.setText(ConstantValues.REACH_ONE);
        est_profile_reach.setText(ConstantValues.EST_ONE);
        estProfileVisit.setText(ConstantValues.REACH_ONE);
        estReach.setText(ConstantValues.EST_ONE);

        amountTextOne.setTextColor(getResources().getColor(R.color.white));
        reachTextOne.setTextColor(getResources().getColor(R.color.white));
        cardOneImage.setImageResource(R.drawable.violet_card);


    }

    private void moveMainActivity() {
        Intent intent = new Intent(PromoteActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("showImageUpload", "false");
        startActivity(intent);
    }


    private void placeReviewLayout() {
        setViewFour();
        AnimationHelper.slideLeft(reviewLayout, chooseAmountLayout, PromoteActivity.this);
        setReviewDatas();
    }

    private void setReviewDatas() {
        if (goal.equalsIgnoreCase("1")) {
            actionButtonLayout.setVisibility(View.GONE);

        } else {
            actionButtonLayout.setVisibility(View.VISIBLE);

        }

    }


    private void placeReviewLayoutBack() {
        setViewThree();
        AnimationHelper.slideRight(chooseAmountLayout, reviewLayout, PromoteActivity.this);
    }


    private void placeAudienceLayoutBack() {
        setViewTwo();
        AnimationHelper.slideRight(audienceLayout, chooseAmountLayout, PromoteActivity.this);
    }

    private void placeChooseAmountLayout() {
        setViewThree();
        makeAllCardDefault();
        selectCardOneLayout();
        AnimationHelper.slideLeft(chooseAmountLayout, audienceLayout, PromoteActivity.this);

    }

    private void placeGoalLayout() {
        setViewOne();
        AnimationHelper.slideRight(goalLayout, audienceLayout, PromoteActivity.this);
    }

    private void placeAudienceLayout() {
        setViewTwo();
        AnimationHelper.slideLeft(audienceLayout, goalLayout, PromoteActivity.this);

    }

    private void moveEditWebSite() {
        Intent intent = new Intent(PromoteActivity.this, EditWebsiteActivity.class);
        startActivityForResult(intent, 1);
    }


}
