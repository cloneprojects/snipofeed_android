package app.com.snipofeed.Activity;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Fragment.PostDetailsFragment;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

public class BookMarkActivity extends AppCompatActivity {
    private JSONArray taggedArray=new JSONArray();
    private GridViewAdapter gridViewAdapter;
    RecyclerView contentRecyclerView;
    private int currentTagpageNumber=0;
    private int totalTagpageNumber;
    AppSettings appSettings=new AppSettings(BookMarkActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//       Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_book_mark);
        initViews();
    }

    private void initViews() {
        contentRecyclerView=findViewById(R.id.contentRecyclerView);
//        try {
//            getTaggedPosts();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


//    private void setTaggedGridValues() {
//
//        GridLayoutManager linearLayoutManager = new GridLayoutManager(BookMarkActivity.this, 3, LinearLayoutManager.VERTICAL, false);
//        contentRecyclerView.setLayoutManager(linearLayoutManager);
//
//        gridViewAdapter = new GridViewAdapter(BookMarkActivity.this, taggedArray, refresh_layout);
//        contentRecyclerView.setAdapter(gridViewAdapter);
//        gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
//            @Override
//            public void onBottomReached(int position) {
//
//                try {
//                    getNextTaggedData();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//
//        gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
//            @Override
//            public void onClicked(String username) {
//
//            }
//
//            @Override
//            public void onClicked(String username, JSONObject jsonObject) {
//                Bundle bundle=new Bundle();
//                bundle.putString("singlePost","false");
//                bundle.putString("postDetails",jsonObject.toString());
//
//                PostDetailsFragment newFragment = new PostDetailsFragment();
//                newFragment.setArguments(bundle);
//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.parentLayoutBookMark, newFragment);
//                transaction.addToBackStack(null);
//                transaction.commit();
//
//            }
//        });
//
//
//
//        contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
//            @Override
//            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//                int position = parent.getChildAdapterPosition(view); // item position
//                int spanCount = 3;
//                int spacing = 5;//spacing between views in grid
//
//                if (position >= 0) {
//                    int column = position % spanCount; // item column
//
//                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
//                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
//
//                    if (position < spanCount) { // top edge
//                        outRect.top = spacing;
//                    }
//                    outRect.bottom = spacing; // item bottom
//                } else {
//                    outRect.left = 0;
//                    outRect.right = 0;
//                    outRect.top = 0;
//                    outRect.bottom = 0;
//                }
//            }
//        });
//
//    }


//    private void getTaggedPosts() throws JSONException {
//        final JSONObject jsonObject = new JSONObject();
//        jsonObject.put("accessToken", appSettings.getAccessToken());
//        jsonObject.put("page", "1");
//        ApiCall.PostMethod(BookMarkActivity.this, UrlHelper.BOOKMARKED_POSTS, jsonObject, new VolleyCallback() {
//            @SuppressLint({"CheckResult", "SetTextI18n"})
//            @Override
//            public void onSuccess(JSONObject response) {
//                taggedArray = response.optJSONArray("pinnedPosts");
//                    setTaggedGridValues();
//            }
//        });
//
//    }



    private void getNextTaggedData() throws JSONException {

        currentTagpageNumber = currentTagpageNumber + 1;
        if (currentTagpageNumber <= totalTagpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentTagpageNumber);

            ApiCall.PostMethodNoProgress(BookMarkActivity.this, UrlHelper.BOOKMARKED_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(BookMarkActivity.this, response.optString("message"));
                    } else {
                        try {
                            totalTagpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("pinnedPosts");
                        duy = validateJsonArray(duy);
                        for (int i = 0; i < duy.length(); i++) {
                            taggedArray.put(duy.optJSONObject(i));
                        }

                        gridViewAdapter.addValues(duy);


                    }

                }
            });
        }
    }



    private JSONArray validateJsonArray(JSONArray myPosts) {
        JSONArray newArray = new JSONArray();
        for (int i = 0; i < myPosts.length(); i++) {
            if (myPosts.optJSONObject(i).optJSONArray("linkData").length() != 0) {
                newArray.put(myPosts.optJSONObject(i));
            }
        }
        return newArray;
    }




}
