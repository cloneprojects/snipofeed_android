package app.com.snipofeed.Activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangePassswordActivity extends AppCompatActivity {
    EditText oldPassword, newPassword, confirmPassword;

    Button confirmButton;
    AppSettings appSettings = new AppSettings(ChangePassswordActivity.this);



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//       Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_change_passsword);
        initViews();
        initListeners();
    }

    private void initListeners() {

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateInputs().equalsIgnoreCase("true")) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("accessToken", appSettings.getAccessToken());
                        jsonObject.put("password", oldPassword.getText().toString());
                        jsonObject.put("newPassword", confirmPassword.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ApiCall.PostMethod(ChangePassswordActivity.this, UrlHelper.CHANGE_PASSWORD, jsonObject, new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONObject response) {
                            Utils.toast(ChangePassswordActivity.this, getResources().getString(R.string.password_changed_successfully));
                            finish();
                        }
                    });

                } else {
                    Utils.toast(ChangePassswordActivity.this,validateInputs());
                }
            }
        });

    }

    private String validateInputs() {
        String value="true";


        if (oldPassword.getText().toString().trim().length()==0||newPassword.getText().toString().trim().length()==0||oldPassword.getText().toString().trim().length()==0)
        {
            return getResources().getString(R.string.password_should_be_minimum);
        } else if (!newPassword.getText().toString().trim().equalsIgnoreCase(confirmPassword.getText().toString().trim()))
        {
            return getResources().getString(R.string.password_not_match);

        } else {

            return value;
        }

    }

    private void initViews() {
        oldPassword = findViewById(R.id.oldPassword);
        newPassword = findViewById(R.id.newPassword);
        confirmPassword = findViewById(R.id.confirmPassword);
        confirmButton = findViewById(R.id.confirmButton);

    }
}
