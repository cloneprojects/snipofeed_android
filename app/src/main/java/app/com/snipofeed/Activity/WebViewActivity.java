package app.com.snipofeed.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WebViewActivity extends AppCompatActivity {
    TextView titleBarText;
    WebView webView;
    String url = "https://tinderboxsolutions.net/privacy-policy.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_web_view);
        titleBarText = findViewById(R.id.titleBarText);
        webView = findViewById(R.id.webView);
        titleBarText.setText(getIntent().getStringExtra("name"));
//        url=getIntent().getStringExtra("url");
        webView.loadUrl(url);

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
