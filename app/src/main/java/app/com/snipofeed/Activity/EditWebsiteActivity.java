package app.com.snipofeed.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import app.com.snipofeed.Helper.ConstantValues;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditWebsiteActivity extends AppCompatActivity implements View.OnClickListener {
    EditText webSiteName;
    String selectedText;
    private ImageView backButton;
    private ImageView nextButton;
    private ImageView learnMoreRound;
    private ImageView shopNowRound;
    private ImageView watchMoreRound;
    private ImageView contactUsRound;
    private ImageView bookNowRound;
    private ImageView signUpRound;
    private RelativeLayout learnMore;
    private RelativeLayout shopNow;
    private RelativeLayout watchMore;
    private RelativeLayout contactUs;
    private RelativeLayout bookNow;
    private RelativeLayout signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);


        setContentView(R.layout.activity_edit_website);


        initViews();
        initListners();
        select(1);
//        clearAll();
    }

    private void initListners() {
        learnMore.setOnClickListener(this);
        shopNow.setOnClickListener(this);
        watchMore.setOnClickListener(this);
        contactUs.setOnClickListener(this);
        bookNow.setOnClickListener(this);
        signUp.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void clearAll() {
        signUpRound.setImageResource(R.drawable.circle_round_grey);
        bookNowRound.setImageResource(R.drawable.circle_round_grey);
        contactUsRound.setImageResource(R.drawable.circle_round_grey);
        watchMoreRound.setImageResource(R.drawable.circle_round_grey);
        shopNowRound.setImageResource(R.drawable.circle_round_grey);
        learnMoreRound.setImageResource(R.drawable.circle_round_grey);
    }

    public void select(int position) {
        if (position == 1) {
            learnMoreRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.LEARN_MORE;
        } else {
            learnMoreRound.setImageResource(R.drawable.circle_round_grey);
        }
        if (position == 2) {
            shopNowRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.SHOP_NOW;

        } else {
            shopNowRound.setImageResource(R.drawable.circle_round_grey);
        }
        if (position == 3) {
            watchMoreRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.WATCH_MORE;

        } else {
            watchMoreRound.setImageResource(R.drawable.circle_round_grey);
        }
        if (position == 4) {
            contactUsRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.CONTACT_US;

        } else {
            contactUsRound.setImageResource(R.drawable.circle_round_grey);
        }
        if (position == 5) {
            bookNowRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.BOOK_NOW;

        } else {
            bookNowRound.setImageResource(R.drawable.circle_round_grey);
        }
        if (position == 6) {
            signUpRound.setImageResource(R.drawable.circle_round_blue);
            selectedText = ConstantValues.SIGN_UP;

        } else {
            signUpRound.setImageResource(R.drawable.circle_round_grey);
        }

    }

    private void initViews() {

        signUp = (RelativeLayout) findViewById(R.id.signUp);
        signUpRound = (ImageView) findViewById(R.id.signUpRound);
        bookNow = (RelativeLayout) findViewById(R.id.bookNow);
        bookNowRound = (ImageView) findViewById(R.id.bookNowRound);
        contactUs = (RelativeLayout) findViewById(R.id.contactUs);
        contactUsRound = (ImageView) findViewById(R.id.contactUsRound);
        watchMore = (RelativeLayout) findViewById(R.id.watchMore);
        watchMoreRound = (ImageView) findViewById(R.id.watchMoreRound);
        shopNow = (RelativeLayout) findViewById(R.id.shopNow);
        shopNowRound = (ImageView) findViewById(R.id.shopNowRound);
        learnMore = (RelativeLayout) findViewById(R.id.learnMore);
        learnMoreRound = (ImageView) findViewById(R.id.learnMoreRound);
        nextButton = (ImageView) findViewById(R.id.nextButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        webSiteName = (EditText) findViewById(R.id.webSiteName);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onClick(View v) {
        if (v == learnMore) {
            select(1);

        } else if (v == shopNow) {
            select(2);


        } else if (v == watchMore) {
            select(3);

        } else if (v == contactUs) {
            select(4);

        } else if (v == bookNow) {
            select(5);

        } else if (v == signUp) {
            select(6);

        } else if (v == nextButton) {
            if (Utils.isValidUrl(webSiteName.getText().toString())) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("website", webSiteName.getText().toString());
                returnIntent.putExtra("action", selectedText);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                Utils.toast(EditWebsiteActivity.this,getResources().getString(R.string.please_enter_valid_website_to_promote));
            }
        } else if (v == backButton) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();

        }
    }
}
