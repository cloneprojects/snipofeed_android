package app.com.snipofeed.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BusinessProfileSignUp extends AppCompatActivity implements View.OnClickListener {
    TextView textInsta;
    CircleImageView profileImage;
    Button continueButton;
    RelativeLayout welcomeLayout;
    LinearLayout contactLayout;
    Button doneButton;
    EditText mobileNumber, emailId;
    ImageView closeButton, backButton;
    AppSettings appSettings = new AppSettings(BusinessProfileSignUp.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);

        setContentView(R.layout.activity_business_profile_sign_up);
        initViews();
        initListeners();
        setValues();
    }

    private void setValues() {
        Glide.with(this).load(appSettings.getUserImage()).apply(Utils.getRequestOptions()).into(profileImage);
        String one = getResources().getString(R.string.welcome_to_insta_business) + " \n ";
        textInsta.setText(one + appSettings.getUserName());
    }

    private void initListeners() {
        continueButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        doneButton.setOnClickListener(this);
    }

    private void initViews() {

        textInsta = findViewById(R.id.textInsta);
        profileImage = findViewById(R.id.profileImage);
        continueButton = findViewById(R.id.continueButton);
        contactLayout = findViewById(R.id.contactLayout);
        welcomeLayout = findViewById(R.id.welcomeLayout);
        backButton = findViewById(R.id.backButton);
        closeButton = findViewById(R.id.closeButton);
        emailId = findViewById(R.id.emailId);
        mobileNumber = findViewById(R.id.mobileNumber);
        doneButton = findViewById(R.id.doneButton);
    }

    @Override
    public void onClick(View v) {
        if (v == continueButton) {
            placeContactLayout();
        } else if (v == closeButton) {
            finish();
        } else if (v == backButton) {
            placeWelcomeLayout();
        } else if (v == doneButton) {
            proceedForSignUp();
        }
    }

    private void proceedForSignUp() {
        if (emailId.getText().toString().trim().length() != 0 && Utils.isValidEmail(emailId.getText().toString())) {
            if (mobileNumber.getText().toString().trim().length() != 0) {
                try {
                    hitAPi();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Utils.toast(BusinessProfileSignUp.this, getResources().getString(R.string.please_enter_valid_mobile));

            }
        } else {
            Utils.toast(BusinessProfileSignUp.this, getResources().getString(R.string.please_enter_valid_email));
        }

    }

    private void hitAPi() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("status", "enabled");
        jsonObject.put("businessContactNumber", mobileNumber.getText().toString());
        jsonObject.put("businessContactAddress", emailId.getText().toString());
        ApiCall.PostMethod(BusinessProfileSignUp.this, UrlHelper.SWITCH_TO_BUSINESS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(BusinessProfileSignUp.this, getResources().getString(R.string.now_you_bocome_business));
                moveMainActivity();

            }
        });
    }


    private void moveMainActivity() {
        Intent intent = new Intent(BusinessProfileSignUp.this, MainActivity.class);
        intent.putExtra("showImageUpload", "false");
        intent.putExtra("moveProfile", "true");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    private void placeWelcomeLayout() {
        slideRight(welcomeLayout, contactLayout);
    }

    private void placeContactLayout() {
        emailId.setText(appSettings.getUserEmail());
        mobileNumber.setText(appSettings.getUserMobile());
        slideLeft(contactLayout, welcomeLayout);
    }


    public void slideLeft(final View in, final View out) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_left);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public void slideRight(final View in, final View out) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_left);
        Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.slide_to_right);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
