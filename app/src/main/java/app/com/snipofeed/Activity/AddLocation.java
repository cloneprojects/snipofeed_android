package app.com.snipofeed.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import app.com.snipofeed.Adapters.AutoCompleteAdapter;
import app.com.snipofeed.Autocomplete.PlaceAutoComplete;
import app.com.snipofeed.Autocomplete.PlacePredictions;
import app.com.snipofeed.Helper.AppController;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddLocation extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,LocationListener {

    EditText searchBox;
    ListView mAutoCompleteList;
    AutoCompleteAdapter mAutoCompleteAdapter;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    String selectedLat, selectedLong;
    private PlacePredictions predictions = new PlacePredictions();
    private String TAG=AddLocation.class.getSimpleName();
    private double currentLatitude;
    private double currentLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_add_location);

        initViews();
        initListeners();

        initlocation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            settingsrequest();
            initlocationRequest();
        }

    }

    private void initListeners() {

        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final PlaceAutoComplete Place = predictions.getPlaces().get(position);
                // pass the result to the calling activity
                if (mGoogleApiClient != null) {

                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, predictions.getPlaces().get(position).getPlaceID())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    Log.d("onResult: ", "status:" + places.getStatus());
                                    if (places.getStatus().isSuccess()) {
                                        searchBox.setText("");
                                        mAutoCompleteList.invalidate();
                                        mAutoCompleteAdapter.clear();
                                        Place myPlace = places.get(0);
                                        Intent returnIntent = new Intent();
                                        returnIntent.putExtra("latitude",""+myPlace.getLatLng().latitude);
                                        returnIntent.putExtra("longitude",""+myPlace.getLatLng().longitude);
                                        Log.d(TAG, "onResult: "+myPlace.getLatLng().latitude);
                                        Log.d(TAG, "onResult: "+myPlace.getLatLng().longitude);
                                        returnIntent.putExtra("place_details",""+Place.getPlaceDesc());
                                        setResult(Activity.RESULT_OK,returnIntent);
                                        finish();

                                    }

                                    places.release();
                                }
                            });
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 106) {
            if (grantResults.length > 0) {

                initlocationRequest();
            }
        }

        if (requestCode == 1) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initlocationRequest();
                settingsrequest();

            }

        }
    }


    private void initViews() {

        searchBox = (EditText) findViewById(R.id.searchBox);
        mAutoCompleteList = (ListView) findViewById(R.id.mAutoCompleteList);

        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchBox.getText().toString().length() > 3) {

                    searchPlaces();
                } else {
                    try {
                        if (mAutoCompleteAdapter.getCount() > 0) {
                            mAutoCompleteList.invalidate();
                            mAutoCompleteAdapter.clear();
                        }
                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&location=");
        urlString.append(selectedLat + "," + selectedLong); // append lat long of current location to show nearby results.
        urlString.append("&radius=500&language=en");
        urlString.append("&key=" + getResources().getString(R.string.map_key));

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }



    private void searchPlaces() {
        JSONObject object = new JSONObject();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPlaceAutoCompleteUrl(searchBox.getText().toString()), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("SearchResponse", response.toString());
                if (response.optString("status").equalsIgnoreCase("ok")) {
                    Gson gson = new Gson();
                    predictions = gson.fromJson(response.toString(), PlacePredictions.class);
                    if (mAutoCompleteAdapter == null) {
                        mAutoCompleteList.setVisibility(View.VISIBLE);
                        mAutoCompleteAdapter = new AutoCompleteAdapter(AddLocation.this, predictions.getPlaces(), AddLocation.this);
                        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                    } else {
                        mAutoCompleteList.setVisibility(View.VISIBLE);
                        mAutoCompleteAdapter.clear();
                        mAutoCompleteAdapter.addAll(predictions.getPlaces());
                        mAutoCompleteAdapter.notifyDataSetChanged();
                        mAutoCompleteList.invalidate();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("SearchResponse", error.toString());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }



    private void initlocation() {
        checkCallingOrSelfPermission("");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();


    }


    private void initlocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000);

    }

    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(AddLocation.this, 5);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (Exception e) {

            }
        } else {

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            Utils.log(TAG, "mylat:" + currentLatitude + ",mylong:" + currentLongitude);
            if (currentLatitude != 0.0 && currentLongitude != 0.0) {

//                    setlocation();

            }

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationChanged(Location location) {

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

    }


}
