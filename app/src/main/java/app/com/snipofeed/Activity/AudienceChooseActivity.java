package app.com.snipofeed.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;

import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AudienceChooseActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView backButton;
    ImageView nextButton;
    int selectedValue = 1;
    int minValue= 0;
    int maxValue = 100;
    ImageView staticMapLayout;
    RelativeLayout locationLay;
    TextView maleText, locationText, ageMinText, ageMaxText;
    ImageView maleRound;
    RelativeLayout maleLayout;
    TextView femaleText, radiusText;
    ImageView femaleRound;
    RelativeLayout femaleLayout;
    LinearLayout goalLayout;
    CrystalSeekbar radiusSeekbar;
    CrystalRangeSeekbar ageSeekbar;
    private String TAG = AudienceChooseActivity.class.getSimpleName();
    private String latitude = "0", longitude = "0";
    private boolean maleSelected = true;
    private boolean femaleSelected = false;
    private boolean bothSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_audience_choose);
        initViews();
        initListeners();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initViews() {

        goalLayout = (LinearLayout) findViewById(R.id.goalLayout);
        femaleLayout = (RelativeLayout) findViewById(R.id.femaleLayout);
        femaleRound = (ImageView) findViewById(R.id.femaleRound);
        femaleText = (TextView) findViewById(R.id.femaleText);
        radiusText = (TextView) findViewById(R.id.radiusText);
        maleLayout = (RelativeLayout) findViewById(R.id.maleLayout);
        maleRound = (ImageView) findViewById(R.id.maleRound);
        maleText = (TextView) findViewById(R.id.maleText);
        ageSeekbar = findViewById(R.id.ageSeekbar);
        radiusSeekbar = (CrystalSeekbar) findViewById(R.id.radiusSeekbar);
        staticMapLayout = (ImageView) findViewById(R.id.staticMapLayout);
        nextButton = (ImageView) findViewById(R.id.nextButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        locationLay = findViewById(R.id.locationLay);
        radiusSeekbar = findViewById(R.id.radiusSeekbar);
        locationText = findViewById(R.id.locationText);
        ageMinText = findViewById(R.id.ageMinText);
        ageMaxText = findViewById(R.id.ageMaxText);
        ageSeekbar = findViewById(R.id.ageSeekbar);


        radiusSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {

                selectedValue = value.intValue();
                loadStaticMap(selectedValue);

                Log.e(TAG, "valueChanged:" + value);

                radiusText.setText(String.valueOf(value + "mi"));

            }
        });

        ageSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                AudienceChooseActivity.this.minValue=minValue.intValue();
                AudienceChooseActivity.this.maxValue=maxValue.intValue();
                ageMaxText.setText(String.valueOf(maxValue));
                ageMinText.setText(String.valueOf(minValue));
            }
        });
        loadStaticMap(1);

    }

    @SuppressLint("CheckResult")
    public void loadStaticMap(int i) {
        String url = Utils.getStaticMapURL(Double.parseDouble(latitude), Double.parseDouble(longitude), i * 1000, AudienceChooseActivity.this, staticMapLayout.getWidth(), staticMapLayout.getHeight());

        Log.d(TAG, "loadStaticMap: " + url);
        RequestOptions options = new RequestOptions();
        options.fitCenter();
        options.placeholder(getResources().getDrawable(R.drawable.map_placeholder));
        options.error(getResources().getDrawable(R.drawable.map_placeholder));
        Glide.with(AudienceChooseActivity.this).load(url).apply(options).into(staticMapLayout);
    }

    private void initListeners() {
        femaleLayout.setOnClickListener(this);
        maleLayout.setOnClickListener(this);
        locationLay.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void setFemaleValidation() {

        // Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_blue).into(femaleRound);

        if (!femaleSelected) {
            Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_blue).into(femaleRound);
            femaleSelected = true;
        } else {
            Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_grey).into(femaleRound);
            femaleSelected = false;
        }


        // Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_grey).into(maleRound);

    }

    private void setMaleValidation() {

        if (!maleSelected) {
            Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_blue).into(maleRound);
            maleSelected = true;
        } else {
            Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_grey).into(maleRound);
            maleSelected = false;
        }
        //Glide.with(AudienceChooseActivity.this).load(R.drawable.circle_round_grey).into(femaleRound);

    }

    @Override
    public void onClick(View v) {
        if (v == maleLayout) {
            setMaleValidation();
        } else if (v == femaleLayout) {
            setFemaleValidation();
        } else if (v == locationLay) {
            moveListLocation();
        } else if (v == nextButton) {

            if (locationText.getText().toString().equalsIgnoreCase("")) {
                Utils.toast(AudienceChooseActivity.this, getResources().getString(R.string.please_select_location));
            } else if (latitude.equalsIgnoreCase("0.0")||longitude.equalsIgnoreCase("0.0"))
            {
                Utils.toast(AudienceChooseActivity.this, getResources().getString(R.string.please_select_location));

            }else {
                if (!latitude.equalsIgnoreCase("0")&&!longitude.equalsIgnoreCase("0")) {
                    Intent passData = new Intent();
                    passData.putExtra("latitude", "" + latitude);
                    passData.putExtra("longitude", "" + longitude);
                    passData.putExtra("radius", "" + selectedValue);
                    passData.putExtra("min", "" + minValue);
                    passData.putExtra("max", "" + maxValue);

                    if (maleSelected && femaleSelected) {
                        passData.putExtra("gender", "" + "2"); // both
                    } else if (maleSelected && !femaleSelected) {
                        passData.putExtra("gender", "" + "0");   //0 male
                    } else if (!maleSelected && femaleSelected) {
                        passData.putExtra("gender", "" + "1"); // 1 female
                    }

                    setResult(Activity.RESULT_OK, passData);
                    finish();
                } else {
                    Utils.toast(AudienceChooseActivity.this,getResources().getString(R.string.please_select_latitude_and_longitude));
                }
            }

        }else if(v==backButton){
            Intent emptyIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, emptyIntent);
            finish();

        }
    }

    private void moveListLocation() {
        Intent intent = new Intent(AudienceChooseActivity.this, ListLocation.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
                String textSetting = (latitude + ", " + longitude);

                locationText.setText(textSetting);
                loadStaticMap(selectedValue);
//                Log.d(TAG, "onActivityResult: "+radiusSeekbar.getMaxValue()+"/"+ageSeekbar.getMinValue());

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

    }
}