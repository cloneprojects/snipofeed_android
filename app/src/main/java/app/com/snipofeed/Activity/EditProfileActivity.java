package app.com.snipofeed.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.Random;

import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {


    AppSettings appSettings = new AppSettings(EditProfileActivity.this);
    CircleImageView userImage;
    TextView changePhoto;
    EditText name, userName, webSite, bioDescription, email, mobileNumber, gender;
    TextView saveButton;
    private PopupWindow pw;
    private String filePath;
    private File displayPicture;
    private Uri uri;
    private String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);

        setContentView(R.layout.activity_edit_profile);
        initViews();
        initListeners();
    }

    private void initListeners() {
        gender.setOnTouchListener(this);
        changePhoto.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initViews() {
        gender = findViewById(R.id.gender);
        userImage = findViewById(R.id.userImage);
        changePhoto = findViewById(R.id.changePhoto);
        name = findViewById(R.id.name);
        userName = findViewById(R.id.userName);
        webSite = findViewById(R.id.webSite);
        bioDescription = findViewById(R.id.bioDescription);
        email = findViewById(R.id.email);
        mobileNumber = findViewById(R.id.mobileNumber);
        saveButton = findViewById(R.id.saveButton);


        setValues();
    }

    public void uploadDataToserver(String userImage) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("name", name.getText().toString());
        jsonObject.put("website", webSite.getText().toString());
        jsonObject.put("bio", bioDescription.getText().toString());
        jsonObject.put("mobileNumber", mobileNumber.getText().toString());
        jsonObject.put("gender", gender.getText().toString());
        if (displayPicture != null) {
            jsonObject.put("profilePic", userImage);

        }
        ApiCall.PostMethod(EditProfileActivity.this, UrlHelper.EDIT_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONObject myProfileObject = response.optJSONArray("userDetails").optJSONObject(0);
Utils.toast(EditProfileActivity.this,getResources().getString(R.string.updated_successfully));
                appSettings.setUserImage(myProfileObject.optString("profilePic"));
                appSettings.setWebsite(myProfileObject.optString("website"));
                appSettings.setBioDescription(myProfileObject.optString("bio"));
                appSettings.setFullName(myProfileObject.optString("name"));
                appSettings.setUserEmail(myProfileObject.optString("email"));
                appSettings.setUserGender(myProfileObject.optString("gender"));
                appSettings.setUserMobile(myProfileObject.optString("mobileNumber"));
                finish();


            }
        });

    }


    private void setValues() {
        gender.setText(appSettings.getUserGender());

        Glide.with(EditProfileActivity.this).load(appSettings.getUserImage()).apply(Utils.getRequestOptions()).into(userImage);
        name.setText(appSettings.getFullName());
        userName.setText(appSettings.getUserName());
        webSite.setText(appSettings.getWebsite());
        bioDescription.setText(appSettings.getBioDescription());
        email.setText(appSettings.getUserEmail());
        mobileNumber.setText(appSettings.getUserMobile());
    }


    private void initiatePopupWindow(View v) {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) EditProfileActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout
            View layout = inflater.inflate(R.layout.gender_options_layout,
                    null);
            RelativeLayout parentLayout;
            // create a 300px width and 470px height PopupWindow
            pw = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            // display the popup in the center
            pw.setBackgroundDrawable(new BitmapDrawable());
            pw.setOutsideTouchable(true);
            pw.setFocusable(true);
            pw.setTouchInterceptor(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // here I want to close the pw when clicking outside it but
                    // at all this is just an example of how it works and you can
                    // implement the onTouch() or the onKey() you want
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        pw.dismiss();
                        return true;
                    }
                    return false;
                }


            });

            final TextView maleView, femaleView;
            maleView = layout.findViewById(R.id.maleView);
            femaleView = layout.findViewById(R.id.femaleView);
            parentLayout = layout.findViewById(R.id.parentLayout);
            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pw.dismiss();
                }
            });
            maleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gender.setText(getResources().getString(R.string.male));
                    pw.dismiss();


                }
            });

            femaleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    gender.setText(getResources().getString(R.string.female));
                    pw.dismiss();


                }
            });
            pw.showAtLocation(v, Gravity.BOTTOM, 0, 0);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == changePhoto) {
            showPictureDialog();
        } else if (v == saveButton) {
            try {
                processChangeUserNameInputs();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void processChangeUserNameInputs() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", userName.getText().toString());
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(EditProfileActivity.this, UrlHelper.CHANGE_USER_NAME, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                appSettings.setUserName(response.optString("username"));
                if (displayPicture != null) {
                    Utils.uploadFile(EditProfileActivity.this, displayPicture, new Utils.UploadCallBack() {
                        @Override
                        public void result(boolean status, String message) {


                            try {
                                uploadDataToserver(message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    try {
                        uploadDataToserver("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }



            }
        });
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == gender) {
            if (event.getAction() == MotionEvent.ACTION_UP)
                initiatePopupWindow(v);

        }
        return true;
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(EditProfileActivity.this, getPackageName() + ".provider", file);
            appSettings.setImagePath(uri.getPath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        } else {
            uri = Uri.fromFile(file);
            path = file.getPath();
            appSettings.setImagePath(path);
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, 104);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity_Res", "" + requestCode);
        switch (requestCode) {
            case 1:
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        beginCrop(uri);
                    } else {
                        Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_image));
                    }
                }
                break;
            case 104:
                if (uri != null) {
                    beginCrop(uri);
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_image));

                }
                break;
            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    private void beginCrop(Uri source) {
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), Calendar.getInstance().getTimeInMillis() + ".jpg"));
        Crop.of(source, outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == RESULT_OK) {
            filePath = Utils.getRealPathFromUriNew(this, Crop.getOutput(result));
            displayPicture = new File(filePath);
            Glide.with(this).load(Crop.getOutput(result)).into(userImage);


        } else if (resultCode == Crop.RESULT_ERROR) {
            Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_image));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
