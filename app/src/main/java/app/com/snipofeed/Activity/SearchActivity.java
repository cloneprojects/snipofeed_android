package app.com.snipofeed.Activity;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.Fragment.PeopleFragment;
import app.com.snipofeed.Fragment.PlacesFragment;
import app.com.snipofeed.Fragment.TagsFragment;
import app.com.snipofeed.Fragment.TopFragment;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends AppCompatActivity {

    private ViewPager viewPager;
    public static EditText searchText;
    int selectedPosition= ConstantKeys.isTop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Utils.setStatusBarColor(this);

        setContentView(R.layout.activity_search);

        viewPager = findViewById(R.id.viewpager);
        searchText = findViewById(R.id.searchText);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(new TopFragment(ConstantKeys.isTop), getResources().getString(R.string.top));
        adapter.addFragment(new PeopleFragment(), getResources().getString(R.string.people));
//        adapter.addFragment(new TagsFragment(ConstantKeys.isHashtags), getResources().getString(R.string.tags));
//        adapter.addFragment(new PlacesFragment(ConstantKeys.isPlaces), getResources().getString(R.string.places));
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position==0)
                {
                    selectedPosition=ConstantKeys.isTop;
                } else if (position==1)
                {
                    selectedPosition=ConstantKeys.isPeople;
                } else if (position==2)
                {
                    selectedPosition=ConstantKeys.isHashtags;
                } else {
                    selectedPosition=ConstantKeys.isPlaces;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (selectedPosition==ConstantKeys.isPeople)
//                {
//                    PeopleFragment.initPeople(SearchActivity.this, getSupportFragmentManager());
//                } else if (selectedPosition==ConstantKeys.isHashtags)
//                {
//                    TagsFragment.initTags(SearchActivity.this);
//
//                } else if (selectedPosition==ConstantKeys.isPlaces)
//                {
//                    PlacesFragment.initPlaces(SearchActivity.this);
//
//                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
