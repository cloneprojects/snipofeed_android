package app.com.snipofeed.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Helper.ConstantValues;
import app.com.snipofeed.Helper.SwitchButton;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    TextView changePassword, privacyPolicy, aboutUs, termsAndConditions, logOut, businessText;
    ImageView backButton;
    SwitchButton privateAccountSwitch;
    LinearLayout privateAccount;
    AppSettings appSettings = new AppSettings(SettingsActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//          Utils.setStatusBarColor(this);
        setContentView(R.layout.activity_settings);
        initViews();
        initListeners();
    }

    private void initListeners() {
        changePassword.setOnClickListener(this);
        privacyPolicy.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        termsAndConditions.setOnClickListener(this);
        logOut.setOnClickListener(this);
        backButton.setOnClickListener(this);
        privateAccount.setOnClickListener(this);
        businessText.setOnClickListener(this);

        privateAccountSwitch.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (privateAccountSwitch.isChecked()) {
                    appSettings.setIsPrivateAccout("true");
                } else {
                    appSettings.setIsPrivateAccout("false");

                }
                try {
                    updatePrivacyStatus();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void initViews() {
        changePassword = findViewById(R.id.changePassword);
        businessText = findViewById(R.id.businessText);
        privacyPolicy = findViewById(R.id.privacyPolicy);
        aboutUs = findViewById(R.id.aboutUs);
        termsAndConditions = findViewById(R.id.termsAndCondition);
        backButton = findViewById(R.id.backButton);
        logOut = findViewById(R.id.logOut);
        privateAccountSwitch = findViewById(R.id.privateAccountSwitch);
        privateAccount = findViewById(R.id.privateAccount);

        if (appSettings.getIsPrivateAccout().equalsIgnoreCase("true")) {
            privateAccountSwitch.setChecked(true);
        } else {
            privateAccountSwitch.setChecked(false);
        }

        if (appSettings.getUserType().equalsIgnoreCase(ConstantValues.BUSINESS_USER)) {
            businessText.setText(getResources().getString(R.string.switch_back_to_normal));
        } else {
            businessText.setText(getResources().getString(R.string.switch_to_business_account));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == changePassword) {
            moveChangePassword();
        } else if (v == privacyPolicy) {
            movePrivacyPolicy();
        } else if (v == aboutUs) {
            moveAboutUs();
        } else if (v == termsAndConditions) {
            moveTermsAndCOndition();
        } else if (v == logOut) {
            logoutFromApp();
        } else if (v == backButton) {
            finish();
        } else if (v == privateAccount) {
            privateAccountSwitch.toggle(true);
        } else if (v == businessText) {
            if (appSettings.getUserType().equalsIgnoreCase(ConstantValues.BUSINESS_USER)) {
                try {
                    backToNormalUser();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                switchToBusinessAccount();
            }
        }

    }

    private void switchToBusinessAccount() {
        Intent intent = new Intent(SettingsActivity.this, BusinessProfileSignUp.class);
        startActivity(intent);
    }


    public void deleteConfirmation(final String postId, final int position) {
        final Dialog dialog = new Dialog(SettingsActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.confirm_switch_back);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        dialog.show();
        final TextView dontDelete, delete;
        dontDelete = dialog.findViewById(R.id.dontDelete);
        delete = dialog.findViewById(R.id.delete);
        dontDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    backToNormalUser();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void backToNormalUser() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("status", "disabled");
        ApiCall.PostMethod(SettingsActivity.this, UrlHelper.SWITCH_TO_BUSINESS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(SettingsActivity.this, getResources().getString(R.string.now_you_bocome_business));
                moveMainActivity();

            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
        intent.putExtra("showImageUpload", "false");
        intent.putExtra("moveProfile", "true");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void updatePrivacyStatus() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("accessToken", appSettings.getAccessToken());
        if (appSettings.getIsPrivateAccout().equalsIgnoreCase("true")) {
            jsonObject.put("status", "disabled");
        } else {
            jsonObject.put("status", "enabled");
        }
        ApiCall.PostMethod(SettingsActivity.this, UrlHelper.UPDATE_PRIVACY, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(SettingsActivity.this, getResources().getString(R.string.updated));
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void moveAboutUs() {
        Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
        intent.putExtra("name", getResources().getString(R.string.about_us));
        intent.putExtra("url", "");
        startActivity(intent);
    }

    private void movePrivacyPolicy() {
        Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
        intent.putExtra("name", getResources().getString(R.string.privacy_policy));
        intent.putExtra("url", "");
        startActivity(intent);

    }

    private void moveChangePassword() {
        Intent intent = new Intent(SettingsActivity.this, ChangePassswordActivity.class);
        startActivity(intent);


    }

    private void logoutFromApp() {

        Intent intent = new Intent(SettingsActivity.this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void moveTermsAndCOndition() {

        Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
        intent.putExtra("name", getResources().getString(R.string.terms_and_condition));
        intent.putExtra("url", "");
        startActivity(intent);

    }
}
