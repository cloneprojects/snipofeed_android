package app.com.snipofeed.Volley;

/**
 * Created by yuvaraj on 28/03/18.
 */

public interface LocationCallBack {
    public void onLocationSuccess(String city, String state, String country);

}
