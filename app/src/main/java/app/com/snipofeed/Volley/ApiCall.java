package app.com.snipofeed.Volley;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.com.snipofeed.Helper.AppController;
import app.com.snipofeed.Helper.Utils;


/**
 * Created by user on 23-10-2017.
 */

public class ApiCall {


    public static String strAdd;
    private static String TAG = ApiCall.class.getSimpleName();

    public static void getMethod(final Activity context, final String url, final VolleyCallback volleyCallback) {

//        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
//                        Utils.dismiss();
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Utils.dismiss();

                VolleyErrorHandler.handle(url, error);


            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public static void PostMethod(final Activity context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        if (isNetworkConnected(context)) {
            Utils.show(context);
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);

                            Utils.dismiss(context);

                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.onSuccess(response);
                                } else {
                                    if (response.has("error_message")) {
                                        Utils.toast(context, response.optString("error_message"));
                                    } else {
                                        Utils.toast(context, response.optString("message"));

                                    }

                                }
                            } else {
                                volleyCallback.onSuccess(response);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.dismiss(context);
                    Utils.log(TAG, "url:" + url + ",error: " + error);

                    VolleyErrorHandler.handle(url, error);
                }});
//            }){
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("Content-Type", "application/x-www-form-urlencoded");
//                    return headers;
//                }
//            };

            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } else {
            Utils.showNoInternet(context);
        }

    }

    public static void PostMethodNoProgress(final Activity context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        if (isNetworkConnected(context)) {
//            Utils.show(context);
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);

//                            Utils.dismiss(context);

                            if (response.has("error")) {
                                if (response.optString("error").equalsIgnoreCase("false")) {
                                    volleyCallback.onSuccess(response);
                                } else {
                                    if (response.has("error_message")) {
                                        Utils.toast(context, response.optString("error_message"));
                                    } else {
                                        Utils.toast(context, response.optString("message"));

                                    }

                                }
                            } else {
                                volleyCallback.onSuccess(response);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Utils.dismiss(context);
                    Utils.log(TAG, "url:" + url + ",error: " + error);

                    VolleyErrorHandler.handle(url, error);
                }});
//            }){
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//
//                    HashMap<String, String> headers = new HashMap<>();
//                    headers.put("Content-Type", "application/x-www-form-urlencoded");
//                    return headers;
//                }
//            };

            AppController.getInstance().addToRequestQueue(jsonObjReq);
        } else {
            Utils.showNoInternet(context);
        }

    }



}
