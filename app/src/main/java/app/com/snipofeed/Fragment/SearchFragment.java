package app.com.snipofeed.Fragment;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.SearchActivity;
import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Adapters.GridViewSearchAdapter;
import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Helper.CustomGridLayoutManager;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

import static app.com.snipofeed.Fragment.HomeFragment.dip2px;

public class SearchFragment extends Fragment {


    View view;
    AppSettings appSettings;
    public  TextView searchText;
    private JSONArray jsonArray = new JSONArray();
    private GridViewSearchAdapter gridViewAdapter;
    JSONArray newsPosts = new JSONArray();
    int currentpageNumber = 1;
    int totalpageNumber = 1;
    RecyclerView postFeedRecyclerView;
    public static RecyclerRefreshLayout refresh_layout;
    private boolean isAdded=false;


    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search, container, false);
        appSettings = new AppSettings(getActivity());
        initViews();
        initListeners();


        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postFeedRecyclerView = view.findViewById(R.id.postFeedRecyclerView);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);
        appSettings.setCanScroll("true");


        return view;
    }

    private void getInitialData() throws JSONException {
        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        ApiCall.PostMethod(getActivity(), UrlHelper.TRENDING_POSTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    refresh_layout.setRefreshing(false);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                if (response.has("searchdata")) {
                    newsPosts = response.optJSONArray("searchdata");

                    try {
                        totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (newsPosts.length() > 0) {
                        CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
//                        GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false){
//                            @Override
//                            public boolean canScrollVertically() {
//                                return false;
//                            }
//                        };
                        postFeedRecyclerView.setLayoutManager(customGridLayoutManager);

                        gridViewAdapter = new GridViewSearchAdapter(getActivity(), newsPosts, refresh_layout, postFeedRecyclerView);
                        postFeedRecyclerView.setAdapter(gridViewAdapter);
                        gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                            @Override
                            public void onBottomReached(int position) {

                                try {
                                    getNextData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                        gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                            @Override
                            public void onClicked(String username) {
                                Bundle bundle = new Bundle();
                                bundle.putString("singlePost", "true");
                                bundle.putString("postId", username);
                                PostDetailsFragment newFragment = new PostDetailsFragment();
                                newFragment.setArguments(bundle);
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                transaction.replace(R.id.parentLayoutSearch, newFragment);
                                transaction.addToBackStack(null);
                                transaction.commit();
                            }

                            @Override
                            public void onClicked(String username, JSONObject jsonObject) {

                            }
                        });


                        if (!isAdded) {
                            isAdded = true;
                            postFeedRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                                @Override
                                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                                    int position = parent.getChildAdapterPosition(view); // item position
                                    int spanCount = 3;
                                    int spacing = 5;

                                    //spacing between views in grid

                                    if (position >= 0) {
                                        int column = position % spanCount; // item column

                                        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                                        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                                        if (position < spanCount) { // top edge
                                            outRect.top = spacing;
                                        }
                                        outRect.bottom = spacing; // item bottom
                                    } else {
                                        outRect.left = 0;
                                        outRect.right = 0;
                                        outRect.top = 0;
                                        outRect.bottom = 0;
                                    }
                                }
                            });
                        }
                    }

                }
            }
        });
    }

    private void getNextData() throws JSONException {

        currentpageNumber = currentpageNumber + 1;
        if (currentpageNumber <= totalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.TRENDING_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("searchdata");
                        for (int i = 0; i < duy.length(); i++) {
                            newsPosts.put(duy.optJSONObject(i));
                        }

                        gridViewAdapter.addValues(duy);


                    }

                }
            });
        }
    }




    private void initListeners() {
        searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveSearchActivity();
            }
        });
    }

    private void moveSearchActivity() {


        FulSearchFragment newFragment = new FulSearchFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.parentLayoutSearch, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
//        Intent intent = new Intent(getActivity(), SearchActivity.class);
//        startActivity(intent);
    }

    private void initViews() {
        postFeedRecyclerView = view.findViewById(R.id.postFeedRecyclerView);
        searchText = view.findViewById(R.id.searchText);


        refresh_layout = view.findViewById(R.id.refresh_layout);
        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getInitialData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



}
