package app.com.snipofeed.Fragment;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Helper.CustomGridLayoutManager;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.com.snipofeed.Helper.Utils.dip2px;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesDetailsFragment extends Fragment {
    View view;
    CircleImageView placesImage;
    TextView placesName, placesNameTop;
    RecyclerView contentRecyclerView;
    AppSettings appSettings;
    int currentNormalpageNumber = 1;
    int totalNormalpageNumber = 1;
    LinearLayout emptyPost;
    String area_name;
    ImageView staticMapLayout;
    String city_name;
    RecyclerRefreshLayout refresh_layout;
    private JSONArray constantArray = new JSONArray();
    private GridViewAdapter gridViewAdapter;

    public PlacesDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_places_details, container, false);
        appSettings = new AppSettings(getActivity());
        city_name = getArguments().getString("city");
        area_name = getArguments().getString("area");

        initViews();


        try {
            getPlaceDetails();
//            getTaggedPosts();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);

        refresh_layout = view.findViewById(R.id.refresh_layout);
        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getPlaceDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }

    private void getPlaceDetails() throws JSONException {
        currentNormalpageNumber = 1;
        placesName.setText(area_name);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("area", area_name);
        jsonObject.put("city", city_name);
        jsonObject.put("page", "" + currentNormalpageNumber);
        ApiCall.PostMethod(getActivity(), UrlHelper.PLACES_POST, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                if (response.has("NewsPosts")) {
                    constantArray = response.optJSONArray("NewsPosts");
                    String latitude = response.optString("lat");
                    String longitude = response.optString("lon");
                    RequestOptions options = new RequestOptions();
                    options.placeholder(getResources().getDrawable(R.drawable.map_placeholder));
                    options.error(getResources().getDrawable(R.drawable.map_placeholder));
                    Glide.with(getActivity()).load(Utils.getStyledStaticMap(latitude, longitude,getActivity())).apply(options).into(staticMapLayout);
                    setGridValues();
                } else {
                    contentRecyclerView.setVisibility(View.GONE);
                    emptyPost.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void setGridValues() {

        if (constantArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyPost.setVisibility(View.GONE);
//            GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
            CustomGridLayoutManager customGridLayoutManager = new CustomGridLayoutManager(getActivity());
            contentRecyclerView.setLayoutManager(customGridLayoutManager);

            gridViewAdapter = new GridViewAdapter(getActivity(), constantArray, refresh_layout);
            contentRecyclerView.setAdapter(gridViewAdapter);
            gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                @Override
                public void onClicked(String username) {

                }

                @Override
                public void onClicked(String username, JSONObject jsonObject) {
                    gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                        @Override
                        public void onClicked(String username) {

                        }

                        @Override
                        public void onClicked(String username, JSONObject jsonObject) {
                            Bundle bundle = new Bundle();
                            bundle.putString("singlePost", "false");
                            bundle.putString("postDetails", jsonObject.toString());
                            PostDetailsFragment newFragment = new PostDetailsFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.parentLayoutHashTags, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();

                        }
                    });

                }
            });


            contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    int position = parent.getChildAdapterPosition(view); // item position
                    int spanCount = 3;
                    int spacing = 5;//spacing between views in grid

                    if (position >= 0) {
                        int column = position % spanCount; // item column

                        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                        if (position < spanCount) { // top edge
                            outRect.top = spacing;
                        }
                        outRect.bottom = spacing; // item bottom
                    } else {
                        outRect.left = 0;
                        outRect.right = 0;
                        outRect.top = 0;
                        outRect.bottom = 0;
                    }
                }
            });
        } else {
            contentRecyclerView.setVisibility(View.GONE);
            emptyPost.setVisibility(View.VISIBLE);
        }
    }

    private void getNextData() throws JSONException {
        currentNormalpageNumber = currentNormalpageNumber + 1;
        if (currentNormalpageNumber <= totalNormalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentNormalpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.HASH_TAG_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("myposts");
                        duy = validateJsonArray(duy);
                        for (int i = 0; i < duy.length(); i++) {
                            constantArray.put(duy.optJSONObject(i));
                        }


                        gridViewAdapter.addValues(duy);


                    }

                }
            });
        }

    }

    private JSONArray validateJsonArray(JSONArray myPosts) {
        JSONArray newArray = new JSONArray();
        for (int i = 0; i < myPosts.length(); i++) {
            if (myPosts.optJSONObject(i).optJSONArray("linkData").length() != 0) {
                newArray.put(myPosts.optJSONObject(i));
            }
        }
        return newArray;
    }


    private void initViews() {
        placesImage = view.findViewById(R.id.placesImage);
        placesName = view.findViewById(R.id.placesName);
        placesNameTop = view.findViewById(R.id.placesNameTop);
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        refresh_layout = view.findViewById(R.id.refresh_layout);
        emptyPost = view.findViewById(R.id.emptyPost);
        staticMapLayout = view.findViewById(R.id.staticMapLayout);
    }

}
