package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import app.com.snipofeed.Activity.PromoteActivity;
import app.com.snipofeed.Adapters.PromotionFeedAdapter;
import app.com.snipofeed.Adapters.PromotionFeedTopAdapter;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SelectPromotionFragment extends Fragment {
    RecyclerView postPager;
    View view;
    RecyclerView promotionFeed;
    ImageView nextButton, backButton;
    JSONObject jsonObject = new JSONObject();
    JSONArray jsonArray = new JSONArray();
    String postId;
JSONObject selectedObj=new JSONObject();

    @SuppressLint("ValidFragment")
    public SelectPromotionFragment(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public SelectPromotionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_select_promotion, container, false);
        initViews();
        initListners();
        return view;
    }

    private void initListners() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PromoteActivity.class);
                intent.putExtra("postId",postId);
                intent.putExtra("postDetails",selectedObj.toString());
                startActivity(intent);

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();

            }
        });
    }

    private void initViews() {
        postPager = view.findViewById(R.id.postPager);
        promotionFeed = view.findViewById(R.id.promotionFeed);
        nextButton = view.findViewById(R.id.nextButton);
        backButton = view.findViewById(R.id.backButton);
        inittopRecyclerView();
        initBottomRecyclerView(jsonArray.optJSONObject(0));
        postId = jsonArray.optJSONObject(0).optString("id");


    }

    private void initBottomRecyclerView(JSONObject jsonObject) {
        selectedObj=jsonObject;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        PromotionFeedAdapter promotionFeedTopAdapter = new PromotionFeedAdapter(getActivity(), jsonObject, getFragmentManager(),0);
        promotionFeed.setLayoutManager(linearLayoutManager);
        promotionFeed.setAdapter(promotionFeedTopAdapter);
    }

    private void inittopRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        PromotionFeedTopAdapter promotionFeedTopAdapter = new PromotionFeedTopAdapter(getActivity(), jsonArray, getFragmentManager());
        postPager.setLayoutManager(linearLayoutManager);
        postPager.setAdapter(promotionFeedTopAdapter);
        promotionFeedTopAdapter.setListeners(new onRecyclerViewItemClicked() {
            @Override
            public void onClicked(String username) {

            }

            @Override
            public void onClicked(String username, JSONObject jsonObject) {
                postId = jsonObject.optString("id");
                initBottomRecyclerView(jsonObject);

            }
        });

    }

}
