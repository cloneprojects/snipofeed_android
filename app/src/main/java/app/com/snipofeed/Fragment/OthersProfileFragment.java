package app.com.snipofeed.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.BookMarkActivity;
import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Activity.SettingsActivity;
import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.CustomGridLayoutManager;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.com.snipofeed.Fragment.HomeFragment.dip2px;


public class OthersProfileFragment extends Fragment implements View.OnClickListener {

    CircleImageView userImageTop, userImage;
    TextView userName, userNameTop, followingCount, followersCount, postsCount, bioDescription;
    AppSettings appSettings;
    int currentNormalpageNumber = 1;
    int totalNormalpageNumber = 1;
    int currentTagpageNumber = 1;
    int totalTagpageNumber = 1;
    String userId = "";
    NestedScrollView scrollView;
    String userNameee, userImageee;
    Button followButton;
    ImageView unfollowButton;
    LinearLayout bookmarkView, taggedView, gridView, listView;
    View gridViewSelected, listViewSelected, taggedSelected, bookmarkedSelected;
    RecyclerView contentRecyclerView;
    int selectedLayout = 1;
    boolean isAdded = false;
    JSONArray constantArray = new JSONArray();
    JSONArray taggedArray = new JSONArray();
    ImageView blockButton;
    String userNameSearch = "";
    LinearLayout publicLayout, privateLayout;
    LinearLayout emptyPost, emptyTag;
    LinearLayout isFollowingLayout;
    private View view;
    private GridViewAdapter gridViewAdapter;
    private PostFeedAdapter postFeedAdapter;
    RecyclerRefreshLayout refresh_layout;

    public static OthersProfileFragment newInstance() {
        OthersProfileFragment fragment = new OthersProfileFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_others_profile, container, false);
        appSettings = new AppSettings(getActivity());
        userNameSearch = getArguments().getString("username");

        initViews();
        initListeners();

        try {
            getMyProfileData();
//            getTaggedPosts();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        setGridSelected();
        setGridValues();

    }

    private void initListeners() {
        bookmarkView.setOnClickListener(this);
        taggedView.setOnClickListener(this);
        gridView.setOnClickListener(this);
        listView.setOnClickListener(this);
        blockButton.setOnClickListener(this);
        followButton.setOnClickListener(this);
        unfollowButton.setOnClickListener(this);
        followersCount.setOnClickListener(this);
        followingCount.setOnClickListener(this);
        postsCount.setOnClickListener(this);
    }

    private void getTaggedPosts() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        jsonObject.put("userId", userId);
        ApiCall.PostMethod(getActivity(), UrlHelper.SEARCH_USER_TAGGED_POSTS, jsonObject, new VolleyCallback() {
            @SuppressLint({"CheckResult", "SetTextI18n"})
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    refresh_layout.setRefreshing(false);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                if (response.has("myposts")) {
                    taggedArray = response.optJSONArray("myposts");
                    if (selectedLayout == ConstantKeys.PINNED_VIEW) {
                        setTaggedGridValues();
                    }
                } else {

                }


            }
        });

    }

    private void setTaggedGridValues() {
        if (taggedArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.GONE);
            CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
            contentRecyclerView.setLayoutManager(customGridLayoutManager);

            gridViewAdapter = new GridViewAdapter(getActivity(), taggedArray, refresh_layout);
            contentRecyclerView.setAdapter(gridViewAdapter);
            gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextTaggedData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            if (!isAdded) {
                isAdded = true;
                contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                        int position = parent.getChildAdapterPosition(view); // item position
                        int spanCount = 3;
                        int spacing = 5;

                        //spacing between views in grid

                        if (position >= 0) {
                            int column = position % spanCount; // item column

                            outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                            outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                            if (position < spanCount) { // top edge
                                outRect.top = spacing;
                            }
                            outRect.bottom = spacing; // item bottom
                        } else {
                            outRect.left = 0;
                            outRect.right = 0;
                            outRect.top = 0;
                            outRect.bottom = 0;
                        }
                    }
                });
            }
        } else {
            contentRecyclerView.setVisibility(View.GONE);
            emptyTag.setVisibility(View.VISIBLE);
            emptyPost.setVisibility(View.GONE);
        }

    }

    private void getNextTaggedData() throws JSONException {

        currentTagpageNumber = currentTagpageNumber + 1;
        if (currentTagpageNumber <= totalTagpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentTagpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.USER_TAGGED_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalTagpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("myposts");

//                        duy = validateJsonArray(duy);
                        for (int i = 0; i < duy.length(); i++) {
                            taggedArray.put(duy.optJSONObject(i));
                        }

                        gridViewAdapter.addValues(duy);


                    }

                }
            });
        }
    }


    private void getMyProfileData() throws JSONException {
        currentNormalpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        jsonObject.put("username", userNameSearch);
        ApiCall.PostMethod(getActivity(), UrlHelper.USER_DETAILS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    refresh_layout.setRefreshing(false);
                }catch (Exception e) {
                    e.printStackTrace();
                }

                JSONObject myProfileObject = response.optJSONArray("profile").optJSONObject(0);
                if (myProfileObject.optString("privacySettings").equalsIgnoreCase("disabled")) {
                    publicLayout.setVisibility(View.VISIBLE);
                    privateLayout.setVisibility(View.GONE);


                    if (response.has("myposts")) {
                        constantArray = response.optJSONArray("myposts");

                        if (constantArray.length() > 0) {

                            try {
                                totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            contentRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                            postFeedAdapter = new PostFeedAdapter(getActivity(), constantArray, getActivity().getSupportFragmentManager());
                            contentRecyclerView.setAdapter(postFeedAdapter);
                            postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                                @Override
                                public void onBottomReached(int position) {

                                    try {
                                        getNextData();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                                @Override
                                public void onClicked(String hashtag, String caption_tags) {

                                    Bundle bundle = new Bundle();
                                    bundle.putString("hashTagId", caption_tags);
                                    HashTagFragment newFragment = new HashTagFragment();
                                    newFragment.setArguments(bundle);
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack so the user can navigate back
                                    transaction.replace(R.id.parentLayoutOthers, newFragment);

                                    transaction.addToBackStack(null);
                                    // Commit the transaction
                                    transaction.commit();


                                }
                            });


                            postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                                @Override
                                public void onClicked(String username, int caption_tags) {

                                    if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("username", username);
                                        OthersProfileFragment newFragment = new OthersProfileFragment();
                                        newFragment.setArguments(bundle);
                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        // Replace whatever is in the fragment_container view with this fragment,
                                        // and add the transaction to the back stack so the user can navigate back
                                        transaction.replace(R.id.parentLayoutOthers, newFragment);

                                        transaction.addToBackStack(null);
                                        // Commit the transaction
                                        transaction.commit();
                                    } else {
                                        MainActivity.moveToOurProfile();
                                    }
                                }
                            });


                            postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                                @Override
                                public void onClicked(String username) {
                                    if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("username", username);
                                        OthersProfileFragment newFragment = new OthersProfileFragment();
                                        newFragment.setArguments(bundle);
                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        // Replace whatever is in the fragment_container view with this fragment,
                                        // and add the transaction to the back stack so the user can navigate back
                                        transaction.replace(R.id.parentLayoutOthers, newFragment);

                                        transaction.addToBackStack(null);
                                        // Commit the transaction
                                        transaction.commit();
                                    } else {
                                        MainActivity.moveToOurProfile();
                                    }
                                }
                            });

                        }
                    }


                } else {
                    privateLayout.setVisibility(View.VISIBLE);
                    publicLayout.setVisibility(View.GONE);

                }

                postsCount.setText(myProfileObject.optString("photocount") + " " + getResources().getString(R.string.posts), TextView.BufferType.SPANNABLE);
                followingCount.setText(myProfileObject.optString("total_followings") + " " + getResources().getString(R.string.following), TextView.BufferType.SPANNABLE);
                followersCount.setText(myProfileObject.optString("total_followers") + " " + getResources().getString(R.string.followers), TextView.BufferType.SPANNABLE);
                userName.setText(myProfileObject.optString("name"));
                userNameTop.setText(myProfileObject.optString("username"));
                userNameee = myProfileObject.optString("username");
                userImageee = myProfileObject.optString("profilePic");
                Glide.with(getActivity()).load(myProfileObject.optString("profilePic")).apply(Utils.getRequestOptions()).into(userImage);
                bioDescription.setText(myProfileObject.optString("bio"));
                userId = myProfileObject.optString("id");
                try {
                    getTaggedPosts();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (myProfileObject.optString("user_follow_status").equalsIgnoreCase("true")) {
                    isFollowingLayout.setVisibility(View.VISIBLE);
                    followButton.setVisibility(View.GONE);
                } else {
                    isFollowingLayout.setVisibility(View.GONE);
                    followButton.setVisibility(View.VISIBLE);
                }
                int start = 0;
                Spannable s = (Spannable) postsCount.getText();
                int end = start + myProfileObject.optString("photocount").length();
                s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                s = (Spannable) followingCount.getText();
                end = start + myProfileObject.optString("total_followings").length();
                s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


                s = (Spannable) followersCount.getText();
                end = start + myProfileObject.optString("total_followers").length();
                s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


                if (selectedLayout == ConstantKeys.GRID_VIEW) {
                    setGridValues();

                } else if (selectedLayout == ConstantKeys.LIST_VIEW) {

                    setListValues();

                }


//                }
            }
        });

    }

    private void setGridValues() {


        if (constantArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.GONE);
            CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
            contentRecyclerView.setLayoutManager(customGridLayoutManager);

            gridViewAdapter = new GridViewAdapter(getActivity(), constantArray, refresh_layout);
            contentRecyclerView.setAdapter(gridViewAdapter);
            gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                @Override
                public void onClicked(String username) {

                }

                @Override
                public void onClicked(String username, JSONObject jsonObject) {
                    gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                        @Override
                        public void onClicked(String username) {

                        }

                        @Override
                        public void onClicked(String username, JSONObject jsonObject) {
                            Bundle bundle = new Bundle();
                            bundle.putString("singlePost", "false");
                            bundle.putString("postDetails", jsonObject.toString());
                            PostDetailsFragment newFragment = new PostDetailsFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.parentLayoutOthers, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();

                        }
                    });

                }
            });


            contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    int position = parent.getChildAdapterPosition(view); // item position
                    int spanCount = 3;
                    int spacing = 5;//spacing between views in grid

                    if (position >= 0) {
                        int column = position % spanCount; // item column

                        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                        if (position < spanCount) { // top edge
                            outRect.top = spacing;
                        }
                        outRect.bottom = spacing; // item bottom
                    } else {
                        outRect.left = 0;
                        outRect.right = 0;
                        outRect.top = 0;
                        outRect.bottom = 0;
                    }
                }
            });
        } else {
            contentRecyclerView.setVisibility(View.GONE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.VISIBLE);
        }

    }

    private void setListValues() {

        if (constantArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            contentRecyclerView.setLayoutManager(linearLayoutManager);
            postFeedAdapter = new PostFeedAdapter(getActivity(), constantArray, getActivity().getSupportFragmentManager());
            contentRecyclerView.setAdapter(postFeedAdapter);
            postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                @Override
                public void onClicked(String hashtag, String caption_tags) {


                    Bundle bundle = new Bundle();
                    bundle.putString("hashTagId", caption_tags);
                    HashTagFragment newFragment = new HashTagFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutOthers, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();


                }
            });


            postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                @Override
                public void onClicked(String username, int caption_tags) {

                    if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                        Bundle bundle = new Bundle();
                        bundle.putString("username", username);
                        OthersProfileFragment newFragment = new OthersProfileFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutOthers, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    } else {
                        MainActivity.moveToOurProfile();
                    }
                }
            });


            postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                @Override
                public void onClicked(String username) {
                    if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                        Bundle bundle = new Bundle();
                        bundle.putString("username", username);
                        OthersProfileFragment newFragment = new OthersProfileFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutOthers, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    } else {
                        MainActivity.moveToOurProfile();
                    }
                }
            });

        } else {

            contentRecyclerView.setVisibility(View.GONE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.VISIBLE);

        }
    }


    @Subscribe
    public void onBookmarked(onBookmarked onBookmarked) {
        postFeedAdapter.changeBookmarkStatus(onBookmarked.getBookMarkStatus(), onBookmarked.getPosition());
    }


    @Subscribe
    public void PostLiked(PostLiked postLiked) {
        postFeedAdapter.changeLikeStatus(postLiked.getLikeStatus(), postLiked.getPosition());
    }

    @Subscribe
    public void unFollow(unFollow unFollow) {
        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void getInitialData() throws JSONException {
        currentNormalpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        jsonObject.put("username", userNameSearch);
        ApiCall.PostMethod(getActivity(), UrlHelper.USER_DETAILS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    refresh_layout.setRefreshing(false);
                }catch (Exception e) {
                    e.printStackTrace();
                }

                JSONObject myprofileObj = response.optJSONArray("profile").optJSONObject(0);
                if (myprofileObj.optString("clickUser").equalsIgnoreCase("0")) {
                    publicLayout.setVisibility(View.GONE);
                } else {
                    publicLayout.setVisibility(View.VISIBLE);

                }

//                }
                if (response.has("NewsPosts")) {
                    constantArray = response.optJSONArray("NewsPosts");

                    try {
                        totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    contentRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                    postFeedAdapter = new PostFeedAdapter(getActivity(), constantArray, getActivity().getSupportFragmentManager());
                    contentRecyclerView.setAdapter(postFeedAdapter);
                    postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                        @Override
                        public void onBottomReached(int position) {

                            try {
                                getNextData();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                        @Override
                        public void onClicked(String hashtag, String caption_tags) {


                            Bundle bundle = new Bundle();
                            bundle.putString("hashTagId", caption_tags);
                            HashTagFragment newFragment = new HashTagFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack so the user can navigate back
                            transaction.replace(R.id.parentLayoutOthers, newFragment);

                            transaction.addToBackStack(null);
                            // Commit the transaction
                            transaction.commit();


                        }
                    });


                    postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                        @Override
                        public void onClicked(String username, int caption_tags) {

                            if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                                Bundle bundle = new Bundle();
                                bundle.putString("username", username);
                                OthersProfileFragment newFragment = new OthersProfileFragment();
                                newFragment.setArguments(bundle);
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack so the user can navigate back
                                transaction.replace(R.id.parentLayoutOthers, newFragment);

                                transaction.addToBackStack(null);
                                // Commit the transaction
                                transaction.commit();
                            } else {
                                MainActivity.moveToOurProfile();
                            }
                        }
                    });


                    postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                        @Override
                        public void onClicked(String username) {

                            if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                                Bundle bundle = new Bundle();
                                bundle.putString("username", username);
                                OthersProfileFragment newFragment = new OthersProfileFragment();
                                newFragment.setArguments(bundle);
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack so the user can navigate back
                                transaction.replace(R.id.parentLayoutOthers, newFragment);

                                transaction.addToBackStack(null);
                                // Commit the transaction
                                transaction.commit();
                            } else {
                                MainActivity.moveToOurProfile();
                            }
                        }
                    });
                }

            }
        });
    }


    private JSONArray validateJsonArray(JSONArray myPosts) {
        JSONArray newArray = new JSONArray();
        if (myPosts.length() > 0) {
            for (int i = 0; i < myPosts.length(); i++) {
                if (myPosts.optJSONObject(i).optJSONArray("linkData").length() != 0) {
                    newArray.put(myPosts.optJSONObject(i));
                }
            }
        } else {

        }
        return newArray;
    }

    private void getNextData() throws JSONException {
        currentNormalpageNumber = currentNormalpageNumber + 1;
        if (currentNormalpageNumber <= totalNormalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentNormalpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.MY_PROFILE, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {

                        if (response.has("myposts")) {
                            try {
                                totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            JSONArray duy = response.optJSONArray("myposts");
//                        duy = validateJsonArray(duy);
                            for (int i = 0; i < duy.length(); i++) {
                                constantArray.put(duy.optJSONObject(i));
                            }
                            if (selectedLayout == ConstantKeys.GRID_VIEW) {
                                gridViewAdapter.addValues(duy);
                            } else {
                                postFeedAdapter.addValues(duy);
                            }
                        }

                    }

                }
            });
        }

    }


    private void initViews() {
        userImage = view.findViewById(R.id.userImage);
//        userImageTop = view.findViewById(R.id.userImageTop);
        userName = view.findViewById(R.id.userName);
        userNameTop = view.findViewById(R.id.userNameTop);
        followingCount = view.findViewById(R.id.followingCount);
        followersCount = view.findViewById(R.id.followersCount);
        postsCount = view.findViewById(R.id.postsCount);
        bioDescription = view.findViewById(R.id.bioDescription);

        bookmarkView = view.findViewById(R.id.bookmarkView);
        taggedView = view.findViewById(R.id.taggedView);
        listView = view.findViewById(R.id.listView);
        gridView = view.findViewById(R.id.gridView);

        bookmarkedSelected = view.findViewById(R.id.bookmarkedSelected);
        taggedSelected = view.findViewById(R.id.taggedSelected);
        listViewSelected = view.findViewById(R.id.listViewSelected);
        gridViewSelected = view.findViewById(R.id.gridViewSelected);
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        blockButton = view.findViewById(R.id.blockButton);
        publicLayout = view.findViewById(R.id.publicLayout);
        privateLayout = view.findViewById(R.id.privateLayout);
        emptyPost = view.findViewById(R.id.emptyPost);
        emptyTag = view.findViewById(R.id.emptyTag);
        isFollowingLayout = view.findViewById(R.id.isFollowingLayout);
        followButton = view.findViewById(R.id.followButton);
        unfollowButton = view.findViewById(R.id.unfollowButton);
        scrollView = view.findViewById(R.id.scrollView);
        refresh_layout = view.findViewById(R.id.refresh_layout);

        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getInitialData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public void onClick(View v) {
        if (v == bookmarkView) {

            setBookmarkedSelected();
            moveToBookmarkActivity();
        } else if (v == taggedView) {
            setTaggedView();
            setTaggedGridValues();
        } else if (v == listView) {
            setListSelected();
            setListValues();

        } else if (v == gridView) {
            setGridSelected();
            setGridValues();
        } else if (v == blockButton) {
            showBlockDialog();
        } else if (v == unfollowButton) {
            showUnFollowDialog();

        } else if (v == followButton) {
            try {
                followUser();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (v == followersCount) {
            placeFollowers();
        } else if (v == followingCount) {
            placeFollowing();
        } else if (v == postsCount) {
            try {
                int top = (int) view.findViewById(R.id.topView).getX();
                int bottom = (int) view.findViewById(R.id.topView).getY();
                scrollView.smoothScrollTo(top, bottom);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void placeFollowing() {

        Bundle bundle = new Bundle();
        bundle.putString("type","others");
        bundle.putString("id",userId);
        MyFollowingFragment newFragment = new MyFollowingFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.parentLayoutOthers, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void placeFollowers() {
        Bundle bundle = new Bundle();
        bundle.putString("type","others");
        bundle.putString("id",userId);
        MyFollowersFragment newFragment = new MyFollowersFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.parentLayoutOthers, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showBlockDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.unfollow_confirmation_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView cancelButton, unfollowButton, userName;
        cancelButton = dialog.findViewById(R.id.cancelButton);
        unfollowButton = dialog.findViewById(R.id.unfollowButton);
        userName = dialog.findViewById(R.id.userName);
        userName.setText(getResources().getString(R.string.block) + " @" + userNameee + " ?");
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        CircleImageView userImage = dialog.findViewById(R.id.userImage);
        Glide.with(getActivity()).load(userImageee).apply(Utils.getRequestOptions()).into(userImage);

        unfollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    blockUser(dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void blockUser(Dialog dialog) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(getActivity(), UrlHelper.FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                changeTofollow();

            }
        });
    }

    private void showUnFollowDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.unfollow_confirmation_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
        TextView cancelButton, unfollowButton, userName;
        CircleImageView userImage;
        cancelButton = dialog.findViewById(R.id.cancelButton);
        userImage = dialog.findViewById(R.id.userImage);
        unfollowButton = dialog.findViewById(R.id.unfollowButton);
        userName = dialog.findViewById(R.id.userName);
        userName.setText(getResources().getString(R.string.unfollow) + " @" + userNameee);
        Glide.with(getActivity()).load(userImageee).apply(Utils.getRequestOptions()).into(userImage);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        unfollowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    unFollowUser(dialog);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void followUser() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(getActivity(), UrlHelper.FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                changeTofollow();

            }
        });

    }

    private void changeTofollow() {
        isFollowingLayout.setVisibility(View.VISIBLE);
        followButton.setVisibility(View.GONE);
    }

    private void unFollowUser(final Dialog dialog) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("followerId", userId);
        ApiCall.PostMethod(getActivity(), UrlHelper.UN_FOLLOW_USER, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                changeToUnfollow();

            }
        });
    }

    private void changeToUnfollow() {
        isFollowingLayout.setVisibility(View.GONE);
        followButton.setVisibility(View.VISIBLE);

    }

    private void showSettingsDialog() {
    }

    private void moveToSettingsActivity() {

        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);

    }

    private void moveToBookmarkActivity() {
        Intent intent = new Intent(getActivity(), BookMarkActivity.class);
        startActivity(intent);
    }


    private void setGridSelected() {
        selectedLayout = ConstantKeys.GRID_VIEW;
        bookmarkedSelected.setVisibility(View.INVISIBLE);
        taggedSelected.setVisibility(View.INVISIBLE);
        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.VISIBLE);


    }

    private void setListSelected() {
        selectedLayout = ConstantKeys.LIST_VIEW;
        bookmarkedSelected.setVisibility(View.INVISIBLE);
        taggedSelected.setVisibility(View.INVISIBLE);
        listViewSelected.setVisibility(View.VISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }

    private void setTaggedView() {
        selectedLayout = ConstantKeys.PINNED_VIEW;
        bookmarkedSelected.setVisibility(View.INVISIBLE);
        taggedSelected.setVisibility(View.VISIBLE);
        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }


    private void setBookmarkedSelected() {
        selectedLayout = ConstantKeys.BOOKMARKED_VIEW;
        bookmarkedSelected.setVisibility(View.VISIBLE);
        taggedSelected.setVisibility(View.INVISIBLE);
        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }
}
