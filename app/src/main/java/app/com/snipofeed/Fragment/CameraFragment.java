package app.com.snipofeed.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.cameraview.CameraView;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;

import app.com.snipofeed.Activity.FilterActivity;
import app.com.snipofeed.Activity.PostFeedActivity;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraFragment extends Fragment implements View.OnClickListener {
    public static ArrayList<String> multipleImages = new ArrayList<>();
    public static ArrayList<String> multipleImagesContentType= new ArrayList<>();
    public static com.wonderkiln.camerakit.CameraView mCameraView;
    View view;
    RelativeLayout choosePhoto;
    ImageView chnageFlash,changeCamera;
    private Handler mBackgroundHandler;
    public static RelativeLayout topLayout;
    private String TAG = CameraFragment.class.getSimpleName();



    private void savePhoto(byte[] data) {

        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = Utils.getImageFileName(getActivity());
        final File file = new File(newFolder, camera_captureFile);


        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            os.write(data);
            os.close();
        } catch (IOException e) {
            Log.w(TAG, "Cannot write to " + file, e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
        multipleImages=new ArrayList<>();
        multipleImages.add(file.getAbsolutePath());
        multipleImagesContentType.add("image");

        Intent intent = new Intent(getActivity(), FilterActivity.class);
        intent.putStringArrayListExtra("images", multipleImages);
        intent.putStringArrayListExtra("contentType", multipleImagesContentType);
        intent.putExtra("type", "image");
        startActivity(intent);

    }

    public CameraFragment() {
        // Required empty public constructor
    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_camera, container, false);
        mCameraView = view.findViewById(R.id.mCameraView);
        choosePhoto = view.findViewById(R.id.choosePhoto);
        changeCamera = view.findViewById(R.id.changeCamera);
        chnageFlash = view.findViewById(R.id.chnageFlash);
        topLayout = view.findViewById(R.id.topLayout);


        choosePhoto.setOnClickListener(this);
        chnageFlash.setOnClickListener(this);
        changeCamera.setOnClickListener(this);

        view.findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }



    @Override
    public void onClick(View v) {
        if (v == choosePhoto) {
            takePhoto();
        } else if (v==chnageFlash)
        {
            if (mCameraView.getFlash()== CameraKit.Constants.FLASH_AUTO)
            {
                mCameraView.setFlash(CameraKit.Constants.FLASH_ON);
                chnageFlash.setImageResource(R.drawable.flash_enabled);
            } else if (mCameraView.getFlash()==CameraKit.Constants.FLASH_ON)
            {
                mCameraView.setFlash(CameraKit.Constants.FLASH_OFF);
                chnageFlash.setImageResource(R.drawable.flash_off);
            } else if (mCameraView.getFlash()==CameraKit.Constants.FLASH_OFF)
            {
                chnageFlash.setImageResource(R.drawable.flash_automatic);
                mCameraView.setFlash(CameraKit.Constants.FLASH_AUTO);

            }

        } else if (v==changeCamera)
        {

            if (mCameraView.isFacingFront())
            {
                mCameraView.setFacing(CameraKit.Constants.FACING_BACK);
            } else {
                mCameraView.setFacing(CameraKit.Constants.FACING_FRONT);

            }
        }
    }

    private void takePhoto() {
        mCameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
            @Override
            public void callback(CameraKitImage cameraKitImage) {
                savePhoto(cameraKitImage.getJpeg());
            }
        });

    }
}
