package app.com.snipofeed.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import app.com.snipofeed.Activity.PostFeedActivity;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {
    View view;
    public static CameraView cameraView;
    RelativeLayout chooseVideo;
    boolean isRecordingStarted = false;
    ProgressBar progressBar;
    LinearLayout timerLayout;
    public static RelativeLayout topLayout;
    ImageView recordImage;
    ImageView chnageFlash,changeCamera;
    boolean isButtonLongPressed = false;
    TextView deleteVideo,nextButton,timerText;


    CountDownTimer countDownTimer=new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            blinkImage();
            //this will be done every 1000 milliseconds ( 1 seconds )
            int progress = (int) ((60000 - millisUntilFinished) / 1000);
            if (progress>9) {
                timerText.setText("00:" +progress);
            } else if (progress<10)
            {
                timerText.setText("00:0" +progress);
            } if (progress==60)
            {
                timerText.setText("01:00");

            }

            progressBar.setProgress(progress);
        }

        @Override
        public void onFinish() {
            animation.cancel();
            deleteVideo.setVisibility(View.VISIBLE);
            timerLayout.setVisibility(View.GONE);
            //the progressBar will be invisible after 60 000 miliseconds ( 1 minute)

        }

    };
    private AlphaAnimation animation;
    private File file;
    private ArrayList<String> multipleImages;
    private ArrayList<String> multipleImagesContentType;


    public VideoFragment() {
        // Required empty public constructor
    }

    public void blinkImage()
    {
        animation = new AlphaAnimation(1, 0);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        recordImage.startAnimation(animation);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_video, container, false);

        cameraView = view.findViewById(R.id.cameraView);
        progressBar = view.findViewById(R.id.progressBar);
        timerLayout = view.findViewById(R.id.timerLayout);
        recordImage = view.findViewById(R.id.recordImage);
        nextButton= view.findViewById(R.id.nextButton);
        topLayout= view.findViewById(R.id.topLayout);
        timerText= view.findViewById(R.id.timerText);
        deleteVideo = view.findViewById(R.id.deleteVideo);

        changeCamera = view.findViewById(R.id.changeCamera);
        chnageFlash = view.findViewById(R.id.chnageFlash);

        chnageFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cameraView.getFlash()== CameraKit.Constants.FLASH_AUTO)
                {
                    cameraView.setFlash(CameraKit.Constants.FLASH_ON);
                    chnageFlash.setImageResource(R.drawable.flash_enabled);
                } else if (cameraView.getFlash()==CameraKit.Constants.FLASH_ON)
                {
                    cameraView.setFlash(CameraKit.Constants.FLASH_OFF);
                    chnageFlash.setImageResource(R.drawable.flash_off);
                } else if (cameraView.getFlash()==CameraKit.Constants.FLASH_OFF)
                {
                    chnageFlash.setImageResource(R.drawable.flash_automatic);
                    cameraView.setFlash(CameraKit.Constants.FLASH_AUTO);

                }

            }
        });
        changeCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cameraView.isFacingFront())
                {
                    cameraView.setFacing(CameraKit.Constants.FACING_BACK);
                } else {
                    cameraView.setFacing(CameraKit.Constants.FACING_FRONT);

                }
            }
        });

        deleteVideo.setVisibility(View.GONE);
        nextButton.setVisibility(View.GONE);




        chooseVideo = (RelativeLayout) view.findViewById(R.id.chooseVideo);

        chooseVideo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isButtonLongPressed = true;
                timerLayout.setVisibility(View.VISIBLE);
                deleteVideo.setVisibility(View.GONE);

                startRecording();
                countDownTimer.start();
                return false;
            }
        });
        chooseVideo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // We're only interested in anything if our speak button is currently pressed.
                    if (isButtonLongPressed) {
                        // Do something when the button is released.

                        isButtonLongPressed = false;
                        topLayout.setVisibility(View.VISIBLE);
                        nextButton.setVisibility(View.VISIBLE);
                        stopRecording();
                    }
                }
                return false;
            }
        });

        deleteVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file.exists()) {
                    file.delete();
                    deleteVideo.setVisibility(View.GONE);
                    nextButton.setVisibility(View.GONE);
                }

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multipleImages=new ArrayList<>();
                multipleImagesContentType=new ArrayList<>();
                multipleImages.add(file.getAbsolutePath());
                multipleImagesContentType.add("video");

                Intent intent = new Intent(getActivity(), PostFeedActivity.class);
                intent.putStringArrayListExtra("images", multipleImages);
                intent.putStringArrayListExtra("contentType", multipleImagesContentType);
                intent.putExtra("type", "video");
                startActivity(intent);
            }
        });

        view.findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });



        return view;
    }


    private void stopRecording() {
        countDownTimer.cancel();

        animation.cancel();
        deleteVideo.setVisibility(View.VISIBLE);
        deleteVideo.setVisibility(View.VISIBLE);

        isRecordingStarted = false;
        Utils.toast(getActivity(), getString(R.string.recording_finished));

        cameraView.stopVideo();
        timerLayout.setVisibility(View.GONE);

    }

    private void startRecording() {
        isRecordingStarted = true;
        Utils.toast(getActivity(), getString(R.string.recording_started));
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(), getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder, getResources().getString(R.string.app_name) + "_Video").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }


        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("VIDEO_" + randomImageNo);
        file = new File(newFolder, camera_captureFile + ".mp4");
        cameraView.captureVideo(file);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }


}
