package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import app.com.snipofeed.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedVideoFragment extends Fragment {
    String url;
    Activity context;
    VideoView postFeedVideo;

    private boolean fragmentResume;
    private boolean fragmentOnCreated;
    private boolean fragmentVisible;



    @SuppressLint("ValidFragment")
    public FeedVideoFragment(String url, Activity context) {
        this.url=url;
        this.context=context;
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {   // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            playVideo();
        } else if (visible) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!visible && fragmentOnCreated) {// only when you go out of fragment screen
            stopVideo();
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void playVideo() {
        Uri uri= Uri.parse(url);
        postFeedVideo.setVideoURI(uri);
        postFeedVideo.start();


    }
    public void stopVideo()
    {
        postFeedVideo.stopPlayback();
    }

    public FeedVideoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_feed_video, container, false);
//        postFeedVideo=view.findViewById(R.id.postFeedVideo);
        return view;
    }

}
