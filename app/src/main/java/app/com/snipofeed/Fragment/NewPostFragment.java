package app.com.snipofeed.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Activity.NewPostActivity;
import app.com.snipofeed.R;

import static app.com.snipofeed.Fragment.BaseFragment.ARGS_INSTANCE;


public class NewPostFragment extends Fragment {


    int fragCount;
    boolean isVisible=false;


    public static NewPostFragment newInstance() {
        NewPostFragment fragment = new NewPostFragment();
        return fragment;
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            moveNewPostActivity();

        } else {

        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_new_post, container, false);




        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }


        return view;
    }

    private void moveNewPostActivity() {
        Intent intent=new Intent(getActivity(), NewPostActivity.class);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.activity_slide_up,R.anim.activity_slide_bottom);
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.bottomNavigationView.setSelectedItemId(R.id.home_item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


//        ( (MainActivity)getActivity()).updateToolbarTitle((fragCount == 0) ? "News" : "Sub News "+fragCount);


    }
}
