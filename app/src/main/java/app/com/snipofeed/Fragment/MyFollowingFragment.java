package app.com.snipofeed.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Adapters.FollowersAdapter;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;


public class MyFollowingFragment extends Fragment {
    View view;
    RecyclerView contentRecyclerView;
    AppSettings appSettings;
    String userid;


    public MyFollowingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_following, container, false);
        appSettings = new AppSettings(getActivity());
        initViews();
        return view;
    }

    private void initViews() {
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        if (getArguments().getString("type").equalsIgnoreCase("others")) {
            userid = getArguments().getString("id");
            getOthersData();
        } else {
            getData();
        }
    }

    private void getOthersData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("id", userid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethod(getActivity(), UrlHelper.OTHERS_FOLLOWING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                FollowersAdapter followersAdapter = new FollowersAdapter(getActivity(), response.optJSONArray("following"));
                contentRecyclerView.setLayoutManager(linearLayoutManager);
                contentRecyclerView.setAdapter(followersAdapter);

                followersAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                    @Override
                    public void onClicked(String username) {

                        if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                            Bundle bundle = new Bundle();
                            bundle.putString("username", username);
                            OthersProfileFragment newFragment = new OthersProfileFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack so the user can navigate back
                            transaction.replace(R.id.parentLayoutMyFollowers, newFragment);

                            transaction.addToBackStack(null);
                            // Commit the transaction
                            transaction.commit();
                        } else {
                            MainActivity.moveToOurProfile();
                        }
                    }
                });


            }
        });

    }

    private void getData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("accessToken", appSettings.getAccessToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethod(getActivity(), UrlHelper.MY_FOLLOWING, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                FollowersAdapter followersAdapter = new FollowersAdapter(getActivity(), response.optJSONArray("following"));
                contentRecyclerView.setLayoutManager(linearLayoutManager);
                contentRecyclerView.setAdapter(followersAdapter);

                followersAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                    @Override
                    public void onClicked(String username) {

                        if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                            Bundle bundle = new Bundle();
                            bundle.putString("username", username);
                            OthersProfileFragment newFragment = new OthersProfileFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            // Replace whatever is in the fragment_container view with this fragment,
                            // and add the transaction to the back stack so the user can navigate back
                            transaction.replace(R.id.parentLayoutMyFollowers, newFragment);

                            transaction.addToBackStack(null);
                            // Commit the transaction
                            transaction.commit();
                        } else {
                            MainActivity.moveToOurProfile();
                        }
                    }
                });


            }
        });
    }

}
