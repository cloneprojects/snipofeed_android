package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import app.com.snipofeed.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeedImageFragment extends Fragment {
    String url;
    ImageView postImage;
    Activity activity;

    public FeedImageFragment() {
    }

    @SuppressLint("ValidFragment")
    public FeedImageFragment(String url, Activity activity) {
        this.url=url;
        this.activity=activity;

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_feed_image, container, false);
        postImage=view.findViewById(R.id.postImage);
        Glide.with(activity).load(url).into(postImage);
        return view;
    }


}
