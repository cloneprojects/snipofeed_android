package app.com.snipofeed.Fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.Activity.FilterActivity;
import app.com.snipofeed.Activity.PostFeedActivity;
import app.com.snipofeed.Adapters.GridImageViewAdapter;
import app.com.snipofeed.Cropper.InstaCropperView;
import app.com.snipofeed.Events.ImageSelected;
import app.com.snipofeed.Events.SpinnerItemSelected;
import app.com.snipofeed.Helper.DynamicWidthSpinner;
import app.com.snipofeed.R;
import app.com.snipofeed.model.Folder;
import app.com.snipofeed.model.Image;
import de.hdodenhof.circleimageview.CircleImageView;


public class GalleryFragment extends Fragment {

    private static final String TAG = GalleryFragment.class.getSimpleName();
    public static GridImageViewAdapter gridImageViewAdapter;
    public static boolean isMultipleSelected = false;
    public static ArrayList<String> multipleImages = new ArrayList<>();
    public static ArrayList<String> multipleImagesContentType = new ArrayList<>();

    public static List<Folder> folders;
    public static DynamicWidthSpinner spinner;
    private final String[] projection = new String[]{MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};
    private final String[] projectionVideo = new String[]{MediaStore.Video.Media._ID, MediaStore.Video.Media.DISPLAY_NAME, MediaStore.Video.Media.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME};
    public ArrayList<String> folderNames = new ArrayList<>();
    RecyclerView galleryFragmentRecyclerView;
    InstaCropperView instaCropper;
    VideoView instaVideo;
    RelativeLayout multipleSelection;
    CircleImageView selectedSymbol;
    TextView nextButton;
    private Handler handler;
    private ArrayList<Image> folderImages = new ArrayList<>();
    private View view;
    private ArrayList<Image> images = new ArrayList<>();
    private ArrayAdapter<String> spinnerArrayAdapter;


    public GalleryFragment() {
        // Required empty public constructor
    }

    private void getData() {
        new loadImages().execute();
    }

    private void initSpinner() {

        spinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        folderNames); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Subscribe
    public void ImageSelected(ImageSelected imageSelected) {
        if (imageSelected.getType().equalsIgnoreCase("image")) {
            instaVideo.setVisibility(View.GONE);
            instaCropper.setVisibility(View.VISIBLE);
            instaCropper.setImageUri(Uri.fromFile(new File(imageSelected.getImagePath())));
        } else {

            instaVideo.setVisibility(View.VISIBLE);
            instaCropper.setVisibility(View.GONE);
            setVideo(imageSelected.getImagePath());

        }

    }

    private void setPhoto(Image image) {
        instaCropper.setImageUri(Uri.fromFile(new File(image.getPath())));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_gallery, container, false);
        initRecyclerView();

        initListeners();
        initViews();
        getData();


        return view;
    }

    public Folder getFolder(String name) {
        for (Folder folder : folders) {
            if (folder.getFolderName().equals(name)) {
                return folder;
            }
        }
        return null;
    }

    private void initViews() {

        nextButton = view.findViewById(R.id.nextButton);
        instaVideo = view.findViewById(R.id.instaVideo);

        spinner = (DynamicWidthSpinner) view.findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                EventBus.getDefault().post(new SpinnerItemSelected(position, folders.get(position)));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isVideoAvailable=false;
             if (multipleImagesContentType.size()==1)
             {
                 if (multipleImagesContentType.get(0).equalsIgnoreCase("video"))
                 {
                     isVideoAvailable=true;
                 }
             }
                if (!isVideoAvailable) {
                    Intent intent = new Intent(getActivity(), FilterActivity.class);
                    intent.putStringArrayListExtra("images", multipleImages);
                    intent.putStringArrayListExtra("contentType", multipleImagesContentType);
                    intent.putExtra("type", "image");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), PostFeedActivity.class);
                    intent.putStringArrayListExtra("images", multipleImages);
                    intent.putExtra("totalImage", "0");
                    intent.putStringArrayListExtra("contentType", multipleImagesContentType);
                    intent.putExtra("type", "image");
                    startActivity(intent);
                }
            }
        });

    }

    private void initListeners() {
        multipleSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multipleImages=new ArrayList<>();

                if (!isMultipleSelected) {
                    isMultipleSelected = true;
                    EventBus.getDefault().post(new SpinnerItemSelected(spinner.getSelectedItemPosition(), folders.get(spinner.getSelectedItemPosition())));
                } else {
                    isMultipleSelected = false;
                    EventBus.getDefault().post(new SpinnerItemSelected(spinner.getSelectedItemPosition(), folders.get(spinner.getSelectedItemPosition())));
                }
            }
        });
        view.findViewById(R.id.closeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


    }

    private void initRecyclerView() {

        selectedSymbol = (CircleImageView) view.findViewById(R.id.selectedSymbol);
        instaCropper = view.findViewById(R.id.instaCropper);
        multipleSelection = view.findViewById(R.id.multipleSelection);

        galleryFragmentRecyclerView = view.findViewById(R.id.galleryFragmentRecyclerView);

        /*galleryFragmentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view); // item position
                int spanCount = 3;
                int spacing = 5;
                //spacing between views in grid

                if (position >= 0) {
                    int column = position % spanCount; // item column

                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = 0;
                    outRect.right = 0;
                    outRect.top = 0;
                    outRect.bottom = 0;
                }
            }
        });*/


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        galleryFragmentRecyclerView.setLayoutManager(gridLayoutManager);
        Log.d(TAG, "initRecyclerView: "+folderImages.size());
        gridImageViewAdapter = new GridImageViewAdapter(getActivity(), folderImages);
        galleryFragmentRecyclerView.setNestedScrollingEnabled(false);
        galleryFragmentRecyclerView.setHasFixedSize(false);
        galleryFragmentRecyclerView.setAdapter(gridImageViewAdapter);

        NestedScrollView nestedScroll;
        nestedScroll = view.findViewById(R.id.nestedScroll);

        int count=folderImages.size();

        final int[] loops = {144};

        if (144 < folderImages.size()) {
            GridImageViewAdapter.count = 144;
            nestedScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (!v.canScrollVertically(1)) {
                        if (v.getChildAt(v.getChildCount() - 1) != null) {
                            if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                                    scrollY > oldScrollY) {
                                if(loops[0] <folderImages.size()){
                                    loops[0] = loops[0] +144;
                                    Log.e(TAG, "onScrollChangeIf: " + loops[0]);
                                    GridImageViewAdapter.count = loops[0];
                                    Log.e(TAG, "GridImageViewAdapterCount: " + folderImages.size());
                                    gridImageViewAdapter.notifyDataSetChanged();
                                }else{
                                    Log.e(TAG, "onScrollChangeElse: "+loops[0] );
                                    GridImageViewAdapter.count = folderImages.size();
                                    gridImageViewAdapter.notifyDataSetChanged();

                                }

                            }
                        }
                    }
                }
            });
        } else {
            GridImageViewAdapter.count = folderImages.size();
        }



    }

    @Subscribe
    public void SpinnerSelected(SpinnerItemSelected spinnerItemSelected) {
        folderImages = spinnerItemSelected.getFolders().getImages();
        initRecyclerView();
        if (spinnerItemSelected.getFolderType().equalsIgnoreCase("image")) {
            instaVideo.setVisibility(View.GONE);
            instaCropper.setVisibility(View.VISIBLE);
            setPhoto(spinnerItemSelected.getFolders().getImages().get(0));
            if (!isMultipleSelected) {
                multipleImages=new ArrayList<>();
                multipleImagesContentType=new ArrayList<>();
                multipleImages.add(spinnerItemSelected.getFolders().getImages().get(0).getPath());
                multipleImagesContentType.add("image");
            }
        } else {
            instaVideo.setVisibility(View.VISIBLE);
            instaCropper.setVisibility(View.GONE);
            setVideo(spinnerItemSelected.getFolders().getImages().get(0).getPath());
            if (!isMultipleSelected) {
                multipleImages=new ArrayList<>();
                multipleImagesContentType=new ArrayList<>();
                multipleImages.add(spinnerItemSelected.getFolders().getImages().get(0).getPath());
                multipleImagesContentType.add("video");
            }
        }

    }

    private void setVideo(String image) {


        instaVideo.setVideoPath(image);
        instaVideo.setMediaController(null);
        instaVideo.start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class loadImages extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                        null, null, MediaStore.Images.Media.DATE_ADDED);


                ArrayList<Image> temp = new ArrayList<>(cursor.getCount());
                File file;
                folders = new ArrayList<>();

                if (cursor.moveToLast()) {
                    do {

                        long id = cursor.getLong(cursor.getColumnIndex(projection[0]));
                        String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                        String path = cursor.getString(cursor.getColumnIndex(projection[2]));
                        String bucket = cursor.getString(cursor.getColumnIndex(projection[3]));

                        file = new File(path);
                        if (file.exists()) {
                            Image image = new Image(id, name, path, false, "image");
                            temp.add(image);


                            Folder folder = getFolder(bucket);
                            if (folder == null) {
                                folder = new Folder(bucket, "image");
                                folderNames.add(folder.getFolderName());
                                folders.add(folder);
                            }

                            folder.getImages().add(image);

                        }

                        if (cursor.isLast()) {

                        }

                    } while (cursor.moveToPrevious());
                }
                cursor.close();
                if (images == null) {
                    images = new ArrayList<>();
                }
                images.clear();
                images.addAll(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (images.size() > 0) {
                multipleImages.add(images.get(0).getPath());
                multipleImagesContentType.add("image");
            }
            new loadVideos().execute();
//            initSpinner();

        }

    }


    public class loadVideos extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                Cursor cursor = getActivity().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projectionVideo,
                        null, null, MediaStore.Video.Media.DATE_ADDED);


                ArrayList<Image> temp = new ArrayList<>(cursor.getCount());
                File file;


                if (cursor.moveToLast()) {
                    do {

                        long id = cursor.getLong(cursor.getColumnIndex(projection[0]));
                        String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                        String path = cursor.getString(cursor.getColumnIndex(projection[2]));
                        String bucket = cursor.getString(cursor.getColumnIndex(projection[3]));

                        file = new File(path);
                        if (file.exists()) {
                            Image image = new Image(id, name, path, false, "video");
                            temp.add(image);


                            Folder folder = getFolder(bucket);
                            if (folder == null) {
                                folder = new Folder(bucket, "video");
                                folderNames.add(folder.getFolderName());
                                folders.add(folder);
                            }

                            folder.getImages().add(image);

                        }

                        if (cursor.isLast()) {

                        }

                    } while (cursor.moveToPrevious());
                }
                cursor.close();
                if (images == null) {
                    images = new ArrayList<>();
                }
                images.clear();
                images.addAll(temp);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            initSpinner();

        }

    }


}
