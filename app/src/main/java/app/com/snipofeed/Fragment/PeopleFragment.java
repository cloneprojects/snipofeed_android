package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Adapters.SearchListAdapter;
import app.com.snipofeed.Events.onSearchStarted;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.Helper.onSearchPeopleClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class PeopleFragment extends Fragment {
    public RecyclerView contentRecyclerView;
    int type;
    View view;
    onSearchPeopleClicked onSearchPeopleClicked;
    private String TAG = PeopleFragment.class.getSimpleName();


    @SuppressLint("ValidFragment")
    public PeopleFragment(onSearchPeopleClicked onSearchPeopleClicked) {
        this.onSearchPeopleClicked= onSearchPeopleClicked;
    }

    public PeopleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Started");
        EventBus.getDefault().register(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: Attached");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onAttach: Detached");

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStart: Stopped");

        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        initPeople();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void searchStarted(onSearchStarted onSearchStarted) {
        Log.d(TAG, "searchStarted: " + onSearchStarted.getFragmentNumber());

        initPeople();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_people, container, false);
        Log.d(TAG, "onCreateView: ");
        return view;


    }


    public void initPeople() {
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        if (FulSearchFragment.searchText.getText().toString().length()>0) {
            final JSONObject jsonObject = new JSONObject();
            final AppSettings appSettings = new AppSettings(getActivity());
            try {
                jsonObject.put("accessToken", appSettings.getAccessToken());
                jsonObject.put("name", FulSearchFragment.searchText.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.SEARCH_USER, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d(TAG, "Search: " + response);

                    JSONArray jsonArray = new JSONArray();
                    jsonArray = response.optJSONArray("users");
                    SearchListAdapter searchListAdapter = new SearchListAdapter(getActivity(), jsonArray, ConstantKeys.isPeople);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    contentRecyclerView.setLayoutManager(linearLayoutManager);
                    contentRecyclerView.setAdapter(searchListAdapter);
//                searchListAdapter.notifyDataSetChanged();
                    searchListAdapter.setClickListeners(new onRecyclerViewItemClicked() {
                        @Override
                        public void onClicked(String username) {

                            if (!username.equalsIgnoreCase(appSettings.getUserName())) {

                                onSearchPeopleClicked.onClicked(username);

                            } else {
                                MainActivity.moveToOurProfile();
                            }
                        }

                        @Override
                        public void onClicked(String username, JSONObject jsonObject) {

                        }
                    });


                }
            });
        }
    }

}
