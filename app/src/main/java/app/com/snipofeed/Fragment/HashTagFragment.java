package app.com.snipofeed.Fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Adapters.HashTagSugesstionAdapterHorizontal;
import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.CustomGridLayoutManager;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onHashTagSuggestionItemClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;
import de.hdodenhof.circleimageview.CircleImageView;

import static app.com.snipofeed.Activity.MainActivity.moveToOurProfile;
import static app.com.snipofeed.Helper.Utils.dip2px;


public class HashTagFragment extends Fragment implements View.OnClickListener {

    CircleImageView hashTagImage;
    TextView hashTagName, hashTagNameTop, postsCount;
    AppSettings appSettings;
    int currentNormalpageNumber = 1;
    int totalNormalpageNumber = 1;
    String hashTagId = "";
    String hashTagNamee, hashTagImageee;
    LinearLayout gridView, listView;
    View gridViewSelected, listViewSelected;
    RecyclerView contentRecyclerView;
    int selectedLayout = 1;
    boolean isAdded = false;
    JSONArray constantArray = new JSONArray();
    LinearLayout emptyPost, emptyTag;
    private View view;
    private GridViewAdapter gridViewAdapter;
    private PostFeedAdapter postFeedAdapter;
    RecyclerView suggestionHashTags;
    RecyclerRefreshLayout refresh_layout;

    public static HashTagFragment newInstance() {
        HashTagFragment fragment = new HashTagFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_hash_tags, container, false);
        appSettings = new AppSettings(getActivity());
        if (getArguments() != null) {
            hashTagId = getArguments().getString("hashTagId");
        }

        initViews();
        initListeners();

        try {
            getHashTagDetails();
//            getTaggedPosts();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);

        refresh_layout = view.findViewById(R.id.refresh_layout);
        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getHashTagDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        setGridSelected();
        setGridValues();

    }

    private void initListeners() {
        gridView.setOnClickListener(this);
        listView.setOnClickListener(this);

    }


    private void getHashTagDetails() throws JSONException {
        currentNormalpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        jsonObject.put("tagId", hashTagId);
        ApiCall.PostMethod(getActivity(), UrlHelper.HASH_TAG_POSTS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
//                try {
//                    refresh_layout.setRefreshing(false);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();

                JSONObject hashTagObject = response.optJSONArray("tag").optJSONObject(0);
                JSONArray suggestionArray=hashTagObject.optJSONArray("suggessions");

                LinearLayoutManager hlinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                suggestionHashTags.setLayoutManager(hlinearLayoutManager );
                HashTagSugesstionAdapterHorizontal hashTagSugesstionAdapterHorizontal=new HashTagSugesstionAdapterHorizontal(getActivity(), suggestionArray);
                suggestionHashTags.setAdapter(hashTagSugesstionAdapterHorizontal);
                hashTagSugesstionAdapterHorizontal.setClickListener(new onHashTagSuggestionItemClicked() {
                    @Override
                    public void Clicked(String caption_tags) {
                        Bundle bundle = new Bundle();
                        bundle.putString("hashTagId", caption_tags);
                        HashTagFragment newFragment = new HashTagFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutHashTags, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    }
                });


                if (response.has("myposts")) {
                    constantArray = response.optJSONArray("myposts");

                    if (constantArray.length() > 0) {

                        try {
                            totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        contentRecyclerView.setLayoutManager(linearLayoutManager);
                        postFeedAdapter = new PostFeedAdapter(getActivity(), constantArray, getActivity().getSupportFragmentManager());
                        contentRecyclerView.setAdapter(postFeedAdapter);
                        postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                            @Override
                            public void onBottomReached(int position) {

                                try {
                                    getNextData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                        postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                            @Override
                            public void onClicked(String hashtag, String caption_tags) {
                                Bundle bundle = new Bundle();
                                bundle.putString("hashTagId", "" + caption_tags);
                                HashTagFragment newFragment = new HashTagFragment();
                                newFragment.setArguments(bundle);
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack so the user can navigate back
                                transaction.replace(R.id.parentLayoutHashTags, newFragment);

                                transaction.addToBackStack(null);
                                // Commit the transaction
                                transaction.commit();


                            }
                        });


                        postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                            @Override
                            public void onClicked(String username, int caption_tags) {
                                if (!username.equalsIgnoreCase(appSettings.getUserName())) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", username);
                                    OthersProfileFragment newFragment = new OthersProfileFragment();
                                    newFragment.setArguments(bundle);
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack so the user can navigate back
                                    transaction.replace(R.id.parentLayoutHashTags, newFragment);

                                    transaction.addToBackStack(null);
                                    // Commit the transaction
                                    transaction.commit();
                                } else {
                                    moveToOurProfile();
                                }
                            }
                        });

                        postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                            @Override
                            public void onClicked(String username) {
                                if (!username.equalsIgnoreCase(appSettings.getUserName())) {

                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", username);
                                    OthersProfileFragment newFragment = new OthersProfileFragment();
                                    newFragment.setArguments(bundle);
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack so the user can navigate back
                                    transaction.replace(R.id.parentLayoutHashTags, newFragment);

                                    transaction.addToBackStack(null);
                                    // Commit the transaction
                                    transaction.commit();
                                }
                                else {
                                    moveToOurProfile();
                                }
                            }
                        });


                    }
                }


                postsCount.setText(hashTagObject.optString("post_count") + " " + getResources().getString(R.string.posts), TextView.BufferType.SPANNABLE);
                hashTagNamee = hashTagObject.optString("tagName");
                hashTagName.setText(hashTagNamee);
                hashTagNameTop.setText(hashTagNamee);
                Glide.with(getActivity()).load(getResources().getDrawable(R.drawable.hash_tag_item)).into(hashTagImage);


                int start = 0;
                Spannable s = (Spannable) postsCount.getText();
                int end = start + hashTagObject.optString("post_count").length();
                s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                if (selectedLayout == ConstantKeys.GRID_VIEW) {
                    setGridValues();

                } else if (selectedLayout == ConstantKeys.LIST_VIEW) {

                    setListValues();

                }


//                }
            }
        });

    }



    private void setGridValues() {


        if (constantArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.GONE);
//            GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
            CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
            contentRecyclerView.setLayoutManager(customGridLayoutManager);

            gridViewAdapter = new GridViewAdapter(getActivity(), constantArray, refresh_layout);
            contentRecyclerView.setAdapter(gridViewAdapter);
            gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                @Override
                public void onClicked(String username) {

                }

                @Override
                public void onClicked(String username, JSONObject jsonObject) {
                    gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
                        @Override
                        public void onClicked(String username) {

                        }

                        @Override
                        public void onClicked(String username, JSONObject jsonObject) {
                            Bundle bundle = new Bundle();
                            bundle.putString("singlePost", "false");
                            bundle.putString("postDetails", jsonObject.toString());
                            PostDetailsFragment newFragment = new PostDetailsFragment();
                            newFragment.setArguments(bundle);
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.parentLayoutHashTags, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();

                        }
                    });

                }
            });


            contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    int position = parent.getChildAdapterPosition(view); // item position
                    int spanCount = 3;
                    int spacing = 5;//spacing between views in grid

                    if (position >= 0) {
                        int column = position % spanCount; // item column

                        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                        if (position < spanCount) { // top edge
                            outRect.top = spacing;
                        }
                        outRect.bottom = spacing; // item bottom
                    } else {
                        outRect.left = 0;
                        outRect.right = 0;
                        outRect.top = 0;
                        outRect.bottom = 0;
                    }
                }
            });
        } else {
            contentRecyclerView.setVisibility(View.GONE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.VISIBLE);
        }

    }

    private void setListValues() {

        if (constantArray.length() > 0) {
            contentRecyclerView.setVisibility(View.VISIBLE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.GONE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            contentRecyclerView.setLayoutManager(linearLayoutManager);
            postFeedAdapter = new PostFeedAdapter(getActivity(), constantArray, getActivity().getSupportFragmentManager());
            contentRecyclerView.setAdapter(postFeedAdapter);
            postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                @Override
                public void onClicked(String hashtag, String caption_tags) {

                    Bundle bundle = new Bundle();
                    bundle.putString("hashTagId", caption_tags);
                    HashTagFragment newFragment = new HashTagFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutHashTags, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();


                }
            });


            postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                @Override
                public void onClicked(String username, int caption_tags) {

                    if (!appSettings.getUserName().equalsIgnoreCase(username))
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("username", username);
                        OthersProfileFragment newFragment = new OthersProfileFragment();
                        newFragment.setArguments(bundle);
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace whatever is in the fragment_container view with this fragment,
                        // and add the transaction to the back stack so the user can navigate back
                        transaction.replace(R.id.parentLayoutHashTags, newFragment);

                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    }  else {
                        moveToOurProfile();
                    }
                }
            });


            postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                @Override
                public void onClicked(String username) {

                    if (!appSettings.getUserName().equalsIgnoreCase(username))
                    {
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username);
                    OthersProfileFragment newFragment = new OthersProfileFragment();
                    newFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.parentLayoutHashTags, newFragment);

                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();
                    }  else {
                        moveToOurProfile();
                    }
                }
            });
        } else {

            contentRecyclerView.setVisibility(View.GONE);
            emptyTag.setVisibility(View.GONE);
            emptyPost.setVisibility(View.VISIBLE);

        }
    }


    @Subscribe
    public void onBookmarked(onBookmarked onBookmarked) {
        postFeedAdapter.changeBookmarkStatus(onBookmarked.getBookMarkStatus(), onBookmarked.getPosition());
    }


    @Subscribe
    public void PostLiked(PostLiked postLiked) {
        postFeedAdapter.changeLikeStatus(postLiked.getLikeStatus(), postLiked.getPosition());
    }

    @Subscribe
    public void unFollow(unFollow unFollow) {
        try {
            getHashTagDetails();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private JSONArray validateJsonArray(JSONArray myPosts) {
        JSONArray newArray = new JSONArray();
        for (int i = 0; i < myPosts.length(); i++) {
            if (myPosts.optJSONObject(i).optJSONArray("linkData").length() != 0) {
                newArray.put(myPosts.optJSONObject(i));
            }
        }
        return newArray;
    }

    private void getNextData() throws JSONException {
        currentNormalpageNumber = currentNormalpageNumber + 1;
        if (currentNormalpageNumber <= totalNormalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentNormalpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.HASH_TAG_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalNormalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("myposts");
                        duy = validateJsonArray(duy);
                        for (int i = 0; i < duy.length(); i++) {
                            constantArray.put(duy.optJSONObject(i));
                        }
                        if (selectedLayout == ConstantKeys.GRID_VIEW) {
                            gridViewAdapter.addValues(duy);
                        } else {
                            postFeedAdapter.addValues(duy);
                        }

                    }

                }
            });
        }

    }


    private void initViews() {
        hashTagImage = view.findViewById(R.id.hashTagImage);
//        userImageTop = view.findViewById(R.id.userImageTop);
        hashTagName = view.findViewById(R.id.hashTagName);
        hashTagNameTop = view.findViewById(R.id.hashTagNameTop);
        postsCount = view.findViewById(R.id.postsCount);
        listView = view.findViewById(R.id.listView);
        refresh_layout = view.findViewById(R.id.refresh_layout);
        gridView = view.findViewById(R.id.gridView);
        listViewSelected = view.findViewById(R.id.listViewSelected);
        gridViewSelected = view.findViewById(R.id.gridViewSelected);
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        emptyPost = view.findViewById(R.id.emptyPost);
        emptyTag = view.findViewById(R.id.emptyTag);
        suggestionHashTags = view.findViewById(R.id.suggestionHashTags);
        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


    }


    @Override
    public void onClick(View v) {
        if (v == listView) {
            setListSelected();
            setListValues();

        } else if (v == gridView) {
            setGridSelected();
            setGridValues();
        }
    }


    private void setGridSelected() {
        selectedLayout = ConstantKeys.GRID_VIEW;
        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.VISIBLE);


    }

    private void setListSelected() {
        selectedLayout = ConstantKeys.LIST_VIEW;
        listViewSelected.setVisibility(View.VISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }

    private void setTaggedView() {
        selectedLayout = ConstantKeys.PINNED_VIEW;

        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }


    private void setBookmarkedSelected() {
        selectedLayout = ConstantKeys.BOOKMARKED_VIEW;
        listViewSelected.setVisibility(View.INVISIBLE);
        gridViewSelected.setVisibility(View.INVISIBLE);
    }
}
