package app.com.snipofeed.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.Activity.FullCommentActivity;
import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.onPostImageClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;


public class NotificationFragment extends Fragment implements onProfileImageClicked, onPostImageClicked {

    ViewPager pager;
    private String TAG = NotificationFragment.class.getSimpleName();

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_notification, container, false);

//        ( (MainActivity)getActivity()).updateToolbarTitle("Share");


        initViews(view);
        return view;
    }

    private void initViews(View view) {

        pager = view.findViewById(R.id.viewpager);


        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new FollowingFragment(ConstantKeys.isTop, this, this), getResources().getString(R.string.following));
        adapter.addFragment(new YouFragment(ConstantKeys.isPeople, this, this), getResources().getString(R.string.you));

        pager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) view.findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    //selectedPosition=ConstantKeys.isTop;
                } else if (position == 1) {
                    //selectedPosition=ConstantKeys.isPeople;
                } else if (position == 2) {
                    //selectedPosition=ConstantKeys.isHashtags;
                } else {
                    //selectedPosition=ConstantKeys.isPlaces;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    public void onClicked(String username) {
        placeOthersProfileFragment(username);
    }

    @Override
    public void onClicked(JSONObject jsonObject) {
        Log.d(TAG, "onClicked: " + jsonObject);
        if (jsonObject.optString("notification_type").equalsIgnoreCase("followers_followers")) {
            placeOthersProfileFragment(jsonObject.optString("notification_3"));
        }  else if (jsonObject.optString("notification_type").equalsIgnoreCase("follow"))
        {
            placeOthersProfileFragment(jsonObject.optString("name"));

        }else if (jsonObject.optString("notification_type").equalsIgnoreCase("followers_like")||jsonObject.optString("notification_type").equalsIgnoreCase("like"))
        {
            if (jsonObject.has("data")) {
                if (jsonObject.optJSONArray("data").length() == 1) {
                    placePostDetails(jsonObject.optJSONArray("data").optJSONObject(0).optString("postId"));
                } else {
                    placePostDetails(jsonObject);
                }
            } else {
                placePostDetails(jsonObject.optString("postId"));

            }
        } else if (jsonObject.optString("notification_type").equalsIgnoreCase("followers_comment")||jsonObject.optString("notification_type").equalsIgnoreCase("comment"))
        {
            placePostComments(jsonObject.optString("postId"));
        }
    }

    private void placePostDetails(JSONObject jsonObject) {
        Bundle bundle = new Bundle();
        bundle.putString("singlePost", "multiple");
        bundle.putString("postDetails", String.valueOf(jsonObject.optJSONArray("data")));
        PostDetailsFragment newFragment = new PostDetailsFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.parentLayoutnotification, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void placePostComments(String id) {
        Intent intent = new Intent(getActivity(), FullCommentActivity.class);
        intent.putExtra("postId", id);
        getActivity().startActivity(intent);
    }

    private void placePostDetails(String id) {
        Bundle bundle = new Bundle();
        bundle.putString("singlePost", "true");
        bundle.putString("postId", id);
        PostDetailsFragment newFragment = new PostDetailsFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.parentLayoutnotification, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void placeOthersProfileFragment(String username) {
        AppSettings appSettings = new AppSettings(getActivity());
        if (!username.equalsIgnoreCase(appSettings.getUserName())) {
            Bundle bundle = new Bundle();
            bundle.putString("username", username);
            OthersProfileFragment newFragment = new OthersProfileFragment();
            newFragment.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.parentLayoutnotification, newFragment);

            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();
        } else {
            MainActivity.moveToOurProfile();
        }
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
