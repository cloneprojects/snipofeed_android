package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.SearchListAdapter;
import app.com.snipofeed.Events.onSearchStarted;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.Helper.onSearchPeopleClicked;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlacesFragment extends Fragment {
    public RecyclerView contentRecyclerView;
    onSearchPeopleClicked onSearchPeopleClicked;
    View view;
    private String TAG = PlacesFragment.class.getSimpleName();


    @SuppressLint("ValidFragment")
    public PlacesFragment(onSearchPeopleClicked onSearchPeopleClicked) {
        this.onSearchPeopleClicked=onSearchPeopleClicked;


    }

    public PlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        initPlaces();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void searchStarted(onSearchStarted onSearchStarted) {
        Log.d(TAG, "searchStarted: ");

        initPlaces();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_places, container, false);

//            initPlaces();

        return view;


    }


    public void initPlaces() {
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);

        if (FulSearchFragment.searchText.getText().toString().length() > 0) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("address", FulSearchFragment.searchText.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.SEARCH_PLACES, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = response.optJSONArray("places");
                    SearchListAdapter searchListAdapter = new SearchListAdapter(getActivity(), jsonArray, ConstantKeys.isPlaces);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    contentRecyclerView.setLayoutManager(linearLayoutManager);
                    contentRecyclerView.setAdapter(searchListAdapter);
                    searchListAdapter.notifyDataSetChanged();
                    searchListAdapter.setClickListeners(new onRecyclerViewItemClicked() {
                        @Override
                        public void onClicked(String city) {

                            onSearchPeopleClicked.onClicked(city);


                        }

                        @Override
                        public void onClicked(String city, JSONObject jsonObject) {


                        }
                    });


                }
            });
        }
    }

}

