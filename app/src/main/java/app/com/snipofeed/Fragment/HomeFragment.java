package app.com.snipofeed.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;


public class HomeFragment extends Fragment {


    View view;
    RecyclerRefreshLayout refresh_layout;
    RecyclerView postFeedRecyclerView;
    PostFeedAdapter postFeedAdapter;
    AppSettings appSettings;
    onBottomReachedListner onBottomReachedListner;
    TextView appName;
    ImageView messageIcon, cameraButton;

    JSONArray newsPosts = new JSONArray();
    int currentpageNumber = 1;
    LinearLayout emptyHome;
    int totalpageNumber = 1;
    private String TAG = HomeFragment.class.getSimpleName();


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    public static float dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return dpValue * scale;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void setTextValue(String username, String text, TextView textView) {
        SpannableString ss = new SpannableString(text);
        SpannableString ss_caption = new SpannableString(username);

        ss_caption.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_black)), 0, username.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        // Splitting the words by spaces
        String[] words = text.split(" ");
        Log.d(TAG, "setTextValue: " + words.length);
        for (String word : words) {
            Log.d("test", word);
            word = word.replace("\n", "");
            Log.d(TAG, "setTextValue: " + word);
            if (word.startsWith("@")) {
                // word is a mention
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {

                        TextView tv = (TextView) widget;
                        // TODO add check if tv.getText() instanceof Spanned
                        Spanned s = (Spanned) tv.getText();
                        int start = s.getSpanStart(this);
                        int end = s.getSpanEnd(this);
//                        Log.d(TAG, "onClick [" + s.subSequence(start, end) + "]");

//                        moveToMentionPage(s.subSequence(start,end));

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Mention matched" + startIndex + " : " + endIndex);
            } else if (word.startsWith("#")) {
                // word is a hash tag


                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        TextView tv = (TextView) widget;
                        // TODO add check if tv.getText() instanceof Spanned
                        Spanned s = (Spanned) tv.getText();
                        int start = s.getSpanStart(this);
                        int end = s.getSpanEnd(this);
//                        Log.d(TAG, "onClick [" + s.subSequence(start, end) + "]");

//                        moveToHashPage(s.subSequence(start,end));
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };
                int startIndex = text.indexOf(word);
                int endIndex = startIndex + word.length();
                Log.d(TAG, "checkColour: " + word + "/" + startIndex + "/" + endIndex);
                ForegroundColorSpan mentionColorSpan = new ForegroundColorSpan(Color.parseColor("#0077B5"));
                ss.setSpan(clickableSpan, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(mentionColorSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d("test", "Hash tag matched" + startIndex + " : " + endIndex);
            }
//            Log.d(TAG, "setTextValue: Finsied");
        }


        textView.setText("");
        textView.append(ss_caption);
        textView.append(" ");
        textView.append(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_home, container, false);
        appSettings = new AppSettings(getActivity());
        initViews();
//        TextView textView = view.findViewById(R.id.text);
//        setTextValue("Yuvaraj", "#check now #poyya #mass #sandwhich #naturalwonder  ", textView);


        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postFeedRecyclerView = view.findViewById(R.id.postFeedRecyclerView);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);
        return view;
    }

    private void getInitialData() throws JSONException {
        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        ApiCall.PostMethod(getActivity(), UrlHelper.ALL_FEED, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(final JSONObject response) {
                try {
                    refresh_layout.setRefreshing(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (response.has("NewsPosts")) {
                    newsPosts = response.optJSONArray("NewsPosts");

                    emptyHome.setVisibility(View.GONE);
                    postFeedRecyclerView.setVisibility(View.VISIBLE);
                    try {
                        totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (newsPosts.length() > 0) {
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                        postFeedAdapter = new PostFeedAdapter(getActivity(), newsPosts, getActivity().getSupportFragmentManager());
                        postFeedRecyclerView.setAdapter(postFeedAdapter);
                        postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                            @Override
                            public void onBottomReached(int position) {

                                try {
                                    getNextData();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });


                        postFeedAdapter.setOnHashTagClickListener(new onHashTagClicked() {
                            @Override
                            public void onClicked(String hashtag, String caption_tags) {


                                Bundle bundle = new Bundle();
                                bundle.putString("hashTagId", caption_tags);
                                HashTagFragment newFragment = new HashTagFragment();
                                newFragment.setArguments(bundle);
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                // Replace whatever is in the fragment_container view with this fragment,
                                // and add the transaction to the back stack so the user can navigate back
                                transaction.replace(R.id.parentLayoutHome, newFragment);

                                transaction.addToBackStack(null);
                                // Commit the transaction
                                transaction.commit();
                            }
                        });


                        postFeedAdapter.setOnMentionClicked(new onMentionClicked() {
                            @Override
                            public void onClicked(String username, int caption_tags) {

                                if (!appSettings.getUserName().equalsIgnoreCase(username.trim())) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", username);
                                    OthersProfileFragment newFragment = new OthersProfileFragment();
                                    newFragment.setArguments(bundle);
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack so the user can navigate back
                                    transaction.replace(R.id.parentLayoutHome, newFragment);

                                    transaction.addToBackStack(null);
                                    // Commit the transaction
                                    transaction.commit();
                                } else {
                                    MainActivity.moveToOurProfile();
                                }
                            }
                        });

                        postFeedAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                            @Override
                            public void onClicked(String username) {
                                if (!appSettings.getUserName().equalsIgnoreCase(username)) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("username", username);
                                    OthersProfileFragment newFragment = new OthersProfileFragment();
                                    newFragment.setArguments(bundle);
                                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    // Replace whatever is in the fragment_container view with this fragment,
                                    // and add the transaction to the back stack so the user can navigate back
                                    transaction.replace(R.id.parentLayoutHome, newFragment);

                                    transaction.addToBackStack(null);
                                    // Commit the transaction
                                    transaction.commit();
                                } else {
                                    MainActivity.moveToOurProfile();
                                }
                            }
                        });
                    }
                } else {
                    emptyHome.setVisibility(View.VISIBLE);
                    postFeedRecyclerView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Subscribe
    public void onBookmarked(onBookmarked onBookmarked) {
        postFeedAdapter.changeBookmarkStatus(onBookmarked.getBookMarkStatus(), onBookmarked.getPosition());
    }


    @Subscribe
    public void PostLiked(PostLiked postLiked) {
        postFeedAdapter.changeLikeStatus(postLiked.getLikeStatus(), postLiked.getPosition());
    }

    @Subscribe
    public void unFollow(unFollow unFollow) {
        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getNextData() throws JSONException {
        currentpageNumber = currentpageNumber + 1;
        if (currentpageNumber <= totalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentpageNumber);


            ApiCall.PostMethod(getActivity(), UrlHelper.ALL_FEED, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("NewsPosts");
                        postFeedAdapter.addValues(duy);

                    }


                }
            });
        }

    }

    private void initViews() {
        cameraButton = view.findViewById(R.id.cameraButton);
        appName = view.findViewById(R.id.appName);
        messageIcon = view.findViewById(R.id.messageIcon);
        emptyHome = view.findViewById(R.id.emptyHome);


        cameraButton.setTranslationY(-150);
        appName.setTranslationY(-150);
        messageIcon.setTranslationY(-150);


//        cameraButton.animate()
//                .translationY(0)
//                .setDuration(200)
//                .setStartDelay(200);
        appName.animate()
                .translationY(0)
                .setDuration(200)
                .setStartDelay(400);
//        messageIcon.animate()
//                .translationY(0)
//                .setDuration(200)
//                .setStartDelay(600)
//                .start();

        refresh_layout = view.findViewById(R.id.refresh_layout);
        refresh_layout.setRefreshInitialOffset(80);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getInitialData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
