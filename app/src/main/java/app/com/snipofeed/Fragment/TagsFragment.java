package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.SearchListAdapter;
import app.com.snipofeed.Events.onSearchStarted;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagsFragment extends Fragment {
    public RecyclerView contentRecyclerView;

    View view;
    onHashTagClicked onHashTagClicked;
    private String TAG=TagsFragment.class.getSimpleName();


    @SuppressLint("ValidFragment")
    public TagsFragment(onHashTagClicked onHashTagClicked) {
        this.onHashTagClicked =onHashTagClicked;

    }

    public TagsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tags, container, false);

//        initTags();

        return view;

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        initTags();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void searchStarted(onSearchStarted onSearchStarted) {
        Log.d(TAG, "searchStarted: ");

            initTags();


    }

    public void initTags() {
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        JSONObject jsonObject = new JSONObject();
        AppSettings appSettings = new AppSettings(getActivity());
        try {
            jsonObject.put("tagName", FulSearchFragment.searchText.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.SEARCH_HASHTAG, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray jsonArray = new JSONArray();
                jsonArray = response.optJSONArray("hashTags");
                SearchListAdapter searchListAdapter = new SearchListAdapter(getActivity(), jsonArray, ConstantKeys.isHashtags);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                contentRecyclerView.setLayoutManager(linearLayoutManager);
                contentRecyclerView.setAdapter(searchListAdapter);
                searchListAdapter.notifyDataSetChanged();
                searchListAdapter.setClickListeners(new onRecyclerViewItemClicked() {
                    @Override
                    public void onClicked(String username) {
                        onHashTagClicked.onClicked(username,"");
                    }

                    @Override
                    public void onClicked(String username, JSONObject jsonObject) {

                    }
                });




            }
        });
    }

}
