package app.com.snipofeed.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import app.com.snipofeed.Events.onSearchStarted;
import app.com.snipofeed.Helper.ConstantKeys;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onSearchPeopleClicked;
import app.com.snipofeed.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FulSearchFragment extends Fragment implements onSearchPeopleClicked,onHashTagClicked {
    public static EditText searchText;
    View view;
    int selectedPosition = ConstantKeys.isTop;
    private ViewPager viewPager;
    private String TAG=FulSearchFragment.class.getSimpleName();


    public FulSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
//        initViews();
        Log.d(TAG, "onResume: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ful_search, container, false);
        initViews();
        return view;
    }




    private void initViews() {
        viewPager = view.findViewById(R.id.viewpager);
        searchText = view.findViewById(R.id.searchText);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new TopFragment(this), getResources().getString(R.string.top));
        adapter.addFragment(new PeopleFragment(this), getResources().getString(R.string.people));
        adapter.addFragment(new TagsFragment(this), getResources().getString(R.string.tags));
        adapter.addFragment(new PlacesFragment(new onSearchPeopleClicked() {
            @Override
            public void onClicked(String city) {
                String s[]=city.split("/");

                placePlacesDetails(s[0],s[1]);
            }
        }), getResources().getString(R.string.places));
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);

        NavigationTabStrip tabStrip = (NavigationTabStrip)view.findViewById(R.id.tabStrip);
        String[] titles = new String[]{getResources().getString(R.string.top), getResources().getString(R.string.people), getResources().getString(R.string.tags),getResources().getString(R.string.places)};
        tabStrip.setTitles(titles);
        tabStrip.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    selectedPosition = ConstantKeys.isTop;
                } else if (position == 1) {
                    selectedPosition = ConstantKeys.isPeople;
                } else if (position == 2) {
                    selectedPosition = ConstantKeys.isHashtags;
                } else {
                    selectedPosition = ConstantKeys.isPlaces;
                }
                EventBus.getDefault().post(new onSearchStarted(selectedPosition));


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "onTextChanged: "+s);
                EventBus.getDefault().post(new onSearchStarted(selectedPosition));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void placePlacesDetails(String s, String s1) {

        Bundle bundle = new Bundle();
        bundle.putString("area", s);
        bundle.putString("city", s1);
        PlacesDetailsFragment newFragment = new PlacesDetailsFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.parentLayoutFullSearch, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    @Override
    public void onClicked(String userName) {

        Bundle bundle = new Bundle();
        bundle.putString("username", userName);
        OthersProfileFragment newFragment = new OthersProfileFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.parentLayoutFullSearch, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();


    }

    @Override
    public void onClicked(String hashtag, String caption_tags) {


        Bundle bundle = new Bundle();
        bundle.putString("hashTagId", hashtag);
        HashTagFragment newFragment = new HashTagFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.parentLayoutFullSearch, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "getItem: "+position);
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);

        }
    }


}
