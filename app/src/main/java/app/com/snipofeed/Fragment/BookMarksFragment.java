package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.GridViewAdapter;
import app.com.snipofeed.Helper.CustomGridLayoutManager;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onRecyclerViewItemClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

import static app.com.snipofeed.Fragment.HomeFragment.dip2px;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookMarksFragment extends Fragment {
    View view;
    private JSONArray taggedArray=new JSONArray();
    private GridViewAdapter gridViewAdapter;
    RecyclerView contentRecyclerView;
    private int currentTagpageNumber=0;
    private int totalTagpageNumber;
    RecyclerRefreshLayout refresh_layout;

    AppSettings appSettings;



    public BookMarksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_book_marks, container, false);
        appSettings=new AppSettings(getActivity());
        initViews();
        return view;
    }

    private void initViews() {
        contentRecyclerView=view.findViewById(R.id.contentRecyclerView);
        try {
            getTaggedPosts();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             getActivity().onBackPressed();
            }
        });



        refresh_layout = view.findViewById(R.id.refresh_layout);
        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getTaggedPosts();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);



    }


    private void setTaggedGridValues() {

//        GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
        contentRecyclerView.setLayoutManager(customGridLayoutManager);

        if (taggedArray.length()>0) {
            gridViewAdapter = new GridViewAdapter(getActivity(), taggedArray, refresh_layout);
            contentRecyclerView.setAdapter(gridViewAdapter);
            gridViewAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                @Override
                public void onBottomReached(int position) {

                    try {
                        getNextTaggedData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        gridViewAdapter.setOnRecyclerViewItemClicked(new onRecyclerViewItemClicked() {
            @Override
            public void onClicked(String username) {

            }

            @Override
            public void onClicked(String username, JSONObject jsonObject) {
                Bundle bundle=new Bundle();
                bundle.putString("singlePost","false");
                bundle.putString("postDetails",jsonObject.toString());
                PostDetailsFragment newFragment = new PostDetailsFragment();
                newFragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.parentLayoutBookMark, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });



        contentRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                int position = parent.getChildAdapterPosition(view); // item position
                int spanCount = 3;
                int spacing = 5;//spacing between views in grid

                if (position >= 0) {
                    int column = position % spanCount; // item column

                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                    if (position < spanCount) { // top edge
                        outRect.top = spacing;
                    }
                    outRect.bottom = spacing; // item bottom
                } else {
                    outRect.left = 0;
                    outRect.right = 0;
                    outRect.top = 0;
                    outRect.bottom = 0;
                }
            }
        });

    }


    private void getTaggedPosts() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("page", "1");
        ApiCall.PostMethod(getActivity(), UrlHelper.BOOKMARKED_POSTS, jsonObject, new VolleyCallback() {
            @SuppressLint({"CheckResult", "SetTextI18n"})
            @Override
            public void onSuccess(JSONObject response) {
                if (response.has("pinnedPosts")) {
                    taggedArray = response.optJSONArray("pinnedPosts");
                    setTaggedGridValues();
                }
            }
        });

    }



    private void getNextTaggedData() throws JSONException {

        currentTagpageNumber = currentTagpageNumber + 1;
        if (currentTagpageNumber <= totalTagpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentTagpageNumber);

            ApiCall.PostMethodNoProgress(getActivity(), UrlHelper.BOOKMARKED_POSTS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalTagpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("pinnedPosts");
                        duy = validateJsonArray(duy);
                        for (int i = 0; i < duy.length(); i++) {
                            taggedArray.put(duy.optJSONObject(i));
                        }

                        gridViewAdapter.addValues(duy);


                    }

                }
            });
        }
    }



    private JSONArray validateJsonArray(JSONArray myPosts) {
        JSONArray newArray = new JSONArray();
        for (int i = 0; i < myPosts.length(); i++) {
            if (myPosts.optJSONObject(i).optJSONArray("linkData").length() != 0) {
                newArray.put(myPosts.optJSONObject(i));
            }
        }
        return newArray;
    }
}
