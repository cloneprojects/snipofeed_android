package app.com.snipofeed.Fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Adapters.NotificationFollowingAdapter;
import app.com.snipofeed.Helper.HeaderView;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onPostImageClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

import static app.com.snipofeed.Fragment.HomeFragment.dip2px;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowingFragment extends Fragment {
    public static RecyclerView contentRecyclerView;
    int type;
    View view;
    JSONArray mainArray = new JSONArray();
    AppSettings appSettings;
    int currentpageNumber = 1;
    RecyclerRefreshLayout refresh_layout;
    int totalpageNumber = 1;
    onProfileImageClicked onProfileImageClicked;
    onPostImageClicked onPostImageClicked;
    private NotificationFollowingAdapter notificationAdapter;


    @SuppressLint("ValidFragment")
    public FollowingFragment(int type, onProfileImageClicked onProfileImageClicked,onPostImageClicked onPostImageClicked) {
        this.type = type;
        this.onProfileImageClicked = onProfileImageClicked;
        this.onPostImageClicked = onPostImageClicked;

    }

    public FollowingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_following, container, false);
        appSettings = new AppSettings(getActivity());
        initViews();
        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }


    private void initViews() {
        contentRecyclerView = view.findViewById(R.id.contentRecyclerView);
        refresh_layout = view.findViewById(R.id.refresh_layout);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));
        refresh_layout.setRefreshView(new HeaderView(getActivity()), layoutParams);

        refresh_layout.setRefreshInitialOffset(50);
        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    getInitialData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    private void getInitialData() throws JSONException {
        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        ApiCall.PostMethod(getActivity(), UrlHelper.FOLLOWING_NOTIFICATION, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                try {
                    refresh_layout.setRefreshing(false);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

                if (response.has("followNotify")) {
                    mainArray = response.optJSONArray("followNotify");
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

                    contentRecyclerView.setLayoutManager(linearLayoutManager);


                    notificationAdapter = new NotificationFollowingAdapter(getActivity(), mainArray);
                    contentRecyclerView.setLayoutManager(linearLayoutManager);
                    contentRecyclerView.setAdapter(notificationAdapter);


                    notificationAdapter.setOnProfileImageClicked(new onProfileImageClicked() {
                        @Override
                        public void onClicked(String username) {
                            onProfileImageClicked.onClicked(username);

                        }
                    });


                    notificationAdapter.setOnPostImageClicked(new onPostImageClicked() {
                        @Override
                        public void onClicked(JSONObject jsonObject) {
                            onPostImageClicked.onClicked(jsonObject);
                        }
                    });
                }


            }
        });
    }


    private void getNextData() throws JSONException {
        currentpageNumber = currentpageNumber + 1;
        if (currentpageNumber <= totalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("page", "" + currentpageNumber);


            ApiCall.PostMethod(getActivity(), UrlHelper.ALL_FEED, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("NewsPosts");
                        notificationAdapter.addValues(duy);

                    }


                }
            });
        }

    }


}
