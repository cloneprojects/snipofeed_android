package app.com.snipofeed.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.com.snipofeed.Activity.MainActivity;
import app.com.snipofeed.Adapters.PostFeedAdapter;
import app.com.snipofeed.Events.PostLiked;
import app.com.snipofeed.Events.onBookmarked;
import app.com.snipofeed.Events.unFollow;
import app.com.snipofeed.Helper.UrlHelper;
import app.com.snipofeed.Helper.Utils;
import app.com.snipofeed.Helper.onBottomReachedListner;
import app.com.snipofeed.Helper.onHashTagClicked;
import app.com.snipofeed.Helper.onMentionClicked;
import app.com.snipofeed.Helper.onProfileImageClicked;
import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;
import app.com.snipofeed.Volley.VolleyCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostDetailsFragment extends Fragment implements onHashTagClicked, onMentionClicked, onProfileImageClicked {
    View view;
    RecyclerRefreshLayout refresh_layout;
    RecyclerView postFeedRecyclerView;
    PostFeedAdapter postFeedAdapter;
    AppSettings appSettings;

    onBottomReachedListner onBottomReachedListner;

    JSONArray newsPosts = new JSONArray();
    int currentpageNumber = 1;
    int totalpageNumber = 1;
    private Bundle bundle;

    public PostDetailsFragment() {
        // Required empty public constructor
    }

    public static float dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return dpValue * scale;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void initViews() {

        appSettings = new AppSettings(getActivity());

        postFeedRecyclerView = view.findViewById(R.id.postFeedRecyclerView);

        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                (int) dip2px(getActivity(), 120), (int) dip2px(getActivity(), 120));

        bundle = getArguments();

        if (bundle.getString("singlePost").equalsIgnoreCase("multiple")) {
            JSONArray newArray = new JSONArray();

            try {
                JSONArray jsonArray = new JSONArray(bundle.getString("postDetails"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id", jsonArray.optJSONObject(i).optString("postId"));
                    newArray.put(jsonObject);
                }

                getNewArray(newArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
        if (bundle.getString("singlePost").equalsIgnoreCase("true")) {

            try {
                getInitialData();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {


            try {
                JSONObject jsonObject = new JSONObject(bundle.getString("postDetails"));
                newsPosts.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
            postFeedAdapter = new PostFeedAdapter(getActivity(), newsPosts, getActivity().getSupportFragmentManager());
            postFeedRecyclerView.setAdapter(postFeedAdapter);
            setListners();
        }


        //        refresh_layout = view.findViewById(R.id.refresh_layout);
//        refresh_layout.setRefreshInitialOffset(50);
//        refresh_layout.setRefreshStyle(RecyclerRefreshLayout.RefreshStyle.PINNED);
//        refresh_layout.setOnRefreshListener(new RecyclerRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                try {
//                    getInitialData();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        view.findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


    }

    private void getNewArray(final JSONArray newArray) throws JSONException {

        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", newArray.toString());
        ApiCall.PostMethod(getActivity(), UrlHelper.LIST_FFEDS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
//                try {
//                    refresh_layout.setRefreshing(false);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
                newsPosts = response.optJSONArray("NewsPosts");

//                try {
//                    totalpageNumber = Integer.parseInt(response.optString("pageCount"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                postFeedAdapter = new PostFeedAdapter(getActivity(), newsPosts, getActivity().getSupportFragmentManager());
                postFeedRecyclerView.setAdapter(postFeedAdapter);
                postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                    @Override
                    public void onBottomReached(int position) {

                        try {
                            getNextData(newArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                setListners();
            }
        });

    }


    private void getInitialData() throws JSONException {
        currentpageNumber = 1;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", appSettings.getAccessToken());
        jsonObject.put("postId", bundle.getString("postId"));
        ApiCall.PostMethod(getActivity(), UrlHelper.VIEW_POST_DETAILS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
//                try {
//                    refresh_layout.setRefreshing(false);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
                newsPosts = response.optJSONArray("Postdetails");

//                try {
//                    totalpageNumber = Integer.parseInt(response.optString("pageCount"));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                postFeedRecyclerView.setLayoutManager(linearLayoutManager);
//                postFeedRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
                postFeedAdapter = new PostFeedAdapter(getActivity(), newsPosts, getActivity().getSupportFragmentManager());
                postFeedRecyclerView.setAdapter(postFeedAdapter);
                postFeedAdapter.setOnBottomReachedListener(new onBottomReachedListner() {
                    @Override
                    public void onBottomReached(int position) {

//                        try {
//                            getNextData();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                });

                setListners();
            }
        });
    }

    public void setListners() {
        postFeedAdapter.setOnMentionClicked(PostDetailsFragment.this);
        postFeedAdapter.setOnProfileImageClicked(PostDetailsFragment.this);
        postFeedAdapter.setOnHashTagClickListener(PostDetailsFragment.this);

    }

    @Subscribe
    public void onBookmarked(onBookmarked onBookmarked) {
        postFeedAdapter.changeBookmarkStatus(onBookmarked.getBookMarkStatus(), onBookmarked.getPosition());
    }


    @Subscribe
    public void PostLiked(PostLiked postLiked) {
        postFeedAdapter.changeLikeStatus(postLiked.getLikeStatus(), postLiked.getPosition());
    }

    @Subscribe
    public void unFollow(unFollow unFollow) {
        try {
            getInitialData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_post_details, container, false);
        initViews();

        return view;
    }

    @Override
    public void onClicked(String hashtag, String caption_tags) {


        Bundle bundle = new Bundle();
        bundle.putString("hashTagId", caption_tags);
        HashTagFragment newFragment = new HashTagFragment();
        newFragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.parentLayoutPostDetails, newFragment);

        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }


    private void getNextData(JSONArray newArray) throws JSONException {
        currentpageNumber = currentpageNumber + 1;
        if (currentpageNumber <= totalpageNumber) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accessToken", appSettings.getAccessToken());
            jsonObject.put("postId", newArray.toString());
            jsonObject.put("page", "" + currentpageNumber);
            ApiCall.PostMethod(getActivity(), UrlHelper.LIST_FFEDS, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {

                    Log.d("", "onSuccess: " + response);

                    if (response.optString("error").equalsIgnoreCase("true")) {
                        Utils.toast(getActivity(), response.optString("message"));
                    } else {
                        try {
                            totalpageNumber = Integer.parseInt(response.optString("pageCount"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        JSONArray duy = response.optJSONArray("NewsPosts");
                        postFeedAdapter.addValues(duy);

                    }


                }
            });
        }

    }


    @Override
    public void onClicked(String mentions, int caption_tags) {

        if (!mentions.equalsIgnoreCase(appSettings.getUserName())) {
            Bundle bundle = new Bundle();
            bundle.putString("username", mentions);
            OthersProfileFragment newFragment = new OthersProfileFragment();
            newFragment.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.parentLayoutPostDetails, newFragment);

            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();
        } else {
            MainActivity.moveToOurProfile();
        }

    }

    @Override
    public void onClicked(String username) {
        if (!username.equalsIgnoreCase(appSettings.getUserName())) {
            Bundle bundle = new Bundle();
            bundle.putString("username", username);
            OthersProfileFragment newFragment = new OthersProfileFragment();
            newFragment.setArguments(bundle);
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.parentLayoutPostDetails, newFragment);

            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();
        } else {
            MainActivity.moveToOurProfile();
        }

    }
}
