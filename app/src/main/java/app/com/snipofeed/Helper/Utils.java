package app.com.snipofeed.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import app.com.snipofeed.PrefHandler.AppSettings;
import app.com.snipofeed.PrefHandler.SharedPreference;
import app.com.snipofeed.R;
import app.com.snipofeed.Volley.ApiCall;

/**
 * Created by yuvaraj on 25/04/18.
 */

public class Utils {
    public static Boolean isShowing = false;
    private static CustomDialog customDialog;
    private static String TAG = Utils.class.getSimpleName();


    public static float dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return dpValue * scale;
    }

    public static void setStatusBarColor(Activity activity)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = activity.getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

    }


    public static Bitmap getBitmapFrompath(String path)
    {

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inDither = true;

        Bitmap bitmap=BitmapFactory.decodeFile(path,bmOptions);

//        int h = (int) (bitmap.getWidth()/1.5);
//        int w = (int) (bitmap.getHeight()/1.5);
//
//        Log.d(TAG, "getBitmapFrompath: "+bitmap.getWidth()+"////"+bitmap.getHeight());
//        Log.d(TAG, "getBitmapFrompath: "+h+"////"+w);
//
//
//        return Bitmap.createScaledBitmap(bitmap, w, h, true);

        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        return newBitmap;
    }
    public static String getStyledStaticMap(String latitude,String longitude,Context context)
    {
        String value="https://maps.googleapis.com/maps/api/staticmap?size=1024x1024&zoom=12&format=png&sensor=false&maptype=roadmap&center="+latitude + "," + longitude +"&style=feature%3Aadministrative%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Alandscape%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Alandscape.man_made%7Celement%3Aall%7Cvisibility%3Aoff%7C&style=feature%3Apoi%7Celement%3Aall%7Cvisibility%3Aoff%7C"+
                "&key=" + context.getResources().getString(R.string.map_key);;
        Log.d(TAG, "getStyledStaticMap: "+value);
        return value;
    }


    public static boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    public static String getStaticMapURL(double latitude, double longitude, int radiusMeters, Context context, int width, int height)
    {
        String pathString = "";
        if (radiusMeters > 0)
        {
            // Add radius path
            ArrayList<LatLng> circlePoints = getCircleAsPolyline(latitude,longitude, radiusMeters);

            if (circlePoints.size() > 0)
            {
                String encodedPathLocations = PolyUtil.encode(circlePoints);
                pathString = "&path=color:0x0000ff25%7Cweight:1%7Cfillcolor:0x0000ff25%7Cenc:" + encodedPathLocations;
            }
        }

        String staticMapURL = "https://maps.googleapis.com/maps/api/staticmap?size=+"+1024+"x"+1024+"&maptype=roadmap&style=feature%3Aadministrative%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Alandscape%7Celement%3Aall%7Cvisibility%3Aon%7C&style=feature%3Alandscape.man_made%7Celement%3Aall%7Cvisibility%3Aoff%7C&style=feature%3Apoi%7Celement%3Aall%7Cvisibility%3Aoff%7C&center=" +
                latitude + "," + longitude +
                pathString +
                "&key=" + context.getResources().getString(R.string.map_key);

        return staticMapURL;
    }

    private static ArrayList<LatLng> getCircleAsPolyline(double latitudes,double longitudes, int radiusMeters)
    {
        ArrayList<LatLng> path = new ArrayList<>();

        double latitudeRadians = latitudes * Math.PI / 180.0;
        double longitudeRadians = longitudes * Math.PI / 180.0;
        double radiusRadians = radiusMeters / 1000.0 / 6371;

        double calcLatPrefix = Math.sin(latitudeRadians) * Math.cos(radiusRadians);
        double calcLatSuffix = Math.cos(latitudeRadians) * Math.sin(radiusRadians);

        for (int angle = 0; angle < 361; angle += 10)
        {
            double angleRadians = angle * Math.PI / 180.0;

            double latitude = Math.asin(calcLatPrefix + calcLatSuffix * Math.cos(angleRadians));
            double longitude = ((longitudeRadians + Math.atan2(Math.sin(angleRadians) * Math.sin(radiusRadians) * Math.cos(latitudeRadians), Math.cos(radiusRadians) - Math.sin(latitudeRadians) * Math.sin(latitude))) * 180) / Math.PI;
            latitude = latitude * 180.0 / Math.PI;

            path.add(new LatLng(latitude, longitude));
        }

        return path;
    }




    public static String getDateFormatted(String serverTime) {

        String date = "";
        DateTime postDateTime = new DateTime(serverTime);

        DateTime currentDateTime = new DateTime();


        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
            String dates = postDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");
            String currentDates = currentDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");

            Date date1 = simpleDateFormat.parse(dates);
            Date date2 = simpleDateFormat.parse(currentDates);
            if (date1.getTime() - date2.getTime() == 0) {
                date = " a moments ago";
            } else {
                Period period = null;
                try {
                    Interval interval = new Interval(date1.getTime(), date2.getTime());
                    period = interval.toPeriod(PeriodType.dayTime());
                    if (period.getYears() != 0) {
                        date = period.getYears() + " years ago";
                    } else if (period.getYears() == 1) {
                        date = period.getYears() + " year ago";
                    } else if (period.getMonths() != 0) {
                        date = period.getMonths() + " months ago";
                    } else if (period.getMonths() == 1) {
                        date = period.getMonths() + " month ago";
                    } else if (period.getDays() != 0) {
                        date = period.getDays() + " day ago";
                    } else if (period.getHours() != 0) {
                        date = period.getHours() + " hours ago";
                    } else if (period.getMinutes() != 0) {
                        date = period.getMinutes() + " minutes ago";
                    } else if (period.getMinutes() == 1) {
                        date = period.getMinutes() + " minute ago";
                    } else if (period.getSeconds() != 0) {
                        date = period.getSeconds() + " seconds ago";
                    } else if (period.getSeconds() == 1) {
                        date = period.getSeconds() + " second ago";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    date = " a moments ago";
                }


//                System.out.printf(
//                        "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
//                        period.getYears(), period.getMonths(), period.getDays(),
//                        period.getHours(), period.getMinutes(), period.getSeconds());
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static File getVideoThumbnail(String path, Context context) throws IOException {
        Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);

        File f = new File(context.getCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");
        f.createNewFile();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmThumbnail.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }

    public static String convertedDate(String dateValue) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
        Date date = originalFormat.parse(dateValue);
        String formattedDate = targetFormat.format(date);  // 20120821
        return formattedDate;
    }



    public static String getDateFormatted(String serverTime, String check) {

        String date = "";

//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
//        String dateString = formatter.format(new Date(serverTime));


//        DateTime postDateTime = new DateTime(Long.valueOf(serverTime), DateTimeZone.UTC);

        DateTime currentDateTime = new DateTime();


        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
//            String dates = postDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");
            String dates = Utils.convertedDate(serverTime);
            String currentDates = currentDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");

            Date date1 = simpleDateFormat.parse(dates);
            Date date2 = simpleDateFormat.parse(currentDates);
            if (date1.getTime() - date2.getTime() == 0) {
                date = " a moments ago";
            } else {
                Period period = null;
                try {
                    Interval interval = new Interval(date1.getTime(), date2.getTime());
                    period = interval.toPeriod(PeriodType.dayTime());
                    if (period.getYears() != 0) {
                        date = period.getYears() + " years ago";
                    } else if (period.getYears() == 1) {
                        date = period.getYears() + " year ago";
                    } else if (period.getMonths() != 0) {
                        date = period.getMonths() + " months ago";
                    } else if (period.getMonths() == 1) {
                        date = period.getMonths() + " month ago";
                    } else if (period.getDays() != 0) {
                        date = period.getDays() + " day ago";
                    } else if (period.getHours() != 0) {
                        date = period.getHours() + " hours ago";
                    } else if (period.getMinutes() != 0) {
                        date = period.getMinutes() + " minutes ago";
                    } else if (period.getMinutes() == 1) {
                        date = period.getMinutes() + " minute ago";
                    } else if (period.getSeconds() != 0) {
                        date = period.getSeconds() + " seconds ago";
                    } else if (period.getSeconds() == 1) {
                        date = period.getSeconds() + " second ago";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    date = " a moments ago";
                }


//                System.out.printf(
//                        "%d years, %d months, %d days, %d hours, %d minutes, %d seconds%n",
//                        period.getYears(), period.getMonths(), period.getDays(),
//                        period.getHours(), period.getMinutes(), period.getSeconds());
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void toast(Context context, String toastmsg) {

        Toast.makeText(context, toastmsg, Toast.LENGTH_SHORT).show();
    }

    public static Drawable setDrawableSelector(Context context, int normal, int selected) {


        Drawable state_normal = ContextCompat.getDrawable(context, normal);

        Drawable state_pressed = ContextCompat.getDrawable(context, selected);


        Bitmap state_normal_bitmap = ((BitmapDrawable) state_normal).getBitmap();

        // Setting alpha directly just didn't work, so we draw a new bitmap!
        Bitmap disabledBitmap = Bitmap.createBitmap(
                state_normal.getIntrinsicWidth(),
                state_normal.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(disabledBitmap);

        Paint paint = new Paint();
        paint.setAlpha(126);
        canvas.drawBitmap(state_normal_bitmap, 0, 0, paint);

        BitmapDrawable state_normal_drawable = new BitmapDrawable(context.getResources(), disabledBitmap);


        StateListDrawable drawable = new StateListDrawable();

        drawable.addState(new int[]{android.R.attr.state_selected},
                state_pressed);
        drawable.addState(new int[]{android.R.attr.state_enabled},
                state_normal_drawable);

        return drawable;
    }


    public static void log(String TAG, String content) {
        Log.d(TAG, " " + content);
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public static void showNoInternet(Activity context) {
        toast(context, context.getResources().getString(R.string.no_internet_connection));
    }

    public static AmazonS3Client getClient(Context context) {
        System.setProperty(SDKGlobalConfiguration.ENFORCE_S3_SIGV4_SYSTEM_PROPERTY, "true");
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                context, Constants.Pool_ID, Constants.REGION);
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);
        s3Client.setEndpoint(Constants.ENDPOINT);
        return s3Client;
    }

    public static JSONArray getHashtagLists(String text) {
        JSONArray jsonArray = new JSONArray();


        String regexPattern = "(#\\w+)";

        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String hashtag = m.group(1);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("new", hashtag);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);

        }
        Log.d(TAG, "getHashtagLists: " + jsonArray);
        return jsonArray;

    }

    public static JSONArray getHashtagLists(String text, JSONArray alreadyArray) {
        JSONArray jsonArray = getHashtagLists(text);
        JSONArray finalArray = new JSONArray();

        if (jsonArray.length() > 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                Log.d(TAG, "getHashtagLists: " + jsonObject);
                if (alreadyArray.length() > 0) {
                    for (int j = 0; j < alreadyArray.length(); j++) {
                        Log.d(TAG, "getHashtagListsAlready: " + alreadyArray.optJSONObject(j));
                        if (!jsonObject.optString("new").equalsIgnoreCase(alreadyArray.optJSONObject(j).optString("name"))) {
                            JSONObject jsonObject1 = new JSONObject();
                            try {
                                jsonObject1.put("new", jsonObject.optString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            finalArray.put(jsonObject1);
                        }
                    }
                } else {
                    return jsonArray;
                }
            }
            return finalArray;
        } else {
            return finalArray;
        }

    }

    public static RequestOptions getRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_dummy_profile);
        requestOptions.error(R.drawable.ic_dummy_profile);
        return requestOptions;

    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUriNew(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String changeTimeFormats(String commentedAt) throws ParseException {


        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
        Date date = originalFormat.parse(commentedAt);
        String formattedDate = targetFormat.format(date);
        Log.d(TAG, "changeTimeFormats: Check");
        return formattedDate;
    }

    public static void uploadFile(final Activity context, final File file, final UploadCallBack uploadCallBack) {
        if (ApiCall.isNetworkConnected(context)) {
            Utils.show(context);
            AmazonS3 amazonS3Client = getClient(context);
            TransferUtility transferUtility = new TransferUtility(amazonS3Client, context);
            Log.d(TAG, "uploadFile: " + file.getAbsolutePath());


            TransferObserver transferObserver = transferUtility.upload(Constants.BUCKET_NAME, file.getName(), file);
            transferObserver.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.e("image_state", state.toString());
                    if (state.toString().equalsIgnoreCase("IN_PROGRESS")) {

                    } else if (state.toString().equalsIgnoreCase("COMPLETED")) {
                        Utils.dismiss(context);

                        String url = Constants.BASES3_URL + file.getName();
                        Log.d(TAG, "s3Upload: " + url);
                        uploadCallBack.result(true, url);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
//                    Log.e("image_state_percentage", String.valueOf(percentage));
                }

                @Override
                public void onError(int id, Exception ex) {
                    uploadCallBack.result(true, ex.getMessage());
                    Utils.dismiss(context);
                    Log.e("image_state_error", ex.getMessage());

                }
            });
        } else {
            Utils.showNoInternet(context);

        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        //DateFormat timeFormatter = new SimpleDateFormat("hh:mma");
        String text = AppController.getContext().getResources().getString(R.string.today);
        String textq = AppController.getContext().getResources().getString(R.string.yesterday);


        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {

            return text;//timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return textq;
        } else {
            return date;
        }
    }

    public static String getImageFileName(Context context) {
        AppSettings appSettings = new AppSettings(context);
        String fileName = "";
        int randomImageNo;
        try {
            randomImageNo = Integer.parseInt(SharedPreference.getInt(context, "image_count"));
        } catch (NumberFormatException e) {
            randomImageNo = 0;
            e.printStackTrace();
        }
        String newFileName = "";
        try {

            String date1 = Utils.getDate(System.currentTimeMillis(), "dd/MM/yyyy");
            if (Utils.formatToYesterdayOrToday(date1).equalsIgnoreCase("Today")) {
                randomImageNo++;
                SharedPreference.putInt(context, "image_count", randomImageNo);
            } else {
                randomImageNo = 0;
            }

            String date = Utils.getDate(System.currentTimeMillis(), "ddMMyyyy");
            newFileName = String.valueOf("IMG_" + System.currentTimeMillis() + "_INS_C00_" + appSettings.getUserName() + "_" + randomImageNo + ".jpg");
            return newFileName;

        } catch (ParseException e) {
            e.printStackTrace();
            return newFileName;
        }
    }

    public static String getVideoFileName(Context context) {
        AppSettings appSettings = new AppSettings(context);

        String fileName = "";
        int randomImageNo;
        try {
            randomImageNo = Integer.parseInt(SharedPreference.getInt(context, "video_count"));
        } catch (NumberFormatException e) {
            randomImageNo = 0;
            e.printStackTrace();
        }
        String newFileName = "";
        try {

            String date1 = Utils.getDate(System.currentTimeMillis(), "dd/MM/yyyy");
            if (Utils.formatToYesterdayOrToday(date1).equalsIgnoreCase("Today")) {
                randomImageNo++;
                SharedPreference.putInt(context, "video_count", randomImageNo);
            } else {
                randomImageNo = 0;
            }

            String date = Utils.getDate(System.currentTimeMillis(), "ddMMyyyy");
            newFileName = String.valueOf("VID_" + System.currentTimeMillis() + "_INS_C00_" + appSettings.getUserName() + "_" + randomImageNo + ".jpg");
            return newFileName;

        } catch (ParseException e) {
            e.printStackTrace();
            return newFileName;
        }
    }

    public static void uploadFile(final Activity context, final File file, final String name, final UploadCallBack uploadCallBack) {
        if (ApiCall.isNetworkConnected(context)) {
            Utils.show(context);
            AmazonS3 amazonS3Client = getClient(context);
            TransferUtility transferUtility = new TransferUtility(amazonS3Client, context);
            Log.d(TAG, "uploadFile: " + file.getAbsolutePath());


            TransferObserver transferObserver = transferUtility.upload(Constants.BUCKET_NAME, name, file);
            transferObserver.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Log.e("image_state", state.toString());
                    if (state.toString().equalsIgnoreCase("IN_PROGRESS")) {

                    } else if (state.toString().equalsIgnoreCase("COMPLETED")) {
                        Utils.dismiss(context);

                        String url = Constants.BASES3_URL + name;
                        Log.d(TAG, "s3Upload: " + url);
                        uploadCallBack.result(true, url);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                    int percentage = (int) (bytesCurrent / bytesTotal * 100);
//                    Log.e("image_state_percentage", String.valueOf(percentage));
                }

                @Override
                public void onError(int id, Exception ex) {
                    uploadCallBack.result(true, ex.getMessage());
                    Utils.dismiss(context);
                    Log.e("image_state_error", ex.getMessage());

                }
            });
        } else {
            Utils.showNoInternet(context);

        }
    }

    public static void dismiss(Activity activity) {
        if (!activity.isFinishing()) {
            try {
                if (isShowing) {
                    isShowing = false;
                    customDialog.dismiss();
                }
            } catch (Exception e) {
                if (isShowing) {
                    isShowing = false;

                } else {
                    isShowing = true;

                }
            }
        } else {
            isShowing = !isShowing;
        }
    }

    public static void show(Activity context) {
        if (!context.isFinishing()) {
            if (!isShowing) {
                isShowing = true;
                customDialog = new CustomDialog(context);
                customDialog.setCancelable(false);
                customDialog.setCanceledOnTouchOutside(false);
                customDialog.setContentView(R.layout.custom_dialog);
                customDialog.show();
            }
        }


    }

    public interface FragmentNavigation {
        void pushFragment(Fragment fragment);
    }

    public interface UploadCallBack {

        void result(boolean status, String message);
    }


}
