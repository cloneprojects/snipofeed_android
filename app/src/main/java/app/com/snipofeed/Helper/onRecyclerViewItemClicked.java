package app.com.snipofeed.Helper;

import org.json.JSONObject;

/**
 * Created by yuvaraj on 18/1/18.
 */

public interface onRecyclerViewItemClicked {
    void onClicked(String username);
    void onClicked(String username, JSONObject jsonObject);
}
