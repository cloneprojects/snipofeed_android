package app.com.snipofeed.Helper;

/**
 * Created by yuvaraj on 04/05/18.
 */

public class ConstantKeys {
    public static String BOOKMARK_KEY = "user_pinned";
    public static String LIKES_COUNT_KEY = "total_likes";
    public static String COMMENTS_LIKES_COUNT_KEY = "comment_likes";
    public static String COMMENTS_COUNT_KEY = "total_comments";
    public static String USER_LIKED_KEY = "user_liked";
    public static String USER_LIKED_STATUS_KEY = "user_liked_status";
    public static int GRID_VIEW = 1;
    public static int LIST_VIEW = 2;
    public static int PINNED_VIEW = 2;
    public static int BOOKMARKED_VIEW = 2;
    public static int isTop = 0;
    public static int isPeople = 1;
    public static int isHashtags = 2;
    public static int isPlaces = 3;
}
