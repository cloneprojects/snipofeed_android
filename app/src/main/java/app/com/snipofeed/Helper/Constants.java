package app.com.snipofeed.Helper;

import com.amazonaws.regions.Regions;

/**
 * Created by yuvaraj on 26/04/18.
 */

public class Constants {
    public static String Pool_ID="ap-northeast-2:11b9ab57-891c-439e-9435-e09f37496aaa";
    public static Regions REGION= Regions.AP_NORTHEAST_2;
    public static String ENDPOINT="https://s3-ap-northeast-2.amazonaws.com";
    public static String BUCKET_NAME="zoechats";
    public static String BASES3_URL= "https://s3.ap-northeast-2.amazonaws.com/zoechats/";



    ///FOr imagePicker


    public static final int REQUEST_CODE_CAPTURE = 2000;

    public static final int FETCH_STARTED = 2001;
    public static final int FETCH_COMPLETED = 2002;
    public static final int ERROR = 2003;

    public static final int MAX_LIMIT = 999;

    public static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 23;
    public static final int PERMISSION_REQUEST_CAMERA = 24;

    public static final String PREF_WRITE_EXTERNAL_STORAGE_REQUESTED = "writeExternalRequested";
    public static final String PREF_CAMERA_REQUESTED = "cameraRequested";



}
