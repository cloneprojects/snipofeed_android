package app.com.snipofeed.Helper;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import app.com.snipofeed.R;

public class AnimationHelper {

    public static void slideLeft(final View in, final View out, Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_from_right);
        Animation animation1 = AnimationUtils.loadAnimation(context, R.anim.slide_to_left);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void slideRight(final View in, final View out,Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_from_left);
        Animation animation1 = AnimationUtils.loadAnimation(context, R.anim.slide_to_right);
        in.startAnimation(animation);
        out.startAnimation(animation1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                in.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                out.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
