package app.com.snipofeed.Helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.dinuscxj.refresh.IRefreshStatus;

import app.com.snipofeed.R;

/**
 * Created by yuvaraj on 03/05/18.
 */

public class HeaderView extends FrameLayout implements IRefreshStatus {
    public HeaderView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public HeaderView(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.refresh_view, null);
        addView(view);
    }

    @Override
    public void reset() {

    }

    @Override
    public void refreshing() {

    }

    @Override
    public void refreshComplete() {

    }

    @Override
    public void pullToRefresh() {

    }

    @Override
    public void releaseToRefresh() {
    }

    @Override
    public void pullProgress(float pullDistance, float pullProgress) {

    }
}
