package app.com.snipofeed.Helper;

/**
 * Created by yuvaraj on 03/05/18.
 */

public interface SmartTextCallback {

    void hashTagClick(String hashTag);
    void mentionClick(String mention);
    void emailClick(String email);
    void phoneNumberClick(String phoneNumber);
    void webUrlClick(String webUrl);
}
