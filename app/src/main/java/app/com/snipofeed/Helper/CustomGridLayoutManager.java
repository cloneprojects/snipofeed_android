package app.com.snipofeed.Helper;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import app.com.snipofeed.PrefHandler.AppSettings;

public class CustomGridLayoutManager  extends GridLayoutManager {

    private int columnWidth;
    private boolean columnWidthChanged = true;
    Context context;

    public CustomGridLayoutManager(Context context) {
        super(context, 3);
        this.context=context;

    }


    @Override
    public boolean canScrollVertically() {
        AppSettings appSettings=new AppSettings(context);
        Log.d("asda", "canScrollVertically: "+appSettings.getCanScroll());
        if (appSettings.getCanScroll().equalsIgnoreCase("true"))
        {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (columnWidthChanged && columnWidth > 0) {
            int totalSpace;
            if (getOrientation() == VERTICAL) {
                totalSpace = getWidth() - getPaddingRight() - getPaddingLeft();
            } else {
                totalSpace = getHeight() - getPaddingTop() - getPaddingBottom();
            }
            int spanCount = Math.max(1, totalSpace / columnWidth);
            setSpanCount(spanCount);
            columnWidthChanged = false;
        }
        super.onLayoutChildren(recycler, state);
    }
}
