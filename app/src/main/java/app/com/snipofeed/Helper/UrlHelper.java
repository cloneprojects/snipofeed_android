package app.com.snipofeed.Helper;

/**
 * Created by yuvaraj on 25/04/18.
 */

public class UrlHelper {
    public static String BASE_URL = "http://139.59.43.169:3012/";
    public static String SIGN_IN = BASE_URL + "login";
    public static String NEW_POST = BASE_URL + "newnewpost";
    public static String CREATE_ACCOUNT = BASE_URL + "createAccount";
    public static String CHECK_USERNAME = BASE_URL + "checkName1";
    public static String CHANGE_USER_NAME = BASE_URL + "checkName";
    public static String SOCIAL_LOGIN = BASE_URL + "facebookLogin";
    public static String FORGOT_PASSWORD = BASE_URL + "getOTP";
    public static String PROFILE_PIC_UPLOAD = BASE_URL + "profilePicUpload";
    public static String UPDATE_DEVICE_TOKEN = BASE_URL + "device_token";
    public static String ALL_FEED = BASE_URL + "feeds";
    public static String BOOKMARK_POST = BASE_URL + "addToPinboard";
    public static String BOOKMARK_POST_REMOVE = BASE_URL + "removeFromPinboard";
    public static String UN_FOLLOW_USER = BASE_URL + "unfollow";
    public static String DELETE_POST = BASE_URL + "deletePost";
    public static String REPORT_POST = BASE_URL + "report";
    public static String LIKE_POST = BASE_URL + "postlike";
    public static String UN_LIKE_POST = BASE_URL + "postunlike";
    public static String LIKE_COMMENT = BASE_URL + "commentlike";
    public static String UN_LIKE_COMMENT = BASE_URL + "commentunlike";
    public static String SHOW_COMMENTS = BASE_URL + "showComments";
    public static String POST_COMMENTS = BASE_URL + "comment";
    public static String REPLY_COMMENTS = BASE_URL + "replyes";
    public static String HASH_TAG_SEARCH = BASE_URL + "hashTagSearch";
    public static String MENTION_SEARCH = BASE_URL + "tagUserSearch";
    public static String VIEW_POST_DETAILS = BASE_URL + "viewpost";
    public static String MY_PROFILE = BASE_URL + "myProfile";
    public static String EDIT_PROFILE = BASE_URL + "editProfile";
    public static String USER_TAGGED_POSTS = BASE_URL + "userTagedPosts";
    public static String SEARCH_USER_TAGGED_POSTS = BASE_URL + "searchUserTagedPosts";
    public static String BOOKMARKED_POSTS = BASE_URL + "showPinned";
    public static String CHANGE_PASSWORD = BASE_URL + "editPassword";
    public static String TRENDING_POSTS = BASE_URL + "defaultSearchData";
    public static String SEARCH_USER = BASE_URL + "searchUser";
    public static String SEARCH_HASHTAG = BASE_URL + "hashTagSearch";
    public static String SEARCH_PLACES = BASE_URL + "searchPlaces";
    public static String UPDATE_PRIVACY = BASE_URL + "privacySetting";
    public static String USER_DETAILS = BASE_URL + "clickUser";
    public static String FOLLOW_USER = BASE_URL + "follow";
    public static String YOU_NOTIFICATION = BASE_URL + "notify";
    public static String FOLLOWING_NOTIFICATION = BASE_URL + "followingNotify";
    public static String HASH_TAG_POSTS = BASE_URL + "clicktag";
    public static String MY_FOLLOWERS = BASE_URL + "showFollowers";
    public static String MY_FOLLOWING = BASE_URL + "showFollowing";
    public static String OTHERS_FOLLOWING = BASE_URL + "showUserFollowing";
    public static String OTHERS_FOLLOWERS = BASE_URL + "showUsersFollowers";
    public static String SWITCH_TO_BUSINESS = BASE_URL + "switchToBusinessUser";
    public static String TOP_SEARCH = BASE_URL + "topsearch";
    public static String PLACES_POST = BASE_URL + "placesPost";
    public static String PROMOTE_POST = BASE_URL + "promote";
    public static String LIST_FFEDS = BASE_URL + "followingfeed";
}
