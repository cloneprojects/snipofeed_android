package app.com.snipofeed.Helper;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import app.com.snipofeed.Activity.NewPostActivity;

public class ExtendedViewPager extends ViewPager {

    /**
     * Reference to the launch activity
     */
    private NewPostActivity launchActivity;

    /**
     * Constructor to call the super constructor
     *
     * @param context The application context
     * @param attrs   The attributes
     */
    public ExtendedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Sets object reference for the {@code launchActivity}
     *
     * @param launchActivity The LaunchActivity to be set
     */
    public void set(NewPostActivity launchActivity) {
        this.launchActivity = launchActivity;
    }

    /**
     * Determines if the pager can be swiped based off the x and y inputs provided, as well as if the
     * barchart can be panned or not.
     *
     * @param x The x coordinate to check
     * @param y The y coordinate to check
     * @return True if the ViewPager will continue with normal swiping action.
     */
    public boolean canSwipe(float x, float y) {
        boolean canSwipe;
        canSwipe = !isPointInsideView(x, y, NewPostActivity.getPagerAdapter());
        Log.d("can", "canSwipe: "+canSwipe);
        return canSwipe;
    }

    /**
     * Takes x and y coordinates and compares them to the coordinates of the passed view. Returns true if the passed coordinates
     * are within the range of the {@code view}
     *
     * @param x    The x coordinate to compare
     * @param y    The y coordinate to compare
     * @param view The view to check the coordinates of
     * @return True if the x and y coordinates match that of the view
     */
    private boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        // point is inside view bounds
        return ((x > viewX && x < (viewX + view.getWidth())) && (y > viewY && y < (viewY + view.getHeight())));
    }

    /**
     * Override of the onInterceptTouchEvent which allows swiping to be disabled when chart is selected
     *
     * @param ev The MotionEvent object
     * @return Call to super if true, otherwise returns false
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return canSwipe(ev.getX(), ev.getY()) ? super.onInterceptTouchEvent(ev) : false;
    }

}