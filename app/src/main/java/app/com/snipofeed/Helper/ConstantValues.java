package app.com.snipofeed.Helper;

public class ConstantValues {
    public static String REACH_ONE ="13-25" ;
    public static String REACH_TWO="45-90" ;
    public static String REACH_THREE="150-200" ;
    public static String REACH_FOUR="250-400" ;

    public static String EST_ONE ="1,100-2,100" ;
    public static String EST_TWO="2,500-3,200" ;
    public static String EST_THREE="3,700-4,900" ;
    public static String EST_FOUR="5,500-8,000" ;

    public static String BUSINESS_USER="business";
    public static String NORMAL_USER="normal";
    public static String LEARN_MORE="Learn More";
    public static String SHOP_NOW="Shop Now";
    public static String WATCH_MORE="Watch More";
    public static String CONTACT_US="Contact Us";
    public static String BOOK_NOW="Book Now";
    public static String SIGN_UP="Sign Up";

}
