package app.com.imagefilters.utils;


import app.com.imagefilters.imageprocessors.Filter;

/**
 * @author Varun on 01/07/15.
 */
public interface ThumbnailCallback {

    void onThumbnailClick(Filter filter);
}
